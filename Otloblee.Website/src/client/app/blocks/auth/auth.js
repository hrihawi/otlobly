﻿(function () {
    'use strict';

    angular
        .module('blocks.auth')
        .factory('auth', auth);

    auth.$inject = ['$http', '$cookieStore', 'exception', '$filter'];

    function auth($http, $cookieStore, exception, $filter) {

        var accessLevels = routingConfig.accessLevels,
            userRoles = routingConfig.userRoles,
            currentUser = $cookieStore.get('otlobleeuser') || { id: '',phoneNumber:'', username: '', email: '', token: '', firstName: '', lastName: '', expired: '', role: userRoles.public };
        //$cookieStore.remove('user');

        var service = {
            authorize: authorize,
            isLoggedIn: isLoggedIn,
            register: register,
            login: login,
            logout: logout,
            accessLevels: accessLevels,
            userRoles: userRoles,
            resetUser:resetUser,
            user: currentUser,
            hasSecurityRoles: hasSecurityRoles,
            allowed: allowed
        };

        return service;
        ///////////////
        function allowed(permission) {
            return currentUser && currentUser.role.title && currentUser.role.title == permission;            
        }
        function getRole(roleName) {
          
            return userRoles[roleName];
        }
        function changeUser(userInfo) {
            console.log(userInfo);
            var _user = {id:userInfo.Id,email:userInfo.Email,username:userInfo.UserName,phoneNumber:userInfo.PhoneNumber, firstName: userInfo.FirstName, lastName: userInfo.LastName, expired: userInfo.expires_in, token: userInfo.access_token, role: getRole(userInfo.Roles) };
            console.log(_user);

            $cookieStore.put('otlobleeuser', _user);
            angular.extend(currentUser, _user);
        }

        function authorize(accessLevel, role) {
            if (role === undefined) {
                role = currentUser.role;
            }
            console.log("currentUser");

            console.log(currentUser.role);
            console.log("role");
            console.log(role);


            return accessLevel.bitMask & role.bitMask;
        }

        function isLoggedIn(user) {
            if (user === undefined) {
                user = currentUser;
            }
            return user.role.title === userRoles.User.title || user.role.title === userRoles.User.title;
        }

        function register(user, success, error) {
            $http.post('/register', user).success(function (res) {
                changeUser(res);
                success();
            }).error(error);
        }

        function login(loginData) {
            var data = 'grant_type=password&username=' + loginData.userName + '&password=' + loginData.password;
            resetUser();

            return $http.post('api/oauth/token',
                data,
                {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                .then(function (userInfo) {
                    console.log(userInfo);
                    changeUser(userInfo.data);
                    return userInfo.data;
                })
                .catch(function (error) {
                    //console.log('error : ', error);
                    //return exception.catcher(error.data.error_description)(error);
                });
        }

        function logout() {
            resetUser();
            //return $http.post('api/account/signout')
            //    .then(function () {
            //        resetUser();
            //    }).catch(function (error) {
            //        return exception.catcher('Error in signning out!')(error);
            //    });
        }
        function hasSecurityRoles(requiredRoles) {
            var hasRole = false,
                roleCheckPassed = false,
                loweredRoles;
            if (currentUser.role === undefined) {
                hasRole = false;
            } else if (currentUser.role !== undefined && requiredRoles === undefined) {
                hasRole = true;
            } else if (loggedInUser !== undefined && requiredRoles !== undefined) {
                if (requiredRoles.length === 0) {
                    hasRole = true;
                } else if (loggedInUser.permittedActions === undefined) {
                    hasRole = false;
                } else {
                    loweredRoles = [];
                    angular.forEach(loggedInUser.permittedActions, function (role) {
                        loweredRoles.push(role.name.toLowerCase());
                    });

                    // check user has at least one role in given required roles
                    angular.forEach(requiredRoles, function (role) {
                        roleCheckPassed = roleCheckPassed || _.contains(loweredRoles, role.toLowerCase());
                    });

                    hasRole = roleCheckPassed;
                }
            }

            return hasRole;
        };
        function resetUser() {
            $cookieStore.remove('otlobleeuser');
            angular.extend(currentUser, {
                id: '', username: '', email: '', token: '', role: userRoles.public
            });
        }
    }
})();