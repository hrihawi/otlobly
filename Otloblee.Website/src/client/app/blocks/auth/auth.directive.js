﻿(function () {
    'use strict';

    angular
        .module('blocks.auth')
        .directive('allowed', allowed)
        .directive('notallowed', notallowed);

    allowed.$inject = ['common','auth'];

    function allowed(common,auth) {
        var directive = {
            restrict: "A",
            link: linker
        };


        function linker(scope, element, attrs) {
            var roles = attrs.allowed.split(',');
            var isAllow = false;
            for (var index = 0; index < roles.length; index++) {
                if (auth.allowed(roles[index])) {
                    isAllow = true;
                    break;
                }

            }
            if (!isAllow) {
                element.remove();
            }
        }

        return directive;


    }

       notallowed.$inject = ['common','auth'];

    function notallowed(common,auth) {
        var directive = {
            restrict: "A",
            link: linker
        };


        function linker(scope, element, attrs) {
            var roles = attrs.notallowed.split(',');
            var isAllow = true;
            for (var index = 0; index < roles.length; index++) {
                if (auth.allowed(roles[index])) {
                    isAllow = false;
                    break;
                }
            }
            if (!isAllow) {
                element.remove();
            }
        }

        return directive;


    }

})();