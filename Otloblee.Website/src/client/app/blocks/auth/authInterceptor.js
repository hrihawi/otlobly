﻿(function () {
    'use strict';

    angular
        .module('blocks.auth')
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['config', '$location', '$q', '$cookieStore', '$injector'];

    function authInterceptor(config, $location, $q, $cookieStore, $injector) {
        return {
            request: function (configuration) {
                configuration.headers = configuration.headers || {};
                if (configuration.url.startsWith('api/')) {
                    configuration.url = config.basePath + configuration.url
                }
                if (configuration.url.startsWith('ConfirmEmail')) {
                    configuration.url = config.basePath + configuration.url
                }
               
                if ($cookieStore.get('otlobleeuser') !== undefined) {
                    configuration.headers.Authorization = 'Bearer ' + $cookieStore.get('otlobleeuser').token;
                }
                return configuration;
            },
            responseError: function (rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    var auth = $injector.get('auth');
                    auth.resetUser();
                    $location.path('/login');
                }
                return $q.reject(rejection);
            }
        };
    }

    angular
        .module('blocks.auth')
        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptor');
        });
})();