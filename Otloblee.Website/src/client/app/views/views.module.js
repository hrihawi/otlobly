(function() {
    'use strict';

    angular.module('app.views', ['app.core', 'app.widgets', 'ngMap', 'ngGeolocation', 'ngDialog','ngRateIt','ngDialog']);
})();
