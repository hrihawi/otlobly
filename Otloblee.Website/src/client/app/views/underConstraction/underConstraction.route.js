﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        routerHelper.configureStates(getStates(access));
    }

    function getStates(access) {
        return [
            {
                state: 'underConstraction',
                config: {
                    url: '/underConstraction',
                    data: {
                        access: access.user
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/underConstraction/underConstraction.html'
                       
                        }
                    }
                }
            }
        ];
    }
})();
