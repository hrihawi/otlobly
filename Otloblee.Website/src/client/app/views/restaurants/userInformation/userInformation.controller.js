(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('userInformationController', userInformationController);

    userInformationController.$inject = ['restaurantsService', 'userManagmentService', 'auth', '$state'];
    function userInformationController(restaurantsService, userManagmentService, auth, $state) {
        var vm = this;

        vm.bill = restaurantsService.bill.get();
        vm.user = null;
        vm.submit = submit;
        activate();

        ////////////////

        function activate() {
            if (!restaurantsService.bill.isVar())
                $state.go('restaurants');
            if (auth.user.id == '') {
                $state.go('loginApp', { returnTo: 'restaurants.userInformation' });
            } else
                getUser();
        }

        function submit() {

            vm.bill.userInfo = {};
            vm.bill.userInfo.firstName = vm.user.firstName;
            vm.bill.userInfo.lastName = vm.user.lastName;
            vm.bill.userInfo.phoneNumber = vm.user.phoneNumber;
            vm.bill.userInfo.fullAddress = vm.user.fullAddress;
            vm.bill.userInfo.UserNotes = vm.user.UserNotes;

            restaurantsService.bill.set(vm.bill);
            $state.go('restaurants.checkOut');

        }

        function getUser() {
            userManagmentService.getUserInfo(auth.user.id).then(function (result) {

                vm.user = result;
            });

        }

    }
})();