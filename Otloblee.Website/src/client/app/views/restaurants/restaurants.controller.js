﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantsController', restaurantsController);

    restaurantsController.$inject = ['$scope', '$filter', 'common', '$stateParams', 'restaurantsService', 'askDialog', 'ngDialog', 'NgMap', '$state'];

    function restaurantsController($scope, $filter, common, $stateParams, restaurantsService, askDialog, ngDialog, NgMap, $state) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        //vm.map2 = { center: { latitude: 45, longitude: 31 }, zoom: 8 };
        vm.item = {};
        vm.positions = [];
        vm.showAll = false;
        vm.restaurantCount = 0;
        vm.getInfoBox = getInfoBox;
        vm.showStore = showStore;
        vm.resturantInfo = {};
        vm.currentLocation = [];
        vm.showMoreRestaurants = showMoreRestaurants;
        vm.foodTypes = [];
        vm.changeTypeFilter = changeTypeFilter;
       vm.changeRestaurantTypeFilter = changeRestaurantTypeFilter;
        activate();


        function activate() {
            restaurantsService.bill.clear();
            restaurantsService.filter.clearTypeFilter();
            restaurantsService.filter.clearRestaurantTypeFilter()
          /*  restaurantsService.general.foodTypes().then(function (results) {
                vm.foodTypes = results;
            });*/
            restaurantsService.getAll().then(function (results) {
                for (var index = 0; index < results.length; index++) {
                    if (!results[index].logoImg) {
                        if (!results[index].logo) {
                            results[index].logoImg = 'img/noPicture110.jpg';
                        }
                        else {
                            results[index].logoImg = 'data:image/png;base64,' + results[index].logo;
                        }
                    }
                }
                vm.restaurants = results;
                vm.restaurantCount = restaurantsService.restaurantCount();
                
                getLocations(results);
                NgMap.getMap().then(function (map) {
                    vm.map = map;
                });
            });

            getCurrnetLocation();

        }



        function changeTypeFilter(type) {
            if (type=='All'){
                restaurantsService.filter.clearTypeFilter().then(getRestaurants());
            }else
            {
            restaurantsService.filter.setTypeFilter(type).then(getRestaurants());
            }

        }
        function changeRestaurantTypeFilter(type) {
              if (type=='All'){
                restaurantsService.filter.clearRestaurantTypeFilter().then(getRestaurants());
            }else
            {
            restaurantsService.filter.seteRestaurantTypeFilter(type).then(getRestaurants());
            }
        }
        function getRestaurants() {
                restaurantsService.getAll(true).then(function (results) {
                    for (var index = 0; index < results.length; index++) {
                        if (!results[index].logoImg) {
                            if (!results[index].logo) {
                                results[index].logoImg = 'img/noPicture110.jpg';
                            }
                            else {
                                results[index].logoImg = 'data:image/png;base64,' + results[index].logo;
                            }
                        }
                    }
                    vm.restaurants = results;
                vm.restaurantCount = restaurantsService.restaurantCount();
                    
                    getLocations(results);
                    NgMap.getMap().then(function (map) {
                        vm.map = map;
                    });
                });
                getCurrnetLocation();
            }
        function showMoreRestaurants() {
            restaurantsService.showMoreRestaurants().then(function (result) {
                if (result.showAll) {
                    vm.showAll = true;
                }
                   activate();
            });
        }

        function getCurrnetLocation() {
            var location = restaurantsService.getLocation();
            console.log('----------------');
            console.log(location);
            if (!location) {
                $state.go("main");
            } else
                vm.currentLocation = restaurantsService.getLocation().split(',');
            console.log(vm.currentLocation);
        }

        function showStore(event, id) {
            vm.resturantInfo = findByID(vm.restaurants, id);
            vm.map.showInfoWindow('bar', this);
        }

        function getLocations(restaurnants) {
            for (var i = 0; i < restaurnants.length; i++) {
                vm.positions.push({
                    gpsLat: restaurnants[i].gpsLat,
                    gpsLng: restaurnants[i].gpsLng,
                    restid: restaurnants[i].id
                });
            }
        }
        function findByID(data, id) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].id == id) {
                    return data[i];
                }
            }
        }
        function getInfoBox(item) {

            return '<div class="marker_info" id="marker_info">' +
                '<img src="' + item.logo + '" alt=""/>' +
                '<h3>' + item.nameArabic + '</h3>' +
                '<em>' + item.foodType + '/' + item.restaurantType + '</em>' +
                '<span>' + item.address + '</span>' +
                '<a href="' + item.url_point + '" class="btn_1">Details</a>' +
                '</div>';

        };

        var data;
        vm.changeStatus = changeStatus;
        vm.addNewRestaurant = addNewRestaurant;
        //vm.map = { center: { latitude: $stateParams.gpslat, longitude: $stateParams.gpslng }, zoom: 8 };

        function addNewRestaurant() {
            ngDialog.open({
                template: 'app/views/restaurants/addNewRestaurant/addNewRestaurant.html',
                controller: 'addNewRestaurantController',
                controllerAs: 'vm',
                className: 'ngdialog-theme-default'
            });
        }

        function setFilter(type) {
            // restaurantsService.filter.set
        }



        function changeStatus(userid, status) {
            askDialog.confirm(
                "Are you sure you want to change status of this user", "warning", "Are you sure?", "Ok", "ok, it's changed", function () { console.log("done"); }
            );
        }







    }
})();
