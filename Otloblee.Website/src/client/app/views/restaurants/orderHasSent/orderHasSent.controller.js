(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('orderHasSentController', orderHasSentController);

    orderHasSentController.$inject = ['restaurantsService','$state'];
    function orderHasSentController(restaurantsService,$state) {
        var vm = this;
        vm.bill = restaurantsService.bill.get();

        activate();

        ////////////////

        function activate() {
            if (!restaurantsService.bill.isVar())
                $state.go('restaurants');

        }
    }
})();