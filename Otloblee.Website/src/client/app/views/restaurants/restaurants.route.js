﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper,access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'restaurants',
                config: {
                    url: '/restaurants',
                    params: { gpslng: null, gpslat: null },

                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/restaurants.html',
                            controller: 'restaurantsController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.menu',
                config: {
                    url: '/menu/:restaurantID',

                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/menu/menu.html',
                            controller: 'menuController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.userInformation',
                config: {
                    url: '/userInformation',

                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/userInformation/userInformation.html',
                            controller: 'userInformationController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.checkOut',
                config: {
                    url: '/checkOut',

                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/checkOut/checkOut.html',
                            controller: 'checkOutController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.User
                    }
                }
            },
            {
                state: 'restaurants.orderHasSent',
                config: {
                    url: '/orderHasSent',

                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/orderHasSent/orderHasSent.html',
                            controller: 'orderHasSentController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.User
                    }
                }
            }
        ];
    }
})();
