﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('restaurantsService', restaurantsService);

    restaurantsService.$inject = ['$http', '$location', 'common', 'exception', '$cookieStore', '$translate'];
    /* @ngInject */
    function restaurantsService($http, $location, common, exception, $cookieStore, $translate) {
        var logger = common.logger;
        var veiwResturantsNumber = 5;
        var localRestaurantInfo = null;
        var localMenusCategorise = null;
        var localAllRestaurants = null;
        var localTopRatedRestaurants = null;
        var countries = null;
        var foodTypes = null;
        var restaurantFilters = {
            foodType: '',
            restaurantType: '',
            distance: 5
        };
        var filterByType = [];
        var bill = {
            total: 0,
            subTotal: 0,
            deliveryFees: 0,
            orders: []
        };
        var localMenus = null;
        var currentLocation = null;
        var selectedResturantId;
        var service = {
            getAll: getAllRestaurants,
            getGoogleRestaurants: getGoogleRestaurants,
            showMoreRestaurants: showMoreRestaurants,
            getTopRatedRestaurants: getTopRatedRestaurants,
            getLocation: getLocation,
            setLocation: setLocation,
            getById: getLocalRestaurantInfo,
            submitOrder: submitOrder,
            addToFilterByType: addToFilterByType,
            restaurantCount: restaurantCount,
            checkCoupon: checkCoupon,
            filter: {
                setTypeFilter: setTypeFilter,
                getTypeFilter: getTypeFilter,
                clearTypeFilter: clearTypeFilter,
                seteRestaurantTypeFilter: seteRestaurantTypeFilter,
                getRestaurantTypeFilter: getRestaurantTypeFilter,
                clearRestaurantTypeFilter: clearRestaurantTypeFilter
            },
            bill: {
                get: getBill,
                set: setBill,
                clear: clearBill,
                isVar: isBillContainOrders
            },

            Menus: {
                getById: getLocalRestaurantInfo
            },

            general: {
                countries: getCountries,
                foodTypes: getFoodTypes,
                resturantTypes: getRestaurantTypes
            }
        };

        return service;

        function getGoogleRestaurants() {
            var thislocation = getLocation().split(',');
            var lat = thislocation[0];
            var lng = thislocation[1];
            console.log(lat + '    ' + lng);
            return $http.get('http://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=5000&type=restaurant&key=AIzaSyBWr1DQiqfQoBMjBaUhAEm4OTTqzsujG4w')
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });
            function getAllRestaurantsComplete(data, status, headers, config) {
                localTopRatedRestaurants = data.data.result;
                return localTopRatedRestaurants;
            }

        }

        function addToFilterByType(filter) {
            filterByType.push(filter);
        }

        function getLang() {
            return $translate.use();
        }

        function getBill() {
            return bill;
        }
        function setBill(newBill) {
            bill = newBill;
        }
        function clearBill() {
            bill = {
                total: 0,
                subTotal: 0,
                deliveryFees: 0,
                orders: []
            };
        }
        function setTypeFilter(type) {
            restaurantFilters.foodType = type;
            return common.$q.when('true');
        }
        function getTypeFilter(type) {
            return restaurantFilters.foodType;
        }
        function clearTypeFilter() {
            restaurantFilters.foodType = "";
            return common.$q.when('true');
        }
        function isBillContainOrders() {
            if (bill.subTotal > 0) {
                return true;
            } else
                return false;
        }

        function restaurantCount() {
            return localAllRestaurants.length;
        }
        function submitOrder(order) {
            return $http.post('api/order/Add', {
                paymentType: order.paymentType,
                deliveryAddress: order.fullAddress,
                userNotes: order.userNotes,
                userId: order.userId,
                firstName: order.firstName,
                lastName: order.lastName,
                phoneNumber: order.phoneNumber,
                fullAddress: order.fullAddress,
                restaurantId: order.restaurantId,
                orderDetails: order.orderDetails,
            })
                .then(addNewRestaurantComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for submit Order')(message);
                });

            function addNewRestaurantComplete(data, status, headers, config) {
                console.log(data);
                return data.data;

            }

        }
        function getLocation() {
            if (currentLocation == null && !$cookieStore.get('location')) {
                return null;
            }
            if ($cookieStore.get('location')) {
                currentLocation = $cookieStore.get('location');
                return currentLocation;
            } else
                return currentLocation;
        }

        function setLocation(lat, lng) {
            currentLocation = lng + "," + lat;
            $cookieStore.remove('location');
            $cookieStore.put('location', currentLocation);
        }

        function getLocalRestaurantInfo(id) {
            console.log('----------');
            console.log(id);
            console.log(localMenus);

            if (localAllRestaurants) {
                for (var i = 0; i < localAllRestaurants.length; i++) {
                    if (id === localAllRestaurants[i].id) {
                        return common.$q.when(localAllRestaurants[i]);
                    }
                }
            } else
                return common.$q.when(null);

        }

        function addNewRestaurant(restaurantInfo) {
            return $http.post('api/Restaurant/Add', {
                nameArabic: restaurantInfo.nameArabic,
                nameEnglish: restaurantInfo.nameEnglish,
                country: restaurantInfo.country,
                phoneNumber1: restaurantInfo.phoneNumber1,
                email: restaurantInfo.email,
                feesPaymentType: restaurantInfo.feesPaymentType,
                fees: restaurantInfo.fees,
                startWorkTime: '08:00',
                endWorkTime: '23:59'
            })
                .then(addNewRestaurantComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add new restaurant')(message);
                });

            function addNewRestaurantComplete(data, status, headers, config) {
                console.log(data);
                return data.data;

            }
        }

        function showMoreRestaurants() {
            veiwResturantsNumber = veiwResturantsNumber + 5;
            var showAll = false;
            if (localAllRestaurants.length <= veiwResturantsNumber) {
                veiwResturantsNumber = localAllRestaurants.length;
                showAll = true;
            };
            return common.$q.when({ showAll: showAll, MaxLength: localAllRestaurants.length });
        }

        function getTopRatedRestaurants() {
            if (localTopRatedRestaurants) {
                return common.$q.when(localTopRatedRestaurants);
            }
            return $http.get('api/Restaurant/SearchSummary?DisplayCount=6&lng=' + getLang())
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });
            function getAllRestaurantsComplete(data, status, headers, config) {
                localTopRatedRestaurants = data.data.result;
                return localTopRatedRestaurants;
            }
        }

        function getAllRestaurants(forceToUpdate) {
            if (localAllRestaurants && !forceToUpdate) {
                var returnedItems = [];
                for (var index = 0; index < veiwResturantsNumber; index++) {
                    returnedItems.push(localAllRestaurants[index]);
                }
                return common.$q.when(returnedItems);
            }
            var typeFilter = '';
            var restaurantTypeFilter = ''
            if (restaurantFilters.foodType != '') {
                typeFilter = '&CategoryType=' + restaurantFilters.foodType;
            }
            if (restaurantFilters.restaurantType != '') {
                restaurantTypeFilter = '&RestaurantType=' + restaurantFilters.restaurantType;
            }
            return $http.get('api/Restaurant/SearchSummary?DisplayCount=300&lng=' + getLang() + typeFilter + restaurantTypeFilter)
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });
            function getAllRestaurantsComplete(data, status, headers, config) {
                veiwResturantsNumber = 5;
                localAllRestaurants = data.data.result;

                console.log(data.data.result);
                if (localAllRestaurants.length <= veiwResturantsNumber) {
                    veiwResturantsNumber = localAllRestaurants.length;
                }
                var returnedItems = [];
                for (var index = 0; index < veiwResturantsNumber; index++) {
                    returnedItems.push(localAllRestaurants[index]);
                }
                return returnedItems;
            }
        }
        function doFilterByType(restarants) {
            var restaurantsToView = [];
            for (var i = 0; i < restarants.length; i++) {
                for (var j = 0; j < restarants[i].menus.length; j++) {
                    if (filterByType.indexOf(restarants[i].menus[j].categoryName) > -1) {
                        restaurantsToView.push(restarants[i]);
                        break;
                    }
                }
            }
            return restaurantsToView;

        }

        function getSelectedResturant() {
            console.log(localRestaurantInfo);

            if (localRestaurantInfo) {
                return common.$q.when(localRestaurantInfo);
            }
            return common.$q.when(null);
        }


        function getCountries() {
            return common.$q.when([
                {
                    DisplayName: 'المملكة العربية السعودية',
                    Value: 'المملكة العربية السعودية'
                },
                {
                    DisplayName: 'الامارات العربية',
                    Value: 'الامارات العربية'
                },
                {
                    DisplayName: 'الكويت',
                    Value: 'الكويت'
                },
                {
                    DisplayName: 'تركيا',
                    Value: 'تركيا'
                },
                {
                    DisplayName: 'مصر',
                    Value: 'مصر'
                }
            ]);
        }

        function getFoodTypes() {
            if (foodTypes) {
                return common.$q.when(foodTypes);
            }
            return $http.get('api/MenuCategory/Search&lng=' + getLang())
                .then(getFoodTypesComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get MenuCategory')(message);
                });

            function getFoodTypesComplete(data, status, headers, config) {
                foodTypes = data.data.result;
                return data.data.result;
            }
        }
        function getRestaurantTypes() {

            return $http.get('api/RestaurantType/Search?Lng=' + getLang())
                .then(RestaurantTypeComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get RestaurantType')(message);
                });

            function RestaurantTypeComplete(data, status, headers, config) {

                return data.data.result;
            }


        }

        function checkCoupon(CouponCode, RestaurantId, SubTotalPrice) {
            return $http.get('api/coupon/CheckValidity?CouponCode=' + CouponCode + '&RestaurantId=' + RestaurantId + '&SubTotalPrice=' + SubTotalPrice)
                .then(addNewRestaurantComplete)
                .catch(function (message) {
                });

            function addNewRestaurantComplete(data, status, headers, config) {
                console.log(data);
                return data.data;
            }
        }

        function seteRestaurantTypeFilter(type) {
            restaurantFilters.restaurantType = type;
            return common.$q.when('true');
        }
        function getRestaurantTypeFilter() {
            return restaurantFilters.restaurantType;
        }
        function clearRestaurantTypeFilter() {
            restaurantFilters.restaurantType = "";
            return common.$q.when('true');
        }

    }
})();


