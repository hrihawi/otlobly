(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('checkOutController', checkOutController);

    checkOutController.$inject = ['restaurantsService', 'auth', '$state', 'common'];
    function checkOutController(restaurantsService, auth, $state, common) {
        var vm = this;
        vm.bill = restaurantsService.bill.get();
        vm.sentOrder = submit;
        activate();
        vm.payment = {};
        vm.payment.paymentType = 'Cash';
        vm.checkCopupon = checkCopupon;
        ////////////////

        function activate() {
            if (!restaurantsService.bill.isVar())
                $state.go('restaurants');
        }

        function submit(order) {
            var newOrder = {
                paymentType: order.paymentType,
                deliveryAddress: vm.bill.userInfo.fullAddress,
                userNotes: vm.bill.userInfo.userNotes,
                userId: auth.user.id,
                firstName: vm.bill.userInfo.firstName,
                lastName: vm.bill.userInfo.lastName,
                phoneNumber: vm.bill.userInfo.phoneNumber,
                fullAddress: vm.bill.userInfo.fullAddress,
                restaurantId: vm.bill.restaurantId,
                orderDetails: vm.bill.orders
            }


            restaurantsService.submitOrder(newOrder).then(function (result) {
                vm.bill.createdDateTime = result.result.createdDateTime;
                restaurantsService.bill.set(vm.bill);
                $state.go('restaurants.orderHasSent');
            });
        }
        function checkCopupon() {
            restaurantsService.checkCoupon(vm.coupon, vm.bill.restaurantId, vm.bill.subTotal).then(function (result) {
                if (result) {
                    if (result.minimumBillValue >= vm.bill.subTotal && IsActive) {
                        common.logger.success('الكوبون صحيح سيتم الخصم من قيمة الفاتورة');
                        vm.bill.copounDiscount = vm.bill.subTotal - DiscountValue;
                    } else
                    common.logger.error('المعذرة لا يمكن تطبيق هذا الكوبون ');
                    
                } else
                    common.logger.error('الكوبون غير صحيح');

            });
        }
    }
})();