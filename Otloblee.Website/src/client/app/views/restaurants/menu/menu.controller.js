﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('menuController', menuController);

    menuController.$inject = ['$filter', 'common', '$stateParams', 'restaurantsService', 'askDialog', '$state', 'config'];

    function menuController($filter, common, $stateParams, restaurantsService, askDialog, $state, config) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.item = {};
        var data;
        vm.restaurantID = null;
        vm.restaurantInfo = null;
        vm.addToOrders = addToOrders;
        vm.bill = restaurantsService.bill.get();
        vm.removeOrder = removeOrder;
        activate();

        function removeOrder(order) {
            var orderIndex = vm.bill.orders.indexOf(order);
            if (vm.bill.orders[orderIndex].count > 1) {
                vm.bill.orders[orderIndex].count = vm.bill.orders[orderIndex].count - 1;
            }
            else {
                vm.bill.orders.splice(orderIndex, 1);
            }

            restaurantsService.bill.set(vm.bill);
            calculateOrders();

        }

        function addToOrders(meal) {
            if (!meal.count) {
                meal.count = 1;
            }
            var addedBefore = -1;
            for (var i = 0; i < vm.bill.orders.length; i++) {
                if (vm.bill.orders[i].id === meal.id) {
                    addedBefore = i;
                    console.log(addedBefore);
                }
            }
            if (addedBefore == -1) {
                meal.mealId = meal.id;
                var mealForAdd = {
                    price: meal.price,
                    mealId: meal.id,
                    count: 1,
                    id: meal.id
                };
                vm.bill.orders.push(meal);
            } else {
                vm.bill.orders[addedBefore].count = vm.bill.orders[addedBefore].count + 1;
            }

            restaurantsService.bill.set(vm.bill);
            calculateOrders();
        }
        function calculateOrders() {
            vm.bill.subTotal = 0;
            vm.bill.total = 0;

            for (var i = 0; i < vm.bill.orders.length; i++) {
                vm.bill.subTotal = vm.bill.subTotal + (vm.bill.orders[i].price * vm.bill.orders[i].count);
            }
            if (vm.bill.subTotal >= vm.restaurantInfo.minAmountForFreeDelivery) {
                vm.bill.deliveryFees = 0;
            } else
                vm.bill.deliveryFees = vm.restaurantInfo.deliveryFees

            vm.bill.total = vm.bill.subTotal + vm.bill.deliveryFees;
            restaurantsService.bill.set(vm.bill);
        }

        function activate() {
            if ($stateParams.restaurantID) {
                vm.restaurantID = $stateParams.restaurantID;
            }
            getResturantByID(vm.restaurantID);
            restaurantsService.bill.set(vm.bill);
        }

        function getResturantByID(id) {
            var basePath = config.basePath;
            restaurantsService.getById(id).then(function (results) {
                 if (!results) {
                    $state.go('main');
                }
                vm.restaurantInfo = results;
                for (var index = 0; index < vm.restaurantInfo.menus.length; index++) {
                    for (var j = 0; j < vm.restaurantInfo.menus[index].meals.length; j++) {
                        if (vm.restaurantInfo.menus[index].meals[j].images.length > 0) {
                            var imageid = vm.restaurantInfo.menus[index].meals[j].images[0].id;
                            vm.restaurantInfo.menus[index].meals[j].image = basePath + 'api/ImageForMeal/Get?id=' + imageid;

                        } else
                            vm.restaurantInfo.menus[index].meals[j].image ='img/noPicture110.jpg';
                    }
                }
                console.log( vm.restaurantInfo);
               
                vm.bill.restaurantId = vm.restaurantID;
                vm.bill.currency = vm.restaurantInfo.currency

            });
        }

    }
})();
