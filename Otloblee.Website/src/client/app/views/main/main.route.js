﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper','access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'main',
                config: {
                    url: '/',
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/main/main.html',
                            controller: 'mainPageController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            }
        ];
    }
})();
