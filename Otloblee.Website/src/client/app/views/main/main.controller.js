﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('mainPageController', mainPageController);

    mainPageController.$inject = ['$timeout', '$state', '$scope', 'growlService', '$geolocation', 'restaurantsService'];
    /* @ngInject */
    function mainPageController($timeout, $state, $scope, growlService, $geolocation, restaurantsService) {
        var vm = this;
        vm.getLocation = getLocation;
        activate();
        vm.map2 = { center: { latitude: 45, longitude: -73 }, zoom: 8 };
        vm.getTopRated = [];

        function activate() {
            getTopRated();
        }

        function getTopRated() {
            restaurantsService.getTopRatedRestaurants().then(function (results) {
                for (var i = 0; i < results.length; i++) {
                    if (!results[i].logoImg) {
                        if (!results[i].logo) {
                            results[i].logoImg = 'img/noPicture110.jpg';
                        }
                        else {
                            results[i].logoImg = 'data:image/png;base64,' + results[i].logo;
                        }
                    }
                }
                vm.getTopRated = results;

            });
        }
        function getLocation() {
            $geolocation.getCurrentPosition({
                timeout: 60000
            }).then(function (position) {
                restaurantsService.filter.clearTypeFilter();
                restaurantsService.filter.clearRestaurantTypeFilter();
                restaurantsService.getAll(true).then(function () {
                    restaurantsService.setLocation(position.coords.longitude, position.coords.latitude);
                    $state.go("restaurants", { gpslng: position.coords.longitude, gpslat: position.coords.latitude });
                });
            });

        }
    }
})();
