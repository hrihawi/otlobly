(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('activateMobileController', activateMobileController);

    activateMobileController.$inject = ['userManagmentService', '$state','common','auth'];
    function activateMobileController(userManagmentService, $state,common,auth) {
        var vm = this;
            var logger = common.logger;
        vm.submitCode = submit;
        vm.resendCode = resendCode;
        vm.user= {};
        activate();

        ////////////////

        function activate() {
            vm.user.phoneNumber = auth.user.phoneNumber;
        }

        function submit(code) {
            userManagmentService.checkActivationCode(code.mobileCode).then(function (result) {
                console.log(result);
                if (result) {
                    $state.go('profile.myInfo');
                    
                }

            });
        }
        function resendCode() {
            userManagmentService.changeNumber(vm.user.phoneNumber).then(function (result) {
                console.log(result);
                if  (result)
                logger.info("لقد تم اعادة ارسال الكود");
                else
                logger.error("يرجى التأكد من الرقم, يجب ان يكون غير مسجل من قبل");
                
            });

        }
    }
})();