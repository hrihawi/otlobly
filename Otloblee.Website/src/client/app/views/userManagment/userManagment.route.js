﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'userManagment',
                config: {
                    url: '/user',
                    data: {
                        access: access.public
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content': {
                            templateUrl: 'app/views/userManagment/userManagment.html',
                            controller: 'userManagmentController',
                            controllerAs: 'vm'
                        }
                    }
                }
            },
            {
                state: 'userManagment.activeMobile',
                config: {
                    url: '/mobileActivation',
                    data: {
                        access: access.User
                    },
                    views: {
                        'subContent': {
                            templateUrl: 'app/views/userManagment/activateMobile/activateMobile.html',
                            controller: 'activateMobileController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }

            
        ];
    }
})();
