﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('userManagmentController', userManagmentController);

    userManagmentController.$inject = ['loginService', 'growlService', 'auth', '$state'];
    function userManagmentController(loginService, growlService, auth, $state) {
        var vm = this;
        vm.submit = submit;
        vm.addingInProcess = false;
        vm.addUserForm = true;
        
        activate();
        ////////////////

        function activate() { }

        function submit(user) {
            vm.addUserForm = false;
            vm.addingInProcess = true;
            vm.addingInProcess
            loginService.register(user)
                .then(function (result) {
                    if (result) {
                        vm.addingInProcess = false;
                        var loginData = { userName: user.userName, password: user.password };
                        auth.login(loginData).then(function (result) {
                            if(result){
                            $state.go('userManagment.activeMobile');
                            }
                        });
                    }
                    else {
                        vm.addingInProcess = false;
                        vm.addUserForm = true;
                    }
                });
        }
    }
})();