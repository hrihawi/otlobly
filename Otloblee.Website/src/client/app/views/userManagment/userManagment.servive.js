﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('userManagmentService', userManagmentService);

    userManagmentService.$inject = ['$http', '$location', 'common', 'exception'];
    /* @ngInject */
    function userManagmentService($http, $location, common, exception) {

        var logger = common.logger;

        var service = {
         getUserInfo: getUserInfo,
         checkActivationCode: checkActivationCode ,
         changeNumber: changeNumber
        };

        return service;

        
        function getUserInfo(userId) {
            return $http.get('api/account/get?id=' + userId)
                .then(getUserInfoComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting UserInfo')(message);
                });

            function getUserInfoComplete(data, status, headers, config) {
                return data.data.result;
            }
            //return common.$q.when(auth.user);
        }


        function checkActivationCode(data) {
              return $http.post('api/Account/ConfirmPhoneNumber', {
                  code: data
            })
                .then(checkActivationCodeComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for check Activation Code')(message);
                });

            function checkActivationCodeComplete(data, status, headers, config) {
                return data.data;

            }
        }
        function  changeNumber(number) {
              return $http.post('api/Account/UpdatePhoneNumber', {
                  phoneNumber: number
            })
                .then(checkActivationCodeComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for check Activation Code')(message);
                });

            function checkActivationCodeComplete(data, status, headers, config) {
                return data.data;

            }
        }
    }

})();
