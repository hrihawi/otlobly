(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('rateOrderController', rateOrderController);

    rateOrderController.$inject = ['profileService','$scope'];
    function rateOrderController(profileService,$scope) {
        var vm = this;
        vm.ratingValue = 1;
        vm.ratingNote = '';
        vm.send = sendRating;

        vm.orderId = $scope.ngDialogData.orderId;
        activate();
        ////////////////

        function activate() { }


        function sendRating() {
            profileService.sendRating({
                rate: vm.ratingValue,
                comment: vm.ratingNote,
                orderId: vm.orderId
            }).then(function () {
                  $scope.ngDialogData.success();
                   $scope.closeThisDialog()
            });



        }
    }
})();