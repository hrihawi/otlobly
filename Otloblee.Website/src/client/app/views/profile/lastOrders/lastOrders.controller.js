(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('lastOrdersController', lastOrdersController);

    lastOrdersController.$inject = ['profileService', 'ngDialog'];
    function lastOrdersController(profileService, ngDialog) {
        var vm = this;
        vm.items = [];
        vm.rating = rating
        activate();

        ////////////////

        function activate() {

            getLastOrders();
        }
        function rating(orderId) {
            ngDialog.open({
                template: 'app/views/profile/lastOrders/rateOrder/rateOrder.html',
                className: 'ngdialog-theme-default',
                controller:'rateOrderController',
                controllerAs: 'vm',
                data: {
                    orderId: orderId,
                    success: getLastOrders
                }

            });
        }
        function getLastOrders() {
            profileService.getLastOrders().then(function (result) {
                for (var i = 0; i > result.length; i++) { }
                vm.items = result;
            });

        }
    }
})();