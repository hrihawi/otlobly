(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'profile',
                config: {
                    url: '/profile',
                    data: {
                        access: access.public
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content': {
                            templateUrl: 'app/views/profile/profile.html',
                            controller: 'profileController',
                            controllerAs: 'vm'
                        }
                    }
                }
            },
            {
                state: 'profile.myInfo',
                config: {
                    url: '/myInfo',
                    data: {
                        access: access.public
                    },
                    views: {
                        'subContent': {
                            templateUrl: 'app/views/profile/myInfo/myInfo.html',
                            controller: 'myInfoController',
                            controllerAs: 'vm'
                        }
                    }
                }
            },
            {
                state: 'profile.myAddress',
                config: {
                    url: '/myAddress',
                    data: {
                        access: access.public
                    },
                    views: {
                        'subContent': {
                            templateUrl: 'app/views/profile/Address/Address.html',
                            controller: 'myAddressController',
                            controllerAs: 'vm'
                        }
                    }
                }
            },
            {
                state: 'profile.lastOrders',
                config: {
                    url: '/lastOrders',
                    data: {
                        access: access.public
                    },
                    views: {
                        'subContent': {
                            templateUrl: 'app/views/profile/lastOrders/lastOrders.html',
                            controller: 'lastOrdersController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }

            
        ];
    }
})();
