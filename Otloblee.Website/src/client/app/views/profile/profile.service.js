(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('profileService', profileService);

    profileService.$inject = ['$http', '$location', 'common', 'exception', 'auth'];
    /* @ngInject */
    function profileService($http, $location, common, exception, auth) {
        var logger = common.logger;
        var service = {
            getMyInfo: getMyInfo,
            getMyAddress: getAddress,
            getLastOrders: getLastOrders,
            updateMyInfo: updateMyInfo,
            sendRating: sendRating
        };

        return service;

        function getMyInfo(userId) {
            return $http.get('api/account/get?id=' + userId)
                .then(getUserInfoComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getUserInfoComplete(data, status, headers, config) {
                return data.data.result;
            }
            //return common.$q.when(auth.user);
        }

        function getAddress(userId) {
            return $http.get('api/account/get?id=' + userId)
                .then(getUserInfoComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getUserInfoComplete(data, status, headers, config) {
                return data.data.result.fullAddress;
            }
        }

        function getLastOrders() {
             return $http.get('api/order/search')
                  .then(getLastOrdersComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting get Last Orders')(message);
                         });

            function getLastOrdersComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function updateMyInfo(info) {
            return $http.post('api/account/update', info)
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function sendRating(info) {
             return $http.post('api/Rating/add', {
                 rate: info.rate,
                 comment: info.comment,
                 orderId: info.orderId
             })
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

    }
})();


