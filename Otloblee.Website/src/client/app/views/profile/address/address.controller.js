(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('myAddressController', myAddressController);

    myAddressController.$inject = ['profileService', 'auth'];
    function myAddressController(profileService, auth) {
        var vm = this;
        vm.editData = false;
        vm.submit = submit;
        activate();
        vm.user = null;
        ////////////////

        function activate() {

            profileService.getMyInfo(auth.user.id).then(function (result) {
                vm.user = result;
            });
        }

         function submit(user) {
            profileService.updateMyInfo(user).then(function (result) {
                vm.editData = false;
            });

        }
    }
})();