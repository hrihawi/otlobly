﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('headerController', headerController);

    headerController.$inject = ['auth','$timeout', 'messageService', 'layoutService','$state'];
    /* @ngInject */
    function headerController(auth,$timeout, messageService, layoutService,$state) {
        var vm = this;
        vm.logout = logout;
        activate();
        vm.sidebarToggle = layoutService.sidebarToggle;
        vm.layoutType = localStorage.getItem('ma-layout-status');
        vm.sidebarStat = function (event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                vm.sidebarToggle.left = false;
            }
        }
        function activate() {
        }


        function logout() {
            auth.logout();
            $state.go('main');
        }

    }
})();
