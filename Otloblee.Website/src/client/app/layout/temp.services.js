angular
      .module('app.layout')

   

    
    // =========================================================================
    // Todo List Widget Data
    // =========================================================================

    .service('todoService', ['$resource', function($resource){
        this.getTodo = function(todo) {
            var todoList = $resource("data/todo.json");
            
            return todoList.get({
                todo: todo
            });
        }
    }])


    // =========================================================================
    // Recent Items Widget Data
    // =========================================================================
    
    .service('recentitemService', ['$resource', function($resource){
        this.getRecentitem = function(id, name, price) {
            var recentitemList = $resource("data/recent-items.json");
            
            return recentitemList.get ({
                id: id,
                name: name,
                price: price
            })
        }
    }])


    // =========================================================================
    // Recent Posts Widget Data
    // =========================================================================
    
    .service('recentpostService', ['$resource', function($resource){
        this.getRecentpost = function(img, user, text) {
            var recentpostList = $resource("data/messages-notifications.json");
            
            return recentpostList.get ({
                img: img,
                user: user,
                text: text
            })
        }
    }])
  


    //// =========================================================================
    //// Malihu Scroll - Custom Scroll bars
    //// =========================================================================
    //.service('scrollService', function() {
    //    var ss = {};
    //    ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
    //        $(selector).mCustomScrollbar({
    //            theme: theme,
    //            scrollInertia: 100,
    //            axis:'yx',
    //            mouseWheel: {
    //                enable: true,
    //                axis: mousewheelaxis,
    //                preventDefault: true
    //            }
    //        });
    //    }
        
    //    return ss;
    //})


  