(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('footerController', footerController);

    footerController.$inject = ['$translate', 'cssInjector'];
    function footerController($translate, cssInjector) {
        var vm = this;
        vm.changeLanguage = changeLanguage;
        vm.selectedLangulage = $translate.use();
        activate();

        ////////////////

        function activate() {
        }

        function changeLanguage(lng) {
            $translate.use(lng);
            checkLanguage();
        }

        function checkLanguage() {
            if (vm.selectedLangulage == 'ar'){
                cssInjector.add("//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css");
                cssInjector.add("/css/rightToLeft.css");
            }else
             cssInjector.removeAll();
            
                
        }
    }
})();