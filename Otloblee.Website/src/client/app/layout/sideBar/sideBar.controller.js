﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('sideBarController', sideBarController);

    sideBarController.$inject = ['$scope', 'growlService','auth','$state'];
    /* @ngInject */
    function sideBarController($scope, growlService, auth, $state) {
        var vm = this;
        vm.logout = logout;
        activate();
        vm.user = {
            userName:auth.user.firstName + ' ' + auth.user.lastName,
            userimage: ''
        };
        function activate() {
          
        }
        function logout() {
         
            auth.logout();
            $state.go('loginApp');
        }

    }
})();
