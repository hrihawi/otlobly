﻿(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[app Error] ',
        appTitle: 'Otloblee',
        basePath: 'http://localhost/OtlobliApi/'

    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider'];
    /* @ngInject */
    function configure($logProvider, routerHelperProvider, exceptionHandlerProvider) {

        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }

        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({ docTitle: + ': ' });
    }

    var arabic = {
        Header: {
            Home: 'الصفحة الرئيسية',
            Restaurants: 'المطاعم',
            AboutUs: 'عن اطلبلي',
            Login: 'تسجيل الدخول',
            MyProfile: 'حسابي',
            Logout: 'تسجيل خروج'
        },
        Footer: {
            OtobleeWebsite: 'موقع اطلبلي',
            Home: 'الصفحة الرئيسية',
            AboutUs: 'حول اطلبلي',
            SignUp: 'تسجيل مستخدم جديد',
            Login: 'تسجيل دخول',
            ContactUs: 'اتصل بنا',
            OtlobleeApplications: 'تطبيقات اطلبلي',
            OtlobleeApplicationsComment: 'حمل تطبيقات اطلبلي الى جوالك واستخدمه بكل سهولة للطلب',
            Language: 'اللغات'
        },
        GeneralMessages:{
            OtlobleeFreeServices: 'خدمات اطلبلي المجانية',
            OtlobleeFreeServicesMsg:'خدمة مجانية لزوار موقع اطلبلي, يمكن استعراض جميع المطاعم المحيطة بك والطلب في اي وقت تشاء',
            MobileApplication:'تطبيقات الجوال',
            MobileApplicationMsg:'يمكنك تحميل تطبيق اطلبلي والدخول الى حسابك لارسال الطلبات الى المطاعم عن طريقه ببساطة وسهولة'
            
        },
        MainPage: {
            Otloblee: 'اطلبلي',
            OrderTakeawayorDeliveryFood: 'الاختيار عليك والطلب علينــا',
            SearchNotes: 'فقط اضغط على تحديد الموقع حتى نقوم بالبحث عن اقرب المطاعم اليك',
            Restaurant: 'مطعم',
            PeopleServed: 'اشخاص قامت بالطلب',
            RegisteredUsers: 'عدد مستخدمين الموقع',
            HowItWorks: 'كيف يتم الطلب',
            HowItWorksComment: 'احصل على الطعام الشهي إلى باب بيتك بـ 4 خطوات بسيطة',
            SearchByAddress: 'البحث عن المطاعم القريبة',
            SearchByAddressComment: 'سنعرض لك جميع المطاعم في منطقتك',
            ChooseARestaurant: 'اختيار مطعم',
            ChooseARestaurantComment: 'لدينا اكثر من 1000 مطعم اونلاين',
            RegisterAndKnowYou: 'تحديد عنوانك وطريقة الدفع',
            RegisterAndKnowYouComment: 'عرفنا عن نفسك بطريقة سهلة وسريعة جداً',
            Delivery: 'طلبك في طريقه اليك',
            DeliveryComment: 'طلبك قادم بسرعة اليك',
            TheMinutesThatUsuallyTakes: 'عدد الدقائق الافتراضية لتصل وجبتك اليك',
            ChooseFromMostPopular: 'المطاعم الاكثر تقييماً',
            ChooseFromMostPopularComment: 'المطاعم الاكثر شعبية حسب تقيماتكم',
            ChooseFromOverRestaurants: 'ابحث عن طلبك بين 1000 مطعم لدينا',
            ChooseFromOverRestaurantsComment: 'نبحث عن جميع المطاعم المحيطة بك والقريبة منك في اي مكان كنت',
            ViewAllRestaurants: 'عرض المطاعم',
            SearchButton: 'عرض المطاعم القريبة',

            DeliveryToAnyWhere: 'توصيل وجبتك الى اي مكان تريده',
            DeliveryToAnyWhereComment: 'هل انت في منزلك, عملك, مكان آخر. فقط قم باختيار وجبتك وموقع اطلبلي سيقوم بالطلب'

        },
        Restaurnts: {
            WeFoundThisAmountOfRestaurants: 'عدد المطاعم القريبة',
            WeFoundThisAmountOfRestaurantsComment: 'لقد اوجدنا المطاعم المحيطة بمنطقتك وبحسب التصنيف الموجود',
            NoMoreRestaurants:' عذراً لا يوجد مطاعم بالخيارات التي طلبتها في منطقتك,حاول مرة اخرى',
            Filter: 'تصفية',
            MenuIsNotReady:'القائمة غير متوفرة',
            ShowMap: 'عرض الخريطة',
            Distance: 'المسافة',
            FoodType: 'نوع الطعام',
            RestaurantType:'نوع المطعم',
            RestaurantIsOpen: 'المطعم مفتوح',
            RestaurantIsClose: 'المطعم مغلق حالياً',
            StartWorkAt: 'يبدأ العمل بـ  ',
            ViewMenu: 'عرض القائمة',
            SeeMore: 'عرض المزيد',
            LessValue: 'اقل قيمة للفاتورة',
            Pizza: 'بيتزا',
            FastFood: 'وجبات سريعة',
            Spaghetti: 'معكرونة',
            Grills: 'مشاوي',
            Drinks: 'مشروبات',
            Cooking: 'طبخ',
            Pastry: 'معجنات',
            All: 'الكل',
            Syria:'المطبخ السوري',
            Lebanon:'المطبخ اللبناني',
            Amirican:'الاصناف الاميركية',
            Jordan:'المطبخ الاردني',
            Egypt:'المطبخ المصري',
            Italia:'المطبخ الايطالي',
            India:'المطبخ الهندي',
            China:'المطبخ الصيني',
            Saudia: 'المطبخ السعودي',
            Menu: {
                DeliveryPrice: 'سعر التوصيل',
                FreeDeliveryIfTheBillIsHigherThan: 'توصيل مجاني في حال الفاتورة اعلى من ',
                BackToRestaurants: 'العودة الى المطاعم',
                RestaurantMenu: 'قائمة المطعم',
                Name: 'اسم الوجبة',
                Price: 'السعر',
                Add: 'اضافة',
                YourOrder: 'طلبك',
                Subtotal: 'المجموع الفرعي',
                Total: 'المجموع الكلي',
                ContinueOrder: 'اسمرار الطلب',
                SendOrder: 'ارسال الطلب',
                DeliveryProcess: 'وقت الطلب'
            },
            UserInformation: {
                UserInformation: 'معلوماتك الشخصية',
                FirstName: 'الاسم الاول',
                Surname: 'الكنية',
                MobileNumber: 'رقم الجوال',
                YourFullAddress: 'عنوانك المفصل',
                DoYouHaveAnyCommentsToTheRestaurant: 'هل لديك اي ملاحظات لارفاقها مع الطلب',

            },
            CheckOut: {
                PreferedPay: 'كيف تفضل ان تدفع؟',
                ATMCard: 'بطاقة أئتمان / بطاقة صراف',
                Cash: 'كاش',
                Coupone: 'كوبون',
                CheckCoupone: 'ادخال'

            },
            OrderHasSent: {
                OrderHasSent: 'تم ارسال الطلب بنجاح الى المطعم',
                ThankYou: 'شكراً لك',
                OrderHasSentComment: 'تم ارسال طلبك بنجاح الى المطعم, يرجى مراقبة حالة الطلب حتى تتم الموافقة عليه',


            },
            Information: 'معلومات',
            PaymentMethod: 'طريقة الدفع',
            DeliveryTheMeal: 'انتهى الطلب'


        },
        UserManagement: {
            WelcomeToNewUser: 'فريق موقع اطلبلي يرحب بك معنا',
            YouCanRegisterWithUsFree: 'يمكنك الحصول على جميع خدمات اطلبلي مجاناً, فقط قم بالسجيل معنا',
            AddNewUser: 'تسجيل مستخدم جديد',
            UserName: 'اسم المستخدم',
            FirstName: 'الاسم الاول',
            Surname: 'الكنية',
            Mobile: 'رقم الجوال',
            Email: 'البريد الالكتروني',
            Address: 'عنوانك',
            Password: 'كلمة المرور',
            ConfirmPassword: 'تأكيد كلمة المرور',
            Code: 'الرمز',
            ResendCode: 'اعادة ارسال الرمز',
            MobileActivationPage: 'تفعيل هاتفك الجوال',
            LoginPage: 'تسجيل الدخول الى الموقع',
                NewUser:' مستخدم جديد',
            Login:'تسجيل دخول',
            SignUp:'ارسال الطلب'
            
        },
        General: {
            Cash: 'كاش',
            AtmCard: 'بطاقة مصرف',
            OtlobleeOnline: 'اطلبلي أونلاين'

        },
        State: {
            Canceled: 'تم الغاء الطلب',
            Created: 'طلب جديد',
            Confirmed: 'تم قبول الطلب',
            ReceivedByCustomer: 'تم توصيل الطلب',
        },
        Position: {
            MainPage: 'الصفحة الرئيسية',
            MyProfile: 'حسابي',
            Restaurants: 'المطاعم',
            Menu: 'قائمة الطعام',
            UserInformation: 'معلومات المستخدم',
            PaymentMethod: 'طريقة الدفع',
            NewUser: 'مستخدم جديد',
            Login: 'تسجيل دخول'
        },
        Profile: {
            Welcome: 'اهلاً بك في موقع اطلبلي اونلاين',
            WelcomeComment: 'يمكنك ان تقوم بتعديل معلوماتك الشخصية او مراقبة طلبك',
            YourInfo: 'معلوماتك',
            FirstName: 'الاسم الاول',
            Surname: 'الكنية',
            Email: 'البريد الالكتروني',
            Mobile: 'رقم الجوال',
            Save: 'حفظ',
            Update: 'تعديل',
            MyInfo: 'معلوماتي',
            MyAddress: 'عنواني',
            MyAddressComment: 'يرجى التأكد انك تستخدم عنوانك الكامل والصحيح',
            ReferanceCode: 'الرقم المرجعي',
            Order: 'الطلب',
            TotalBill: 'قيمة الفاتورة',
            RestaurantName: 'اسم المطعم',
            OrderTime: 'وقت الطلب',
            ConfirmedTime: 'وقت الموافقة',
            Address: 'العنوان',
            PaymentType: 'طريقة الدفع',
            OrderState: 'حالة الطلب',
            Raiting: 'التقييم',
            OrderRating: 'تقيم الطلب',
            MyOrders: 'طلباتي',
            YourOpinian: 'رأيك ي هذا الطلب',
            Send: 'ارسال',
            YourOrder:'تقييم طلبك',
            Rating:'تقييم'
        }
    };

    var english = {
        Header: {
            Home: 'Home',
            Restaurants: 'Restaurants',
            AboutUs: 'AboutUs',
            Login: 'Login',
            MyProfile: 'MyProfile',
            Logout: 'Logout'
        },
        Footer: {
            OtobleeWebsite: 'Otoblee Website',
            Home: 'Home Page',
            AboutUs: 'About Us',
            SignUp: 'Sign Up',
            Login: 'Login',
            ContactUs: 'Contact Us',
            OtlobleeApplications: 'Otloblee Applications',
            OtlobleeApplicationsComment: 'Download Otloblee Application to your mobile device',
            Language: 'Languages'
        },
            GeneralMessages:{
            OtlobleeFreeServices: 'Free Services',
            OtlobleeFreeServicesMsg:'Otloblee services is totaly free, you can order as much as you want in any time',
            MobileApplication:'Mobile Application',
            MobileApplicationMsg:'You can install Otloblee Application on your mobile phone and make orders in very simple way'
        },
        MainPage: {
            Otloblee: 'Otloblee',
            OrderTakeawayorDeliveryFood: 'We Will Order For You',
            SearchNotes: 'Plase share your location to find all nearby restaurants',
            Restaurant: 'Restaurant',
            PeopleServed: 'People Served',
            RegisteredUsers: 'Registered Users',
            HowItWorks: 'How It Works',
            HowItWorksComment: 'For just 4 steps you will receive your order to your place',
            SearchByAddress: 'Find Nearby Restaurants',
            SearchByAddressComment: 'Show all the nearby restaurants',
            ChooseARestaurant: 'Choose a restaurant',
            ChooseARestaurantComment: 'We have more than 1000 restaurants online',
            RegisterAndKnowYou: 'Register or access your account',
            RegisterAndKnowYouComment: 'So simple way to regester or access your account',
            Delivery: 'Delivery',
            DeliveryComment: 'Your order on the way!!',
            TheMinutesThatUsuallyTakes: 'The Minutes That Usually Takes',
            ChooseFromMostPopular: 'Most Popular Restaurants',
            ChooseFromMostPopularComment: 'Choose From Most Popular',
            ChooseFromOverRestaurants: 'Easy and simple ',
            ChooseFromOverRestaurantsComment: 'Find what you want over more than 1000 restaurants online',
            ViewAllRestaurants: 'Show Restaurants',
            SearchButton: 'Find nearby restaurants',
            DeliveryToAnyWhere: 'Deliver meals to anywhere you want',
            DeliveryToAnyWhereComment: 'Are you in your home?!, your business!! elsewhere!!. Just choose your meal and let otloblee make order for you'

        },
        Restaurnts: {
            WeFoundThisAmountOfRestaurants: 'Number of nearby restaurants',
            WeFoundThisAmountOfRestaurantsComment: 'We Found This Amount Of Restaurants',
            NoMoreRestaurants:'Sorry, there are no restaurants in your area with your specification filter',
            Filter: 'Filter',
            MenuIsNotReady:'Menu is not avalible',
            ShowMap: 'Show Map',
            Distance: 'Distance',
            FoodType: 'Food Type',
            RestaurantType:'Restaurant Type',
            RestaurantIsOpen: 'Restaurant Is Open',
            RestaurantIsClose: 'Restaurant Is Close',
            StartWorkAt: 'Start Work At',
            ViewMenu: 'View Menu',
            SeeMore: 'See More',
            LessValue: 'less bill value',
            Pizza: 'Pizza',
            FastFood: 'FastFood',
            Spaghetti: 'Spaghetti',
            Grills: 'Grills',
            Drinks: 'Drinks',
            Cooking: 'Cooking',
            Pastry: 'Pastry',
            All: 'All',
            Saudia:'Saudi Kitchen',
             Syria:'Syrian Kuisine',
            Lebanon:'Lebanese cuisine',
            Amirican:'American Kitchen',
            Jordan:'Jordanian cuisine',
            Egypt:'Egyptian cuisine',
            Italia:'Italian kitchen',
            India:'Italian kitchen',
            China:'Chinese Cuisine',
            Menu: {
                DeliveryPrice: 'Delivery Price',
                FreeDeliveryIfTheBillIsHigherThan: 'Free Delivery If The Bill Is Higher Than',
                BackToRestaurants: 'Back To Restaurants',
                RestaurantMenu: 'Restaurant Menu',
                Name: 'Meal Name',
                Price: 'Price',
                Add: 'Add',
                YourOrder: 'Your Order',
                Subtotal: 'Sub total',
                Total: 'Total',
                ContinueOrder: 'Continue Order',
                SendOrder: 'Send Order',
                DeliveryProcess: 'Delivery Process'
            },
            UserInformation: {
                UserInformation: 'User Information',
                FirstName: 'First Name',
                Surname: 'Surname',
                MobileNumber: 'Mobile Number',
                YourFullAddress: 'Your Full Address',
                DoYouHaveAnyCommentsToTheRestaurant: 'Do You Have Any Comments To The Restaurant',

            },
            CheckOut: {
                PreferedPay: 'How do you prefer to pay?',
                ATMCard: 'Cradit card / ATM card',
                Cash: 'Cash',
                Coupone: 'Coupone',
                CheckCoupone: 'Enter Coupone'


            },
            OrderHasSent: {
                OrderHasSent: 'Order Has Been Sent  Successfuly',
                ThankYou: 'Thank You',
                OrderHasSentComment: 'Your order has been sent successfully, please check your order until confirmed by the restaurant.',


            },
            Information: 'Information',
            PaymentMethod: 'Payment Method',
            DeliveryTheMeal: 'Finish'
        },
        UserManagement: {
            WelcomeToNewUser: 'Otloblee Team  Welcomes You Join Us',
            YouCanRegisterWithUsFree: 'You can get all website services for free, just sign up',
            AddNewUser: 'Sign up',
            UserName: 'UserName',
            FirstName: 'First Name',
            Surname: 'Surname',
            Mobile: 'Mobile Numner',
            Email: 'Email',
            Address: 'Address',
            Password: 'Password',
            ConfirmPassword: 'ConfirmPassword',
            Code: 'Code',
            ResendCode: 'Resend Code',
            MobileActivationPage: 'Mobile Activation Page',
            LoginPage: 'Login Page',
            NewUser:'New User',
            Login:'Login',
            SignUp:'Sign Up'
        },
        General: {
            Cash: 'Cash',
            AtmCard: 'Atm Card',
            OtlobleeOnline: 'Otloblee Online'
        },
        State: {
            Canceled: 'Canceled',
            Created: 'New Order',
            Confirmed: 'Confirmed',
            ReceivedByCustomer: 'You Received',
        },
        Position: {
            MainPage: 'Home Page ',
            MyProfile: 'My Profile',
            Restaurants: 'Restaurants',
            Menu: 'Menu',
            UserInformation: 'User Information',
            PaymentMethod: 'Payment Method',
            NewUser: 'NewUser',
            Login: 'Login'
            
        },
        Profile: {
            Welcome: 'Welcome To Otloblee Online',
            WelcomeComment: 'You can here preview your information and update it.tracking your order',
            YourInfo: 'Your Information',
            FirstName: 'FirstName',
            Surname: 'SurName',
            Email: 'Email',
            Mobile: 'Mobile',
            Save: 'Save',
            Update: 'Update',
            MyInfo: 'My Info',
            MyAddress: 'My Address',
            MyAddressComment: 'Please be sure you already add your full address to use it in your order',
            ReferanceCode: 'Referance Code',
            Order: 'Order',
            TotalBill: 'Total Bill',
            RestaurantName: 'Restaurant Name',
            OrderTime: 'Order Time',
            ConfirmedTime: 'Confirmed Time',
            Address: 'Address',
            PaymentType: ' Payment Type',
            OrderState: 'Order State',
            Raiting: 'Raiting',
            OrderRating: 'Order Rating',
            MyOrders: 'My Orders',
            YourOpinian: 'Your Opinian',
            Send: 'Send',
            YourOrder:'Your order',
            Rating:'Rating'
        }


    };

    core.config(['$translateProvider', function ($translateProvider) {
        // add translation table
        $translateProvider.translations('ar', arabic);
        $translateProvider.translations('en', english);
        var browserLanguage = navigator.language.split('-');
        $translateProvider.preferredLanguage(browserLanguage[0]);

    }]);

    core.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.spinnerTemplate = '<div id="preloader" "><div class="spinner2">  <div class="rect1"></div>  <div class="rect2"></div>  <div class="rect3"></div>  <div class="rect4"></div>  <div class="rect5"></div></div></div>';
    }])

    core.config(function (cssInjectorProvider) {
        cssInjectorProvider.setSinglePageMode(false);
    });

})();
