(function () {
    'use strict';

    angular
        .module('app.core', ['pascalprecht.translate',
            'ngAnimate', 
            'blocks.exception', 'blocks.logger', 'blocks.router', 'blocks.auth','ngCookies',
            'ui.router', 'angular-loading-bar', 'ngMap','angular.css.injector'
        ]);

})();

   