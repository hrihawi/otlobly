//////////////////////////////  /////////////////////////////////////
Request:
---------------------------------------------------------------------

Response:
---------------------------------------------------------------------



////////////////////////////// Login /////////////////////////////////////
Request:
---------------------------------------------------------------------
POST: http://localhost/OtlobliApi/oauth/token
username : OtlobliAdmin
password : ~Qwe123456
grant_type : password

Response:
---------------------------------------------------------------------
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1laWQiOiI0OTRmZjA0NS05MTg3LTQ0NTYtYTQ1My1iYTkyMGM3ZmQ4ZmEiLCJ1bmlxdWVfbmFtZSI6Imhhc2FuIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS9hY2Nlc3Njb250cm9sc2VydmljZS8yMDEwLzA3L2NsYWltcy9pZGVudGl0eXByb3ZpZGVyIjoiQVNQLk5FVCBJZGVudGl0eSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiOTg2NzRjMTMtNGE5MC00YTU5LTg1MTAtMzYyZDFmOGEyOTVjIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdC9PdGxvYmxpQXBpIiwiYXVkIjoiNDE0ZTE5MjdhMzg4NGY2OGFiYzc5ZjcyODM4MzdmZDEiLCJleHAiOjE0NTkwMTUzNTMsIm5iZiI6MTQ1ODkyODk1M30.0d3DeDzqZ7nZ0EWaAwAUQ4trM38yyHLecJGgZ1AwJp8",
  "token_type": "bearer",
  "expires_in": 86399
}


////////////////////////////// users /////////////////////////////////////
Request:
---------------------------------------------------------------------
GET: http://localhost/OtlobliApi/api/account/Search

Response:
---------------------------------------------------------------------
[
  {
    "url": null,
    "id": "494ff045-9187-4456-a453-ba920c7fd8fa",
    "userName": "WebsiteAdmin",
    "fullName": "Admin Admin",
    "email": "admin@otlobli.com",
    "emailConfirmed": true,
    "level": 3,
    "joinDate": "2016-03-25T00:00:00",
    "roles": [],
    "claims": []
  },
]



////////////////////////////// Create /////////////////////////////////////
Request:
---------------------------------------------------------------------
POST: http://localhost/OtlobliApi/api/account/Add
{
    "Email": "h1@h.com",
    "Username": "hasan1",
    "FirstName": "hasan",
    "LastName": "rihawi",
    "RoleName": "WebsiteAdmin",
    "Password": "~Qwe123456",
    "ConfirmPassword": "~Qwe123456",
  }

Response:
---------------------------------------------------------------------
{
  "url": null,
  "id": "91cd0a82-54f2-42ce-972d-cba50f80813b",
  "userName": "hasan1",
  "fullName": "hasan rihawi",
  "email": "h1@h.com",
  "emailConfirmed": false,
  "level": 3,
  "joinDate": "2016-03-25T00:00:00+02:00",
  "roles": [],
  "claims": []
}


////////////////////////////// Add Token to Request Header /////////////////////////////////////
Request:
---------------------------------------------------------------------
Hearders:
Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1laWQiOiI0OTRmZjA0NS05MTg3LTQ0NTYtYTQ1My1iYTkyMGM3ZmQ4ZmEiLCJ1bmlxdWVfbmFtZSI6Imhhc2FuIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS9hY2Nlc3Njb250cm9sc2VydmljZS8yMDEwLzA3L2NsYWltcy9pZGVudGl0eXByb3ZpZGVyIjoiQVNQLk5FVCBJZGVudGl0eSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiOTg2NzRjMTMtNGE5MC00YTU5LTg1MTAtMzYyZDFmOGEyOTVjIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdC9PdGxvYmxpQXBpIiwiYXVkIjoiNDE0ZTE5MjdhMzg4NGY2OGFiYzc5ZjcyODM4MzdmZDEiLCJleHAiOjE0NTkwMTc2MTQsIm5iZiI6MTQ1ODkzMTIxNH0.iipbYunFvpLu7khzaDTmvOl37JvWaxFQTDRBK2z8AiA
Content-Type  : application/json
