﻿using System;
using System.Data.Entity;
using System.Linq;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class WebsiteInfoService
    {
        public PagedListResult<WebsiteInfo> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<WebsiteInfo, WebsiteInfo>();
            return productRepository.Search(query);
        }

        private static SearchQuery<WebsiteInfo> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<WebsiteInfo>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart
            };


            return query;
        }

        public WebsiteInfo GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.WebsiteInfos.SingleOrDefault(it => it.Id == getCriteria.Id);
            }
        }

        public void AddNew(WebsiteInfo websiteInfo)
        {
            using (var context = new ApplicationDbContext())
            {
                context.WebsiteInfos.Add(websiteInfo);
                context.SaveChanges();
            }
        }

        public void Update(WebsiteInfo websiteInfo)
        {
            using (var context = new ApplicationDbContext())
            {
                context.WebsiteInfos.Attach(websiteInfo);
                context.Entry(websiteInfo).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.WebsiteInfos.SingleOrDefault(it => it.Id == id);
                context.WebsiteInfos.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
