﻿using System;
using System.Data.Entity;
using System.Linq;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class OrderService
    {
        public PagedListResult<OrderForReturn> Search(SearchCriteria searchCriteria, Guid userId)
        {
            var query = getQueryFromSearchCriteria(searchCriteria, userId);
            var productRepository = new Repository<Order, OrderForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Order> getQueryFromSearchCriteria(SearchCriteria searchCriteria, Guid userId)
        {
            var query = new SearchQuery<Order>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "Restaurant, User, Coupon, OrderDetails.Meal.Images, Ratings.User",
            };
            query.AddSortCriteria(new ExpressionSortCriteria<Order, DateTime>
                                  (x => x.CreatedDateTime,
                                   SortDirection.Descending));

            if (searchCriteria.SpecificPeriodValue > 0)
            {
                if (searchCriteria.SpecificPeriodUnit == PeriodUnits.Day)
                    query.AddFilter(x => x.CreatedDateTime > DateTime.UtcNow.AddDays(-searchCriteria.SpecificPeriodValue));
                else if (searchCriteria.SpecificPeriodUnit == PeriodUnits.Hour)
                    query.AddFilter(x => x.CreatedDateTime > DateTime.UtcNow.AddHours(-searchCriteria.SpecificPeriodValue));
                else if (searchCriteria.SpecificPeriodUnit == PeriodUnits.Min)
                    query.AddFilter(x => x.CreatedDateTime > DateTime.UtcNow.AddMinutes(-searchCriteria.SpecificPeriodValue));
            }


            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.Include("Roles").First(x => x.Id == userId);
                var roleId = user.Roles.First().RoleId;
                var userRole = context.Roles.First(x => x.Id == roleId);
                if (userRole.Name == RolesNames.User)
                    query.AddFilter(order => order.User.Id == userId);
                else if (userRole.Name == RolesNames.RestaurantAdmin 
                    || userRole.Name == RolesNames.RestaurantManager)
                {
                    var restaurantsOwnedByThisManager =
                        context.Restaurants.Include("Managers").Where(x => x.Managers.Any(m => m.Id == userId))
                        .Select(r =>  r.Id );

                    query.AddOrFilter(restaurant => restaurant.Id == Guid.Empty);
                    foreach (var resId in restaurantsOwnedByThisManager)
                        query.AddOrFilter(order => order.Restaurant.Id == resId);
                }
            }

            return query;
        }

        public OrderForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.Orders.Include("User").Include("Restaurant").Include("Coupon")
                    .Include("OrderDetails.Meal.Images").Include("Ratings.User")
                    .SingleOrDefault(it => it.Id == getCriteria.Id);
                if (item == null)
                    throw new NotFoundException("Specified Order is not found");

                var orderForReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<OrderForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<OrderForReturn>(item);

                orderForReturn.SubTotalPrice = orderForReturn.OrderDetails.Sum(x => x.Price * x.Count);
                orderForReturn.TotalPrice = orderForReturn.SubTotalPrice + orderForReturn.DeliveryFees;
                if (item.Coupon != null)
                    orderForReturn.TotalPrice -= item.Coupon.DiscountValue;

                return orderForReturn;
            }
        }

        public OrderForReturn AddNew(OrderForAdd orderForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var order = AutoMapping.DefaultInstanse.Map<Order>(orderForAdd);

                foreach (var orderDetailForAdd in orderForAdd.OrderDetails)
                {
                    var orderDetail = AutoMapping.DefaultInstanse.Map<OrderDetail>(orderDetailForAdd);
                    var meal = context.Meals.Single(x => x.Id == orderDetailForAdd.MealId);
                    orderDetail.Order = order;
                    orderDetail.Meal = meal;
                    orderDetail.Price = meal.Price;
                    order.OrderDetails.Add(orderDetail);
                }

                var subTotalPrice = order.OrderDetails.Sum(x => x.Price * x.Count);

                if (!string.IsNullOrWhiteSpace(orderForAdd.CouponCode))
                {
                    var coupon = context.Coupons.Include("Restaurant").FirstOrDefault(r => r.Code == orderForAdd.CouponCode);

                    if (coupon == null)
                        throw new BadRequestException("Specified Coupon is not valid");

                    if (coupon.Restaurant.Id != orderForAdd.RestaurantId)
                        throw new BadRequestException("Specified Coupon is not valid with current resturant");

                    if (coupon.HitsCount >= coupon.Limit)
                        throw new BadRequestException("Specified Coupon exceeded the max limit");

                    if (subTotalPrice < coupon.MinimumBillValue)
                        throw new BadRequestException("Order Price less than Minimum Order Value");

                    coupon.LastHitDateTime = DateTime.UtcNow;
                    coupon.HitsCount += 1;

                    context.Coupons.Attach(coupon);
                    context.Entry(coupon).State = EntityState.Modified;

                    order.Coupon = coupon;

                }

                var rest = context.Restaurants.Single(r => r.Id == orderForAdd.RestaurantId);
                order.Restaurant = rest;

                var user = context.Users.Single(r => r.Id == orderForAdd.UserId);
                order.User = user;

                context.Orders.Add(order);
                context.SaveChanges();

                var orderForReturn = AutoMapping.DefaultInstanse.Map<OrderForReturn>(order);
                orderForReturn.SubTotalPrice = subTotalPrice;

                if (orderForReturn.SubTotalPrice < rest.MinAmountForFreeDelivery)
                {
                    orderForReturn.DeliveryFees = rest.DeliveryFees;
                    orderForReturn.TotalPrice = subTotalPrice + orderForReturn.DeliveryFees;
                }

                if (order.Coupon != null)
                    orderForReturn.TotalPrice -= order.Coupon.DiscountValue;

                return orderForReturn;
            }
        }

        public void Update(OrderForReturn orderForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var order = AutoMapping.DefaultInstanse.Map<Order>(orderForReturn);

                var rest = context.Restaurants.Single(r => r.Id == orderForReturn.RestaurantId);
                order.Restaurant = rest;

                var user = context.Users.Single(r => r.Id == orderForReturn.UserId);
                order.User = user;

                context.Orders.Attach(order);
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public OrderForReturn UpdateStatus(OrderForReturnSummary orderForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var order = context.Orders.Include("Restaurant").Include("User").FirstOrDefault(x => x.Id == orderForReturn.Id);
                if (order == null)
                    throw new NotFoundException();

                order.State = orderForReturn.State;
                context.Orders.Attach(order);
                context.Entry(order).State = EntityState.Modified;
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<OrderForReturn>(order);
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Orders.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                context.Orders.Remove(itemToDelete);
                context.SaveChanges();
                return true;
            }
        }
    }
}
