﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class SettingService
    {
        public PagedListResult<Setting> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Setting,Setting>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Setting> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Setting>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart
            };


            return query;
        }

        public Setting GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Settings.SingleOrDefault(it => it.Id == getCriteria.Id);
            }
        }

        public void AddNew(Setting Setting)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Settings.Add(Setting);
                context.SaveChanges();
            }
        }

        public void Update(Setting Setting)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Settings.Attach(Setting);
                context.Entry(Setting).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Settings.SingleOrDefault(it => it.Id == id);
                context.Settings.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
