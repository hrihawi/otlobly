﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class OrderDetailService
    {
        public PagedListResult<OrderDetailForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<OrderDetail,OrderDetailForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<OrderDetail> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<OrderDetail>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "Order, Meal",
            };

            return query;
        }

        public OrderDetailForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.OrderDetails.Include("Order").Include("Meal").SingleOrDefault(it => it.Id == getCriteria.Id);
                return getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<OrderDetailForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<OrderDetailForReturn>(item);
            }
        }

        public void AddNew(OrderDetailForAdd orderForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var orderDetail = AutoMapping.DefaultInstanse.Map<OrderDetail>(orderForAdd);

                var meal = context.Meals.Single(x => x.Id == orderForAdd.MealId);
                orderDetail.Meal = meal;

                var order = context.Orders.Single(x => x.Id == orderForAdd.OrderId);
                orderDetail.Order = order;

                context.OrderDetails.Add(orderDetail);
                context.SaveChanges();
            }
        }

        public void Update(OrderDetailForReturn orderDetailForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var orderDetail = AutoMapping.DefaultInstanse.Map<OrderDetail>(orderDetailForReturn);

                var meal = context.Meals.Single(x => x.Id == orderDetailForReturn.MealId);
                orderDetail.Meal = meal;

                var order = context.Orders.Single(x => x.Id == orderDetailForReturn.OrderId);
                orderDetail.Order = order;

                context.OrderDetails.Attach(orderDetail);
                context.Entry(orderDetail).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.OrderDetails.SingleOrDefault(it => it.Id == id);
                context.OrderDetails.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
