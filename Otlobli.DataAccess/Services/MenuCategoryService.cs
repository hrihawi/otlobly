﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class MenuCategoryService
    {
        public PagedListResult<MenuCategoryForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<MenuCategory,MenuCategoryForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<MenuCategory> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<MenuCategory>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart
            };


            return query;
        }

        public MenuCategoryForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.MenuCategories.SingleOrDefault(it => it.Id == getCriteria.Id);
                return getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<MenuCategoryForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<MenuCategoryForReturn>(item);
            }
        }

        public MenuCategoryForReturn AddNew(MenuCategoryForAdd menuCategoryForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var menuCat = AutoMapping.DefaultInstanse.Map<MenuCategory>(menuCategoryForAdd);
                context.MenuCategories.Add(menuCat);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<MenuCategoryForReturn>(menuCat);
            }
        }

        public void Update(MenuCategoryForReturn menuCategoryForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var menuCategory = AutoMapping.DefaultInstanse.Map<MenuCategory>(menuCategoryForReturn);
                context.MenuCategories.Attach(menuCategory);
                context.Entry(menuCategory).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.MenuCategories.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.MenuCategories.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
