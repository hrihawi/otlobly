﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class ImageForRestaurantService
    {
        public PagedListResult<ImageForRestaurantForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria<ImageForRestaurantForReturn>(searchCriteria);
            var productRepository = new Repository<ImageForRestaurant, ImageForRestaurantForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<ImageForRestaurant> getQueryFromSearchCriteria<T>(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<ImageForRestaurant>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
            };

            if (searchCriteria.RestaurantId != Guid.Empty)
                query.AddFilter(restaurant => restaurant.Id == searchCriteria.RestaurantId);

            return query;
        }

        public ImageForRestaurant GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.ImagesForRestaurant.SingleOrDefault(it => it.Id == getCriteria.Id);
            }
        }

        public ImageForRestaurantForReturn Add(ImageForRestaurantForAdd imageForAdd)
        {
            imageForAdd.Data = imageForAdd.Data.ResizeImage();
            using (var context = new ApplicationDbContext())
            {
                var img = AutoMapping.DefaultInstanse.Map<ImageForRestaurant>(imageForAdd);
                var rest = context.Restaurants.Single(r=> r.Id == imageForAdd.RestaurantId);
                if (rest != null)
                    rest.Images.Add(img);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<ImageForRestaurantForReturn>(img);
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.ImagesForRestaurant.Single(it => it.Id == id);
                context.ImagesForRestaurant.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
