﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class DeviceService
    {
        public PagedListResult<DeviceForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Device,DeviceForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Device> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Device>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart
            };

            if (searchCriteria.UserId != Guid.Empty)
                query.AddFilter(x => x.User.Id == searchCriteria.UserId);

            return query;
        }

        public Device GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Devices.SingleOrDefault(it => it.Id == getCriteria.Id);
            }
        }

        public void AddNew(DeviceForAdd deviceForAdd, Guid userId)
        {
            using (var context = new ApplicationDbContext())
            {
                var oldDevice = context.Devices.FirstOrDefault(x => x.Key == deviceForAdd.Key);
                var user = context.Users.FirstOrDefault(x => x.Id == userId);

                if (oldDevice != null)
                {
                    oldDevice.User = user;
                    oldDevice.DeviceName = deviceForAdd.DeviceName;
                    oldDevice.OsType = deviceForAdd.OsType;

                    context.Devices.Attach(oldDevice);
                    context.Entry(oldDevice).State = EntityState.Modified;
                }
                else
                {
                    var device = AutoMapping.DefaultInstanse.Map<Device>(deviceForAdd);
                    device.User = user;
                    context.Devices.Add(device);
                }

                context.SaveChanges();
            }
        }

        public void Update(DeviceForReturn deviceForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var device = AutoMapping.DefaultInstanse.Map<Device>(deviceForReturn);

                context.Devices.Attach(device);
                context.Entry(device).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Devices.SingleOrDefault(it => it.Id == id);
                context.Devices.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
