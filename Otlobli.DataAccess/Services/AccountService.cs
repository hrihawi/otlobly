﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class AccountService
    {
        public void Update(UserForReturn userForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var mappedUser = AutoMapping.DefaultInstanse.Map<User>(userForReturn);

                var oldItem = context.Users.First(r => r.Id == mappedUser.Id);
                mappedUser.SecurityStamp = oldItem.SecurityStamp;
                mappedUser.PasswordHash = oldItem.PasswordHash;

                if (oldItem.PhoneNumber != mappedUser.PhoneNumber)
                    mappedUser.PhoneNumberConfirmed = false;

                if (oldItem.Email != mappedUser.Email)
                    mappedUser.EmailConfirmed = false;

                context.Entry(oldItem).CurrentValues.SetValues(mappedUser);

                context.Users.Attach(oldItem);
                context.Entry(oldItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void UpdateStatus(UserStatusForAdd status)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Users.Single(r => r.Id == status.UserId);
                itemToUpdate.IsActive = status.IsActive;
                context.Users.Attach(itemToUpdate);
                context.Entry(itemToUpdate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public User GetUserById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.Include("Restaurant").FirstOrDefault(x => x.Id == id);
                return user;
            }
        }
    }
}
