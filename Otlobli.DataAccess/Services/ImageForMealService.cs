﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class ImageForMealService
    {
        public PagedListResult<ImageForMealForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria<ImageForMealForReturn>(searchCriteria);
            var productRepository = new Repository<ImageForMeal, ImageForMealForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<ImageForMeal> getQueryFromSearchCriteria<T>(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<ImageForMeal>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
            };
            return query;
        }


        public ImageForMeal GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.ImagesForMeal.SingleOrDefault(it => it.Id == getCriteria.Id);
            }
        }

        public ImageForMealForReturn Add(ImageForMealForAdd imageForAdd)
        {
            imageForAdd.Data = imageForAdd.Data.ResizeImage();
            var img = AutoMapping.DefaultInstanse.Map<ImageForMeal>(imageForAdd);
            using (var context = new ApplicationDbContext())
            {
                var itemParent = context.Meals.Single(r => r.Id == imageForAdd.MealId);
                if (itemParent != null)
                    itemParent.Images.Add(img);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<ImageForMealForReturn>(img);
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.ImagesForMeal.Single(it => it.Id == id);
                context.ImagesForMeal.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
