﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Device.Location;
using System.Linq;
using Otlobli.Model;
using Z.EntityFramework.Plus;

namespace Otlobli.DataAccess
{
    public class RestaurantService
    {
        public PagedListResult<RestaurantForReturn> Search(SearchCriteria searchCriteria, string userName, string role)
        {
            if (role == RolesNames.WebsiteManager)
                searchCriteria.CreatedByUser = userName;
            var query = getQueryFromSearchCriteria<RestaurantForReturn>(searchCriteria);
            var productRepository = new Repository<Restaurant, RestaurantForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Restaurant> getQueryFromSearchCriteria<T>(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<Restaurant>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
                IncludeProperties = "RestaurantType, FoodType, Country, City, Images," +
                                    " Menus.Meals.Images, Menus.Category," +
                                    " Managers.Roles, Ratings, Managers",
            };

            if (!string.IsNullOrWhiteSpace(searchCriteria.GpsLat) && !string.IsNullOrWhiteSpace(searchCriteria.GpsLng))
            {
                var currentCoord = new GeoCoordinate(Convert.ToDouble(searchCriteria.GpsLat), Convert.ToDouble(searchCriteria.GpsLng));
                using (var context = new ApplicationDbContext())
                {
                    var allResturants = context.Restaurants.Select(
                        p => new { Id = p.Id, GpsLat = p.GpsLat, GpsLng = p.GpsLng }).ToList();
                    int maxDistance = string.IsNullOrWhiteSpace(searchCriteria.GpsDistance)
                        ? 5 : Convert.ToInt32(searchCriteria.GpsDistance);

                    query.AddOrFilter(restaurant => restaurant.Id == Guid.Empty);

                    foreach (var item in allResturants)
                    {
                        try
                        {
                            var resCoord = new GeoCoordinate(Convert.ToDouble(item.GpsLat), Convert.ToDouble(item.GpsLng));
                            // distanse in km between the user location and the current resturant
                            var distanse = currentCoord.GetDistanceTo(resCoord) / 1000;
                            if (distanse <= maxDistance)
                                query.AddOrFilter(restaurant => restaurant.Id == item.Id);
                        }
                        catch (Exception) { }
                    }
                }
            }

            query.AddFilter(restaurant => restaurant.IsActive);

            if (!string.IsNullOrWhiteSpace(searchCriteria.RestaurantType))
                query.AddFilter(restaurant => restaurant.RestaurantType.NameArabic == searchCriteria.RestaurantType
                || restaurant.RestaurantType.NameEnglish == searchCriteria.RestaurantType);

            if (!string.IsNullOrWhiteSpace(searchCriteria.CategoryType))
                query.AddFilter(restaurant => restaurant.Menus.Any(x =>
                x.Category.NameArabic == searchCriteria.CategoryType
                || x.Category.NameEnglish == searchCriteria.CategoryType));

            if (searchCriteria.PriceRangeMin.HasValue)
                query.AddFilter(restaurant => restaurant.DeliveryFees > searchCriteria.PriceRangeMin.Value);

            if (searchCriteria.PriceRangeMax.HasValue)
                query.AddFilter(restaurant => restaurant.DeliveryFees < searchCriteria.PriceRangeMax.Value);

            if (!string.IsNullOrWhiteSpace(searchCriteria.CreatedByUser))
                query.AddFilter(restaurant => restaurant.CreatedByUser == searchCriteria.CreatedByUser);

            if (!string.IsNullOrWhiteSpace(searchCriteria.AnyWhere))
            {
                query.AddFilter(restaurant =>
                       restaurant.Country.NameArabic.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.Country.NameEnglish.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.DescriptionArabic.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.DescriptionEnglish.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.FullAddressArabic.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.FullAddressEnglish.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.NameArabic.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.NameEnglish.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.PhoneNumber1.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.PhoneNumber2.ToLower().Contains(searchCriteria.AnyWhere.ToLower())
                    || restaurant.Website.ToLower().Contains(searchCriteria.AnyWhere.ToLower()));
            }
            return query;
        }

        public PagedListResult<RestaurantForReturnSummary> SearchSummary(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria<RestaurantForReturnSummary>(searchCriteria);
            var productRepository = new Repository<Restaurant, RestaurantForReturnSummary>();
            return productRepository.Search(query);
        }

        public RestaurantForReturn GetById(GetCriteria getCriteria)
        {
            string ip = IpAddressHelper.GetIPAddress();
            using (var context = new ApplicationDbContext())
            {
                var item = context.Restaurants.Include("Images").Include("Managers.Roles")
                    .Include("RestaurantType").Include("FoodType").Include("Country").Include("City")
                    .Include("Menus.Meals.Images").Include("Menus.Category").Include("Ratings")
                    .Include("Managers")
                    .SingleOrDefault(it => it.Id == getCriteria.Id);

                if (item == null)
                    return null;
                context.Visitors.Add(new Visitor()
                {
                    Ip = ip,
                    Restaurant = item
                });

                var itemToReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<RestaurantForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<RestaurantForReturn>(item);

                foreach (var mngr in itemToReturn.Managers)
                {
                    if (mngr.Roles != null && mngr.Roles.Any())
                    {
                        var id = new Guid(mngr.Roles[0]);
                        var role = context.Roles.First(r => r.Id == id);
                        mngr.Roles = getCriteria.Lng == LanguageTypes.En
                            ? new List<string>() { role.Name }
                            : new List<string>() { role.NameArabic };
                    }
                }
                context.SaveChanges();
                return itemToReturn;
            }
        }

        public RestaurantForReturnSummary GetSummaryById(GetCriteria getCriteria)
        {
            var item = GetById(getCriteria);
            return getCriteria.Lng == LanguageTypes.En
                ? AutoMapping.InstanseEnglish.Map<RestaurantForReturnSummary>(item)
                : AutoMapping.InstanseArabic.Map<RestaurantForReturnSummary>(item);
        }

        public RestaurantForReturn AddNew(RestaurantForAdd resForAdd, string userName)
        {
            resForAdd.Logo = resForAdd.Logo.ResizeImage();
            using (var context = new ApplicationDbContext())
            {
                var rest = AutoMapping.DefaultInstanse.Map<Restaurant>(resForAdd);
                rest.CreatedByUser = userName;

                if (!string.IsNullOrWhiteSpace(resForAdd.City))
                {
                    var city = context.Cities
                        .FirstOrDefault(f => f.NameEnglish == resForAdd.City || f.NameArabic == resForAdd.City);
                    if (city != null)
                        rest.City = city;
                }

                if (!string.IsNullOrWhiteSpace(resForAdd.Country))
                {
                    var country = context.Countries
                    .First(f => f.NameEnglish == resForAdd.Country || f.NameArabic == resForAdd.Country);
                    rest.Country = country;
                }

                if (!string.IsNullOrWhiteSpace(resForAdd.RestaurantType))
                {
                    var restaurantType = context.RestaurantTypes
                    .First(f => f.NameEnglish == resForAdd.RestaurantType || f.NameArabic == resForAdd.RestaurantType);
                    rest.RestaurantType = restaurantType;
                }

                if (!string.IsNullOrWhiteSpace(resForAdd.FoodType))
                {
                    var foodType = context.FoodTypes
                    .First(f => f.NameEnglish == resForAdd.FoodType || f.NameArabic == resForAdd.FoodType);
                    rest.FoodType = foodType;
                }

                context.Restaurants.Add(rest);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<RestaurantForReturn>(rest);
            }
        }

        public void UpdateStatus(RestaurantStatusForAdd status)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Restaurants.Single(r => r.Id == status.ResturantId);
                itemToUpdate.IsActive = status.IsActive;
                context.Restaurants.Attach(itemToUpdate);
                context.Entry(itemToUpdate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Update(RestaurantForReturn resForUpdate)
        {
            resForUpdate.Logo = resForUpdate.Logo.ResizeImage();
            using (var context = new ApplicationDbContext())
            {
                resForUpdate.Managers = null;
                var updatedItem = AutoMapping.DefaultInstanse.Map<Restaurant>(resForUpdate);

                //.AsNoTracking()
                // in update, we will not send the logo, so we shouldn't delete it when update
                var oldItem = context.Restaurants.First(r => r.Id == resForUpdate.Id);
                if (updatedItem.Logo == null || updatedItem.Logo.Length == 0)
                    updatedItem.Logo = oldItem.Logo;

                context.Entry(oldItem).CurrentValues.SetValues(updatedItem);

                var country = context.Countries
                .FirstOrDefault(f => f.NameEnglish == resForUpdate.Country || f.NameArabic == resForUpdate.Country);
                if (country != null)
                    oldItem.Country = country;

                var city = context.Cities
                .FirstOrDefault(f => f.NameEnglish == resForUpdate.City || f.NameArabic == resForUpdate.City);
                if (city != null)
                    oldItem.City = city;

                var restaurantType = context.RestaurantTypes
                    .FirstOrDefault(f => f.NameEnglish == resForUpdate.RestaurantType || f.NameArabic == resForUpdate.RestaurantType);
                if (restaurantType != null)
                    oldItem.RestaurantType = restaurantType;

                var foodType = context.FoodTypes
                    .FirstOrDefault(f => f.NameEnglish == resForUpdate.FoodType || f.NameArabic == resForUpdate.FoodType);
                if (foodType != null)
                    oldItem.FoodType = foodType;

                context.Restaurants.Attach(oldItem);
                context.Entry(oldItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void UpdateAvailability(RestaurantAvailabilityForAdd status)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Restaurants.Single(r => r.Id == status.ResturantId);
                itemToUpdate.IsAvailable = status.IsAvailable;
                context.Restaurants.Attach(itemToUpdate);
                context.Entry(itemToUpdate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void UploadLogo(LogoForAdd logoForAdd)
        {
            logoForAdd.Logo = logoForAdd.Logo.ResizeImage();
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Restaurants.Single(r => r.Id == logoForAdd.ResturantId);
                itemToUpdate.Logo = logoForAdd.Logo;
                context.Restaurants.Attach(itemToUpdate);
                context.Entry(itemToUpdate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Restaurants.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.Restaurants.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }

        public void AssignAdmin(RestaurantUserForAdd resUserForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Restaurants.FirstOrDefault(it => it.Id == resUserForAdd.ResturantId);
                if (itemToUpdate == null)
                    throw new BadRequestException("Missing Properties: Restaurant Id");
                var user = context.Users.Include("Roles").FirstOrDefault(it => it.Email == resUserForAdd.UserEmail);
                if (user == null)
                    throw new BadRequestException("Missing Properties: User Id");

                var currentRoles = context.Roles.ToList();
                var newRole = currentRoles.FirstOrDefault(x => x.Name == resUserForAdd.Role);
                if (newRole == null)
                    throw new BadRequestException("Missing Properties: Role Id");

                user.Roles.Clear();
                user.Roles.Add(new UserRole()
                {
                    UserId = user.Id,
                    RoleId = newRole.Id,
                });
                itemToUpdate.Managers.Add(user);
                context.SaveChanges();
            }
        }

        public void RemoveAdmin(RestaurantUserForRemove resUserForRemove)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToUpdate = context.Restaurants.Single(it => it.Id == resUserForRemove.ResturantId);
                var user = context.Users.Single(it => it.Email == resUserForRemove.UserEmail);

                //var currentRoles = context.Roles.ToList();
                //var userRole = currentRoles.Single(x => x.Name == RolesNames.User);
                //var websiteAdminRole = currentRoles.Single(x => x.Name == RolesNames.WebsiteAdmin);
                //var websiteManagerRole = currentRoles.Single(x => x.Name == RolesNames.WebsiteManager);
                //var restaurantAdminRole = currentRoles.Single(x => x.Name == RolesNames.RestaurantAdmin);

                //var userAdminC = context.Restaurants.Include("Managers").Where(r => r.Managers.)

                //if (user.Roles.All(r => r.RoleId != websiteAdminRole.Id 
                //                || r.RoleId != websiteManagerRole.Id
                //                || r.RoleId != restaurantAdminRole.Id))
                //{
                //    user.Roles.Clear();
                //    user.Roles.Add(new UserRole()
                //    {
                //        UserId = user.Id,
                //        RoleId = userRole.Id,
                //    });
                //}

                itemToUpdate.Managers.Remove(user);
                context.SaveChanges();
            }
        }


        public PagedListResult<RestaurantReportForReturn> GetRestaurantsReport(SearchCriteria searchCriteria, string userName, string role)
        {
            var finalResult = new PagedListResult<RestaurantReportForReturn>();

            using (var context = new ApplicationDbContext())
            {
                var query = context.Restaurants.Where(r => r.IsActive);
                if (role == RolesNames.WebsiteManager)
                    query = query.Where(r => r.CreatedByUser == userName);

                if (searchCriteria.RestaurantId != Guid.Empty)
                    query = query.Where(r => r.Id == searchCriteria.RestaurantId);

                if (searchCriteria.StartDateTime.HasValue && searchCriteria.EndDateTime.HasValue)
                    query.IncludeFilter(x =>
                    x.Orders.Where(o => o.CreatedDateTime > searchCriteria.StartDateTime.Value
                    && o.CreatedDateTime < searchCriteria.EndDateTime.Value));
                else
                    query = query.Include("Orders");

                query = query.OrderBy(x => (true));
                finalResult.Count = query.Count();
                var restaurants = searchCriteria.DisplayCount > 0
                                ? query.Skip(searchCriteria.DisplayStart).Take(searchCriteria.DisplayCount).ToList()
                                : query.ToList();

                finalResult.Entities = restaurants.Select(r => mapToRestaurantReportForReturn(r, searchCriteria.StartDateTime, searchCriteria.EndDateTime)).ToList();

                finalResult.HasNext = (searchCriteria.DisplayStart > 0 || searchCriteria.DisplayCount > 0)
                    && (searchCriteria.DisplayStart + searchCriteria.DisplayCount < finalResult.Count);

                finalResult.HasPrevious = searchCriteria.DisplayStart > 0;
            }

            return finalResult;
        }

        private RestaurantReportForReturn mapToRestaurantReportForReturn(Restaurant r, DateTime? startDateTime, DateTime? endDateTime)
        {
            var result = new RestaurantReportForReturn()
            {
                RestaurantId = r.Id,
                RestaurantName = r.NameArabic,
                CreatedOrdersCount = r.Orders.Count(o => o.State == OrderStates.Created),
                ConfirmedOrdersCount = r.Orders.Count(o => o.State == OrderStates.Confirmed),
                ReceivedByCustomerOrdersCount = r.Orders.Count(o => o.State == OrderStates.ReceivedByCustomer),
                CanceledOrdersCount = r.Orders.Count(o => o.State == OrderStates.Canceled),
                FeesPaymentType = r.FeesPaymentType,
                TotalOrdersCount = r.Orders.Count(),
            };
            if (r.FeesPaymentType == FeesPaymentTypes.Monthly)
            {
                if (startDateTime.HasValue && endDateTime.HasValue)
                    result.TotalFees = (endDateTime.Value - startDateTime.Value).TotalDays * (r.Fees / 30);
                else
                    result.TotalFees = r.Fees;
            }
            else
                result.TotalFees = r.Orders.Count() * r.Fees;

            return result;
        }
    }
}
