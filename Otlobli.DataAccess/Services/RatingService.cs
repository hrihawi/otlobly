﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class RatingService
    {
        public PagedListResult<RatingForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Rating, RatingForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Rating> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Rating>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "User, Order, Restaurant"
            };

            if (searchCriteria.RestaurantId != Guid.Empty)
                query.AddFilter(x => x.Restaurant.Id == searchCriteria.RestaurantId);

            return query;
        }

        public RatingForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.Ratings.Include("User").Include("Order").Include("Restaurant").SingleOrDefault(it => it.Id == getCriteria.Id);
                return getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<RatingForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<RatingForReturn>(item);
            }
        }

        public void AddNew(RatingForAdd ratingForAdd, Guid userId)
        {
            var rating = AutoMapping.DefaultInstanse.Map<Rating>(ratingForAdd);

            using (var context = new ApplicationDbContext())
            {
                var user = context.Users.FirstOrDefault(x => x.Id == userId);
                var order = context.Orders.Include("Restaurant.Ratings").FirstOrDefault(x => x.Id == ratingForAdd.OrderId);
                rating.User = user;
                rating.Order = order;
                order.Restaurant.Rate = calcRate(order.Restaurant, rating.Rate);
                rating.Restaurant = order.Restaurant;
                context.Ratings.Add(rating);
                context.SaveChanges();
            }
        }

        private double calcRate(Restaurant restaurant, double currentRate)
        {
            if (restaurant.Ratings != null && restaurant.Ratings.Count > 0)
            {
                double value = (restaurant.Ratings.Sum(x => x.Rate) + currentRate) /
                               ((double)restaurant.Ratings.Count + 1);
                return Math.Round(value, 2, MidpointRounding.AwayFromZero);
            }
            return 0;
        }

        public void Update(RatingForReturn ratingForReturn)
        {
            var updatedItem = AutoMapping.DefaultInstanse.Map<Rating>(ratingForReturn);

            using (var context = new ApplicationDbContext())
            {
                var oldItem = context.Ratings.Include("User").Include("Order").First(r => r.Id == ratingForReturn.Id);
                context.Entry(oldItem).CurrentValues.SetValues(updatedItem);

                context.Ratings.Attach(oldItem);
                context.Entry(oldItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Ratings.SingleOrDefault(it => it.Id == id);
                context.Ratings.Remove(itemToDelete);
                context.SaveChanges();
            }
        }
    }
}
