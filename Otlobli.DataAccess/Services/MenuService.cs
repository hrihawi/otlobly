﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class MenuService
    {
        public PagedListResult<MenuForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Menu, MenuForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Menu> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Menu>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "Restaurant,Category,Meals.Images",
            };

            if (searchCriteria.RestaurantId != Guid.Empty)
                query.AddFilter(menu => menu.Restaurant.Id == searchCriteria.RestaurantId);


            return query;
        }

        public MenuForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var menu = context.Menus.Include("Restaurant").Include("Category").Include("Meals.Images").SingleOrDefault(it => it.Id == getCriteria.Id);
                var menuForReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<MenuForReturn>(menu)
                    : AutoMapping.InstanseArabic.Map<MenuForReturn>(menu);
                //TODO: return meals also
                return menuForReturn;
            }
        }

        public MenuForReturn AddNew(MenuForAdd MenuForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToAdd = AutoMapping.DefaultInstanse.Map<Menu>(MenuForAdd);
                context.Menus.Add(itemToAdd);

                var rest = context.Restaurants.Single(r => r.Id == MenuForAdd.RestaurantId);
                rest.Menus.Add(itemToAdd);

                var catg = context.MenuCategories.Single(r => r.Id == MenuForAdd.CategoryId);
                catg.Menus.Add(itemToAdd);

                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<MenuForReturn>(itemToAdd);
            }
        }

        public void Update(MenuForReturn menuForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var updatedItem = AutoMapping.DefaultInstanse.Map<Menu>(menuForReturn);
                updatedItem.Meals = null;

                var oldItem = context.Menus.First(r => r.Id == menuForReturn.Id);
                context.Entry(oldItem).CurrentValues.SetValues(updatedItem);

                var rest = context.Restaurants.Single(r => r.Id == menuForReturn.RestaurantId);
                oldItem.Restaurant = rest;
                var catg = context.MenuCategories.First(r => r.NameArabic == menuForReturn.CategoryName
                || r.NameEnglish == menuForReturn.CategoryName);
                oldItem.Category = catg;

                context.Menus.Attach(oldItem);
                context.Entry(oldItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Menus.Include("Category").Include("Restaurant").SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.Menus.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
