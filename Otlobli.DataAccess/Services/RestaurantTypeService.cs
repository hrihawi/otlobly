﻿
using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class RestaurantTypeService
    {
        public PagedListResult<RestaurantTypeForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria<RestaurantTypeForReturn>(searchCriteria);
            var productRepository = new Repository<RestaurantType, RestaurantTypeForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<RestaurantType> getQueryFromSearchCriteria<T>(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<RestaurantType>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
            };

            //if (searchCriteria.DateTimeRangeMin.HasValue)
            //    query.AddFilter(RestaurantType => RestaurantType.CreatedDateTime > searchCriteria.DateTimeRangeMin.Value);

            //if (searchCriteria.DateTimeRangeMax.HasValue)
            //    query.AddFilter(RestaurantType => RestaurantType.CreatedDateTime < searchCriteria.DateTimeRangeMax.Value);


            return query;
        }

        public RestaurantTypeForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.RestaurantTypes.SingleOrDefault(it => it.Id == getCriteria.Id);
                return getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<RestaurantTypeForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<RestaurantTypeForReturn>(item);
            }
        }      

        public RestaurantTypeForReturn AddNew(RestaurantTypeForAdd RestaurantType)
        {
            using (var context = new ApplicationDbContext())
            {
                var rest = AutoMapping.DefaultInstanse.Map<RestaurantType>(RestaurantType);
                context.RestaurantTypes.Add(rest);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<RestaurantTypeForReturn>(rest);
            }
        }

        public void Update(RestaurantTypeForReturn RestaurantType)
        {
            using (var context = new ApplicationDbContext())
            {
                var newRest = AutoMapping.DefaultInstanse.Map<RestaurantType>(RestaurantType);
                context.RestaurantTypes.Attach(newRest);
                context.Entry(newRest).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.RestaurantTypes.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.RestaurantTypes.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
