﻿using System;
using System.Data.Entity;
using System.Linq;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class FoodTypeService
    {
        public PagedListResult<FoodTypeForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<FoodType, FoodTypeForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<FoodType> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<FoodType>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
            };

            //if (searchCriteria.DateTimeRangeMin.HasValue)
            //    query.AddFilter(foodType => foodType.CreatedDateTime > searchCriteria.DateTimeRangeMin.Value);

            //if (searchCriteria.DateTimeRangeMax.HasValue)
            //    query.AddFilter(foodType => foodType.CreatedDateTime < searchCriteria.DateTimeRangeMax.Value);


            return query;
        }

        public FoodTypeForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.FoodTypes.SingleOrDefault(it => it.Id == getCriteria.Id);
                return getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<FoodTypeForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<FoodTypeForReturn>(item);
            }
        }      

        public FoodTypeForReturn AddNew(FoodTypeForAdd FoodType)
        {
            using (var context = new ApplicationDbContext())
            {
                var rest = AutoMapping.DefaultInstanse.Map<FoodType>(FoodType);
                context.FoodTypes.Add(rest);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<FoodTypeForReturn>(rest);
            }
        }

        public void Update(FoodTypeForReturn FoodType)
        {
            using (var context = new ApplicationDbContext())
            {
                var newRest = AutoMapping.DefaultInstanse.Map<FoodType>(FoodType);
                context.FoodTypes.Attach(newRest);
                context.Entry(newRest).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.FoodTypes.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.FoodTypes.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
