﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Management.Instrumentation;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class CountryService
    {
        public PagedListResult<CountryForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria<CountryForReturn>(searchCriteria);
            var productRepository = new Repository<Country, CountryForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Country> getQueryFromSearchCriteria<T>(SearchCriteria searchCriteria)
        {
            if (searchCriteria == null)
                return null;
            var query = new SearchQuery<Country>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                LanguageFilter = searchCriteria.Lng,
                IncludeProperties = "Cities",
            };

            //if (searchCriteria.DateTimeRangeMin.HasValue)
            //    query.AddFilter(x => x.CreatedDateTime > searchCriteria.DateTimeRangeMin.Value);

            //if (searchCriteria.DateTimeRangeMax.HasValue)
            //    query.AddFilter(x => x.CreatedDateTime < searchCriteria.DateTimeRangeMax.Value);

            return query;
        }

        public CountryForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.Countries.Include("Cities")
                    .SingleOrDefault(it => it.Id == getCriteria.Id);

                if (item == null)
                    return null;

                var itemToReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<CountryForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<CountryForReturn>(item);

                return itemToReturn;
            }
        }

        public CountryForReturn AddNew(CountryForAdd resForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = AutoMapping.DefaultInstanse.Map<Country>(resForAdd);
                context.Countries.Add(item);
                context.SaveChanges();
                return AutoMapping.DefaultInstanse.Map<CountryForReturn>(item);
            }
        }

        public void Update(CountryForReturn resForUpdate)
        {
            using (var context = new ApplicationDbContext())
            {
                var updatedItem = AutoMapping.DefaultInstanse.Map<Country>(resForUpdate);

                var oldItem = context.Countries.First(r => r.Id == resForUpdate.Id);
                context.Entry(oldItem).CurrentValues.SetValues(updatedItem);

                context.Countries.Attach(oldItem);
                context.Entry(oldItem).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Countries.SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.Countries.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }

    }
}
