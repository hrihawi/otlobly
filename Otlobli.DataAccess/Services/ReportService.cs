﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Device.Location;
using System.Linq;
using Otlobli.Model;
using Z.EntityFramework.Plus;

namespace Otlobli.DataAccess
{
    public class ReportService
    {
        public StatisticsReport GetStatisticsReport(SearchCriteria searchCriteria, string userName, string role)
        {
            StatisticsReport report = new StatisticsReport();
            using (var context = new ApplicationDbContext())
            {
                var restaurantQuery = context.Restaurants.Where(r => r.IsActive);
                if (searchCriteria.StartDateTime.HasValue && searchCriteria.EndDateTime.HasValue)
                {
                    restaurantQuery = restaurantQuery.Where(o => o.CreatedDateTime > searchCriteria.StartDateTime.Value
                                                                 && o.CreatedDateTime < searchCriteria.EndDateTime.Value);
                }
                report.RestaurantsCount = restaurantQuery.Count();


                var ordersQuery = context.Orders.AsQueryable();
                if (searchCriteria.StartDateTime.HasValue && searchCriteria.EndDateTime.HasValue)
                {
                    ordersQuery = ordersQuery.Where(o => o.CreatedDateTime > searchCriteria.StartDateTime.Value
                                                                 && o.CreatedDateTime < searchCriteria.EndDateTime.Value);
                }
                report.OrdersCount = ordersQuery.Count();

                var usersQuery = context.Users.Where(x => x.IsActive);
                if (searchCriteria.StartDateTime.HasValue && searchCriteria.EndDateTime.HasValue)
                {
                    usersQuery = usersQuery.Where(o => o.CreatedDateTime > searchCriteria.StartDateTime.Value
                                                                 && o.CreatedDateTime < searchCriteria.EndDateTime.Value);
                }
                report.UsersCount = usersQuery.Count();

                var visitorsQuery = context.Visitors.AsQueryable();
                if (searchCriteria.StartDateTime.HasValue && searchCriteria.EndDateTime.HasValue)
                {
                    visitorsQuery = visitorsQuery.Where(o => o.CreatedDateTime > searchCriteria.StartDateTime.Value
                                                                 && o.CreatedDateTime < searchCriteria.EndDateTime.Value);
                }
                report.VisitorsCount = visitorsQuery.Count();
            }

            return report;
        }
    }
}
