﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class MealService
    {
        public PagedListResult<MealForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Meal, MealForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Meal> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Meal>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "Menu, Images",
            };
            return query;
        }

        public MealForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var Meal = context.Meals.Include("Menu").Include("Images").SingleOrDefault(it => it.Id == getCriteria.Id);

                var MealForReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<MealForReturn>(Meal)
                    : AutoMapping.InstanseArabic.Map<MealForReturn>(Meal);
                return MealForReturn;
            }
        }

        public MealForReturn AddNew(MealForAdd mealForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToAdd = AutoMapping.DefaultInstanse.Map<Meal>(mealForAdd);

                context.Meals.Add(itemToAdd);

                var menu = context.Menus.Single(r => r.Id == mealForAdd.MenuId);
                menu.Meals.Add(itemToAdd);

                context.SaveChanges();

                return AutoMapping.DefaultInstanse.Map<MealForReturn>(itemToAdd);
            }
        }

        public void Update(MealForReturn mealForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var meal = AutoMapping.DefaultInstanse.Map<Meal>(mealForReturn);

                var parent = context.Menus.Single(r => r.Id == mealForReturn.MenuId);
                meal.Menu = parent;

                context.Meals.Attach(meal);
                context.Entry(meal).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Meals.Include("Menu").SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;

                itemToDelete.IsActive = false;
                context.Meals.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
