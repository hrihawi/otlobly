﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class CouponService
    {
        public PagedListResult<CouponForReturn> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Coupon, CouponForReturn>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Coupon> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Coupon>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart,
                IncludeProperties = "Restaurant",
            };


            return query;
        }

        public CouponForReturn GetById(GetCriteria getCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.Coupons.Include("Restaurant").SingleOrDefault(it => it.Id == getCriteria.Id);

                var itemForReturn = getCriteria.Lng == LanguageTypes.En
                    ? AutoMapping.InstanseEnglish.Map<CouponForReturn>(item)
                    : AutoMapping.InstanseArabic.Map<CouponForReturn>(item);

                return itemForReturn;
            }
        }

        public CouponForReturn CheckValidity(CouponValidityCriteria validityCriteria)
        {
            using (var context = new ApplicationDbContext())
            {
                var item = context.Coupons.Include("Restaurant").FirstOrDefault(it => it.Code == validityCriteria.CouponCode);
                if (item == null)
                    throw new BadRequestException("Specified Coupon is not valid");

                if (item.Restaurant.Id != validityCriteria.RestaurantId)
                    throw new BadRequestException("Specified Coupon is not valid with current resturant");

                if (item.HitsCount >= item.Limit)
                    throw new BadRequestException("Specified Coupon exceeded the max limit");

                if (validityCriteria.SubTotalPrice < item.MinimumBillValue)
                    throw new BadRequestException("Order Price less than Minimum Order Value");

                var itemForReturn = AutoMapping.DefaultInstanse.Map<CouponForReturn>(item);

                return itemForReturn;
            }
        }


        public void AddNew(CouponForAdd couponForAdd)
        {
            using (var context = new ApplicationDbContext())
            {
                var coupon = AutoMapping.DefaultInstanse.Map<Coupon>(couponForAdd);

                var res = context.Restaurants.FirstOrDefault(x => x.Id == couponForAdd.RestaurantId);
                coupon.Restaurant = res;

                context.Coupons.Add(coupon);
                context.SaveChanges();
            }
        }

        public void Update(CouponForReturn couponForReturn)
        {
            using (var context = new ApplicationDbContext())
            {
                var coupon = AutoMapping.DefaultInstanse.Map<Coupon>(couponForReturn);

                var res = context.Restaurants.FirstOrDefault(x => x.Id == couponForReturn.RestaurantId);
                coupon.Restaurant = res;

                context.Coupons.Attach(coupon);
                context.Entry(coupon).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var context = new ApplicationDbContext())
            {
                var itemToDelete = context.Coupons.Include("Restaurant").SingleOrDefault(it => it.Id == id);
                if (itemToDelete == null)
                    return false;
                itemToDelete.IsActive = false;
                context.Coupons.Attach(itemToDelete);
                context.Entry(itemToDelete).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
        }
    }
}
