using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Otlobli.DataAccess.Migrations;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid, Login, UserRole, Claim>
    {
        public ApplicationDbContext() : base("OtlobliConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            this.Configuration.LazyLoadingEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Map Entities to their tables.
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<Claim>().ToTable("UserClaim");
            modelBuilder.Entity<Login>().ToTable("UserLogin");
            modelBuilder.Entity<UserRole>().ToTable("UserRole");

            modelBuilder.Entity<Restaurant>()
                .HasMany<Menu>(c => c.Menus)
                .WithOptional(x => x.Restaurant)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Restaurant>()
                .HasMany<ImageForRestaurant>(c => c.Images)
                .WithOptional(x => x.Restaurant)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Meal>()
            .HasMany<ImageForMeal>(c => c.Images)
            .WithOptional(x => x.Meal)
            .WillCascadeOnDelete(true);
        }

        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<ImageForRestaurant> ImagesForRestaurant { get; set; }
        public DbSet<ImageForMeal> ImagesForMeal { get; set; }
        public DbSet<Meal> Meals { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuCategory> MenuCategories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<RestaurantType> RestaurantTypes { get; set; }
        public DbSet<FoodType> FoodTypes { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<WebsiteInfo> WebsiteInfos { get; set; }
        public DbSet<Visitor> Visitors { get; set; }
    }
}