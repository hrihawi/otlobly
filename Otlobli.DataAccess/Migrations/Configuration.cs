using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Otlobli.Model;

namespace Otlobli.DataAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            //ContextKey = "Otlobli.Infrastructure.MessagingModel.ApplicationDbContext";
            if (AppConfiguration.Get("AutomaticMigrationsEnabled", "false") == "true")
                AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            try
            {
                //  This method will be called after migrating to the latest version.
                if (!context.Roles.Any(r => r.Name == RolesNames.WebsiteAdmin))
                {
                    var store = new RoleStore<Role, Guid, UserRole>(context);
                    var manager = new RoleManager<Role, Guid>(store);
                    manager.Create(new Role { Name = RolesNames.WebsiteAdmin, NameArabic = "������ ����� ������" });
                    manager.Create(new Role { Name = RolesNames.WebsiteManager, NameArabic = "���� ������" });
                    manager.Create(new Role { Name = RolesNames.RestaurantAdmin, NameArabic = "������ ����� ������" });
                    manager.Create(new Role { Name = RolesNames.RestaurantManager, NameArabic = "���� ������" });
                    manager.Create(new Role { Name = RolesNames.User, NameArabic = "������ ����" });
                }

                if (!context.Users.Any(u => u.UserName == "OtlobliAdmin"))
                {
                    var store = new UserStore<User, Role, Guid, Login, UserRole, Claim>(context);
                    var manager = new UserManager<User, Guid>(store);
                    var user = new User()
                    {
                        Email = "admin@otlobli.com",
                        EmailConfirmed = true,
                        FirstName = RolesNames.WebsiteAdmin,
                        LastName = RolesNames.WebsiteAdmin,
                        UserName = "OtlobliAdmin",
                        CreatedDateTime = DateTime.UtcNow.Date,
                        Country = "a",
                        City = "a",
                        Region = "a",
                        FullAddress = "a",
                        IsActive = true,
                        //PasswordHash = new PasswordHasher().HashPassword("~Qwe123456"),
                    };

                    manager.Create(user, "~Qwe123456");
                    manager.AddToRole(user.Id, RolesNames.WebsiteAdmin);
                }

                #region Filling Countries

                if (!context.Countries.Any(u => u.NameEnglish == "Saudi Arabia"))
                {
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�������",
                    //    NameEnglish = "Algeria",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�������",
                    //    NameEnglish = "Bahrain",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "���",
                    //    NameEnglish = "Egypt",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "������",
                    //    NameEnglish = "Iraq",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "������",
                    //    NameEnglish = "Jordan",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "������",
                    //    NameEnglish = "Kuwait",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�����",
                    //    NameEnglish = "Lebanon",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�����",
                    //    NameEnglish = "Libya",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "���������",
                    //    NameEnglish = "Mauritania",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "������",
                    //    NameEnglish = "Morocco",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "����",
                    //    NameEnglish = "Oman",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "������",
                    //    NameEnglish = "Palestine",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "���",
                    //    NameEnglish = "Qatar",
                    //});
                    var saudi = new Country() { NameArabic = "������� ������� ��������", NameEnglish = "Saudi Arabia", };
                    saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Riyadh" });
                    saudi.Cities.Add(new City() { NameArabic = "���", NameEnglish = "Jeddah" });
                    saudi.Cities.Add(new City() { NameArabic = "���", NameEnglish = "Mecca" });
                    saudi.Cities.Add(new City() { NameArabic = "������� �������", NameEnglish = "Medina" });
                    saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Hofuf" });
                    saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Ta'if" });
                    saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Dammam" });
                    saudi.Cities.Add(new City() { NameArabic = "���� ����", NameEnglish = "Khamis Mushait" });
                    saudi.Cities.Add(new City() { NameArabic = "�����", NameEnglish = "Khobar" });
                    context.Countries.Add(saudi);
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�������",
                    //    NameEnglish = "Somalia",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�������",
                    //    NameEnglish = "Sudan",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�����",
                    //    NameEnglish = "Syria",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�����",
                    //    NameEnglish = "Turkey",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "����",
                    //    NameEnglish = "Tunisia",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�������� ������� �������",
                    //    NameEnglish = "United Arab Emirates",
                    //});
                    //context.Countries.Add(new Country()
                    //{
                    //    NameArabic = "�����",
                    //    NameEnglish = "Yemen",
                    //});

                }
                else
                {
                    var saudi = context.Countries.Include("Cities").FirstOrDefault(u => u.NameEnglish == "Saudi Arabia");
                    if (saudi != null && saudi.Cities.Count == 0)
                    {
                        saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Riyadh" });
                        saudi.Cities.Add(new City() { NameArabic = "���", NameEnglish = "Jeddah" });
                        saudi.Cities.Add(new City() { NameArabic = "���", NameEnglish = "Mecca" });
                        saudi.Cities.Add(new City() { NameArabic = "������� �������", NameEnglish = "Medina" });
                        saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Hofuf" });
                        saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Ta'if" });
                        saudi.Cities.Add(new City() { NameArabic = "������", NameEnglish = "Dammam" });
                        saudi.Cities.Add(new City() { NameArabic = "���� ����", NameEnglish = "Khamis Mushait" });
                        saudi.Cities.Add(new City() { NameArabic = "�����", NameEnglish = "Khobar" });
                        context.Countries.Attach(saudi);
                        context.Entry(saudi).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }

                #endregion

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    var x = String.Format(
                        "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        string xx = String.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName,
                            ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }
}
