﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    //-----------------------------------------------------------------------
    /// <summary>
    /// Class used for searching informations.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class SearchQuery<TEntity>
    {
        //-----------------------------------------------------------------------
        /// <summary>
        /// Default constructor
        /// </summary>
        public SearchQuery()
        {
            AndFilters = new List<Expression<Func<TEntity, bool>>>();
            OrFilters = new List<Expression<Func<TEntity, bool>>>();
            SortCriterias = new List<ISortCriteria<TEntity>>();
        }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Contains a list of filters to be applied to the query.
        /// </summary>
        public List<Expression<Func<TEntity, bool>>> AndFilters { get; protected set; }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Adds a new filter to the list
        /// </summary>
        /// <param name="filter"></param>
        public void AddFilter(Expression<Func<TEntity, Boolean>> filter)
        {
            AndFilters.Add(filter);
        }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Contains a list of filters to be applied to the query.
        /// </summary>
        public List<Expression<Func<TEntity, bool>>> OrFilters { get; protected set; }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Adds a new filter to the list
        /// </summary>
        /// <param name="filter"></param>
        public void AddOrFilter(Expression<Func<TEntity, Boolean>> filter)
        {
            OrFilters.Add(filter);
        }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Contains a list of criterias that would be used for sorting.
        /// </summary>
        public List<ISortCriteria<TEntity>> SortCriterias
        {
            get;
            protected set;
        }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Adds a Sort Criteria to the list.
        /// </summary>
        /// <param name="sortCriteria"></param>
        public void AddSortCriteria(ISortCriteria<TEntity> sortCriteria)
        {
            SortCriterias.Add(sortCriteria);
        }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Contains a list of properties that would be eagerly loaded 
        /// with he query.
        /// </summary>
        public string IncludeProperties { get; set; }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Contain the LanguageTypes to map the result to spesific language
        /// with he query.
        /// </summary>
        public LanguageTypes LanguageFilter { get; set; }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Number of items to be skipped. Useful for paging.
        /// </summary>
        public int Skip { get; set; }

        //-----------------------------------------------------------------------
        /// <summary>
        /// Represents the number of items to be returned by the query.
        /// </summary>
        public int Take { get; set; }
    }
}
