﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using Otlobli.Model;

namespace Otlobli.DataAccess
{
    public class Repository<T, TReturn> : IRepository<T, TReturn> where T : class where TReturn : class
    {
        protected ApplicationDbContext Context;
        protected IDbSet<T> DbSet;
        //protected readonly object Locker = new object();

        public Repository()
        {
            Context = new ApplicationDbContext();
            DbSet = Context.Set<T>();
        }

        //----------------------------------------------------------------        
        public virtual PagedListResult<TReturn> Search(SearchQuery<T> searchQuery)
        {
            IQueryable<T> sequence = DbSet;

            //Applying filters
            sequence = ManageFilters(searchQuery, sequence);

            //Include Properties
            sequence = ManageIncludeProperties(searchQuery, sequence);

            //Resolving Sort Criteria
            //This code applies the sorting criterias sent as the parameter
            sequence = ManageSortCriterias(searchQuery, sequence);

            //Applying Or filters
            sequence = ManageOrFilters(searchQuery, sequence);

            return GetTheResult(searchQuery, sequence);
        }


        public virtual PagedListResult<TReturn> Search(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            //Applying filters
            sequence = ManageFilters(searchQuery, sequence);

            //Include Properties
            sequence = ManageIncludeProperties(searchQuery, sequence);

            //Resolving Sort Criteria
            //This code applies the sorting criterias sent as the parameter
            sequence = ManageSortCriterias(searchQuery, sequence);

            return GetTheResult(searchQuery, sequence);
        }

        /// <summary>
        /// Executes the query against the repository (database).
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        protected virtual PagedListResult<TReturn> GetTheResult(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            //Counting the total number of object.
            var resultCount = sequence.Count();

            var result = (searchQuery.Take > 0)
                                ? (sequence.Skip(searchQuery.Skip).Take(searchQuery.Take).ToList())
                                : (sequence.ToList());

            //Debug info of what the query looks like
            //Console.WriteLine(sequence.ToString());

            // Setting up the return object.
            bool hasNext = (searchQuery.Skip > 0 || searchQuery.Take > 0) && (searchQuery.Skip + searchQuery.Take < resultCount);
            return new PagedListResult<TReturn>()
            {
                Entities = searchQuery.LanguageFilter == LanguageTypes.En ?
                result.Select(r => AutoMapping.InstanseEnglish.Map<TReturn>(r)).ToList() :
                result.Select(r => AutoMapping.InstanseArabic.Map<TReturn>(r)).ToList(),
                HasNext = hasNext,
                HasPrevious = (searchQuery.Skip > 0),
                Count = resultCount
            };
        }

        /// <summary>
        /// Resolves and applies the sorting criteria of the SearchQuery
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        protected virtual IQueryable<T> ManageSortCriterias(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            if (searchQuery.SortCriterias != null && searchQuery.SortCriterias.Count > 0)
            {
                var sortCriteria = searchQuery.SortCriterias[0];
                var orderedSequence = sortCriteria.ApplyOrdering(sequence, false);

                if (searchQuery.SortCriterias.Count > 1)
                {
                    for (var i = 1; i < searchQuery.SortCriterias.Count; i++)
                    {
                        var sc = searchQuery.SortCriterias[i];
                        orderedSequence = sc.ApplyOrdering(orderedSequence, true);
                    }
                }
                sequence = orderedSequence;
            }
            else
            {
                sequence = ((IOrderedQueryable<T>)sequence).OrderBy(x => (true));
            }
            return sequence;
        }

        /// <summary>
        /// Chains the where clause to the IQueriable instance.
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        protected virtual IQueryable<T> ManageFilters(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            if (searchQuery.AndFilters != null && searchQuery.AndFilters.Count > 0)
            {
                foreach (var filterClause in searchQuery.AndFilters)
                {
                    sequence = sequence.Where(filterClause);
                }
            }
            return sequence;
        }

        protected virtual IQueryable<T> ManageOrFilters(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            var predicate = PredicateBuilder.False<T>();

            if (searchQuery.OrFilters != null && searchQuery.OrFilters.Count > 0)
            {
                foreach (var filterClause in searchQuery.OrFilters)
                {
                    predicate = predicate.Or(filterClause);
                }
                return sequence.AsExpandable().Where(predicate);
            }
            return sequence;
        }

        /// <summary>
        /// Includes the properties sent as part of the SearchQuery.
        /// </summary>
        /// <param name="searchQuery"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        protected virtual IQueryable<T> ManageIncludeProperties(SearchQuery<T> searchQuery, IQueryable<T> sequence)
        {
            if (!string.IsNullOrWhiteSpace(searchQuery.IncludeProperties))
            {
                var properties = searchQuery.IncludeProperties.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var includeProperty in properties)
                {
                    sequence = sequence.Include(includeProperty);
                }
            }
            return sequence;
        }
    }
}