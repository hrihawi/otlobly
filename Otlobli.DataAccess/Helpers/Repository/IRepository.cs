﻿namespace Otlobli.DataAccess
{
    public interface IRepository<TEntity, TReturnEntity> where TEntity : class
    {
        PagedListResult<TReturnEntity> Search(SearchQuery<TEntity> searchQuery);
    }
}