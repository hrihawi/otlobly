﻿namespace Otlobli.Model
{
    public enum OrderStates
    {
        Created = 0,
        Confirmed = 1,
        SentToCustomer = 2,
        ReceivedByCustomer = 3,
        Canceled = 8,
    }
}