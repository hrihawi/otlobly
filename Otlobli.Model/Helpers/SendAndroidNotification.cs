﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Otlobli.Model
{
    public class AndroidNotificationHelper
    {
        public static string Push(string key, string notificationString)
        {
            if (string.IsNullOrEmpty(key))
                throw new BadRequestException("DeviceId cann't be null or empty");

            var tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", AppConfiguration.Get("Android:GoogleAppKey", "AIzaSyCOo9EVIfbOfTOz6ZOB-lUEZXO4p6x6bJA")));

            var postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message="
                + Uri.EscapeUriString(notificationString) + "&data.time="
                + DateTime.UtcNow + "&registration_id=" + key + "";

            var byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            var dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            if (dataStream != null)
            {
                var tReader = new StreamReader(dataStream);
                var response = tReader.ReadToEnd();
                tReader.Close();
                dataStream.Close();
                tResponse.Close();
                return response;
            }
            return null;
        }
    }
}
