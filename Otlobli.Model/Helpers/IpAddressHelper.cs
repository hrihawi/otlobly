﻿using System.Net;
using System.Web;

namespace Otlobli.Model
{
    public static class IpAddressHelper
    {
        /// <summary>
        /// method to get Client ip address
        /// </summary>
        public static string GetIPAddress()
        {
            string hostname = Dns.GetHostName();
            string IP4Address = null;

            if (HttpContext.Current != null)
                IP4Address = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (HttpContext.Current != null && !isValidIp(IP4Address))
                IP4Address = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (HttpContext.Current != null && !isValidIp(IP4Address))
                IP4Address = HttpContext.Current.Request.UserHostAddress;


            if (!isValidIp(IP4Address))
                foreach (var IPA in Dns.GetHostEntry(hostname).AddressList)
                {
                    if (IPA.AddressFamily.ToString() == "InterNetwork")
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }

            if (!isValidIp(IP4Address))
                foreach (IPAddress IPA in Dns.GetHostAddresses(hostname))
                {
                    if (IPA.AddressFamily.ToString() == "InterNetwork")
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }

            if (!isValidIp(IP4Address))
                foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
                {
                    if (IPA.AddressFamily.ToString() == "InterNetwork")
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }

            return IP4Address == "::1" ? "127.0.0.1" : IP4Address;
        }

        private static bool isValidIp(string IP4Address)
        {
            return !string.IsNullOrEmpty(IP4Address) && IP4Address != "::1" && IP4Address != "127.0.0.1";
        }

    }
}
