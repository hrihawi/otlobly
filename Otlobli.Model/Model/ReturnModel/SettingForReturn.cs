﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class SettingForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}
