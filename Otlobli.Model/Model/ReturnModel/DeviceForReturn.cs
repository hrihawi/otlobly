﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class DeviceForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreateDateTime { get; set; }

        public string OsType { get; set; }

        public string Key { get; set; }

        public string DeviceName { get; set; }

        public Guid UserId { get; set; }
    }
}
