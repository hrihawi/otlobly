﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MenuForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Name { get; set; }
        
        public string NameArabic { get; set; }

        public string NameEnglish { get; set; }

        public Guid RestaurantId { get; set; }

        public string CategoryName { get; set; }

        public List<MealForReturn> Meals { get; set; }

    }
}