﻿ using System;
 using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class CouponForReturn
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreateDateTime { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public DateTime ExpiryDateTime { get; set; }

        public int HitsCount { get; set; }

        public int Limit { get; set; }

        [Required]
        public DateTime LastHitDateTime { get; set; }

        [Required]
        public double DiscountValue { get; set; }

        [Required]
        public double MinimumBillValue { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public Guid RestaurantId { get; set; }

    }
}