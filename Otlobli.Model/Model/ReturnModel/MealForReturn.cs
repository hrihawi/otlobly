﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MealForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Name { get; set; }

        public string NameArabic { get; set; }

        public string NameEnglish { get; set; }

        public string Description { get; set; }

        public string DescriptionArabic { get; set; }

        public string DescriptionEnglish { get; set; }

        public double Price { get; set; }

        public double DiscountPrice { get; set; }

        public DateTime DiscountStartDateTime { get; set; }

        public DateTime DiscountEndDateTime { get; set; }

        public bool IsActive { get; set; }

        public Guid MenuId { get; set; }

        public List<ImageForMealForReturnSummary> Images { get; set; }
        
    }
}