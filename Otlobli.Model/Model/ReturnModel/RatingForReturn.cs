﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RatingForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public int Rate { get; set; }

        public string Comment { get; set; }

        public Guid UserId { get; set; }
        
        public Guid OrderId { get; set; }

        public Guid RestaurantId { get; set; }
    }
}
