﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderDetailForReturn
    {
        public Guid Id { get; set; }

        public Guid OrderId { get; set; }

        public DateTime CreatedDateTime { get; set; }

        /// <summary>
        /// Meal info
        /// </summary>
        public Guid MealId { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }


        public int Count { get; set; }

        public string UserNotes { get; set; }

        public ImageForMealForReturnSummary Image { get; set; }

    }
}