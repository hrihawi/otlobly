﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string ReferenceNumber { get; set; }

        public bool IsCustomDelivery { get; set; }

        public DateTime CustomDeliveryDateTime { get; set; }

        public PaymentTypes PaymentType { get; set; }

        public string DeliveryAddress { get; set; }

        public string UserNotes { get; set; }

        public string RestaurantNotes { get; set; }

        public OrderStates State { get; set; }

        public double DeliveryFees { get; set; }

        public DateTime ConfirmDateTime { get; set; }

        public Guid UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string FullAddress { get; set; }

        public Guid RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public Guid CouponId { get; set; }

        public double SubTotalPrice { get; set; }

        public double TotalPrice { get; set; }

        public string Currency { get; set; }

        public string OrderLocation { get; set; }

        public List<OrderDetailForReturn> OrderDetails { get; set; }

        public List<RatingForReturn> Ratings { get; set; }

    }
}
