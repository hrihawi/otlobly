﻿using System;

namespace Otlobli.Model
{
    public class ImageForRestaurantForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public int Order { get; set; }

        public Guid RestaurantId { get; set; }
    }
}