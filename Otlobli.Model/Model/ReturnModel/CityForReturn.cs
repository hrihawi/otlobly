﻿using System;

namespace Otlobli.Model
{
    public class CityForReturn
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Name { get; set; }

        public string NameArabic { get; set; }

        public string NameEnglish { get; set; }
    }
}