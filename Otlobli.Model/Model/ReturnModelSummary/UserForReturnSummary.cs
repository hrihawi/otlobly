﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Otlobli.Model
{
    public class UserForReturnSummary
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public List<string> Roles { get; set; }
    }
}