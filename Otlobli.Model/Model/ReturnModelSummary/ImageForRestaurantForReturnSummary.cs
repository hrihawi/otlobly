﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class ImageForRestaurantForReturnSummary
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public int Order { get; set; }
    }
}