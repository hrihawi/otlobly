﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderForReturnSummary
    {
        public Guid Id { get; set; }

        public OrderStates State { get; set; }
    }
}
