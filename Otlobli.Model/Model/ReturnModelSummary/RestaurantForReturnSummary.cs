﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantForReturnSummary
    {
        public Guid Id { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public string Name { get; set; }

        public string NameArabic { get; set; }

        public string NameEnglish { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Description { get; set; }

        public string FullAddress { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string Website { get; set; }

        public string Email { get; set; }

        public byte[] Logo { get; set; }

        public bool IsOpen { get; set; }

        public string StartWorkTime { get; set; }

        public string EndWorkTime { get; set; }

        public double MinAmountForFreeDelivery { get; set; }

        public double DeliveryFees { get; set; }

        public string GpsLng { get; set; }

        public string GpsLat { get; set; }

        public bool IsAvailable { get; set; }

        public bool IsActive { get; set; }

        public bool IsAcceptingDelivery { get; set; }

        public string Currency { get; set; }

        public string RestaurantType { get; set; }

        public string FoodType { get; set; }

        public double Rate { get; set; }

        public List<MenuForReturn> Menus { get; set; }

    }
}