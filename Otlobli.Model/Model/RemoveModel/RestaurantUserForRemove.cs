﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantUserForRemove
    {
        [Required]
        public string UserEmail { get; set; }

        [Required]
        public Guid ResturantId { get; set; }
    }
}