﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Rating
    {
        public Rating()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public int Rate { get; set; }

        public string Comment { get; set; }

        //[Required]
        public virtual User User { get; set; }

        [Required]
        public virtual Order Order { get; set; }

        public virtual Restaurant Restaurant { get; set; }
    }
}
