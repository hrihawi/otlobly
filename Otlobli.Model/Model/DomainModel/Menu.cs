﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Menu
    {
        public Menu()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            Meals = new List<Meal>();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        [Required]
        public virtual Restaurant Restaurant { get; set; }

        [Required]
        public virtual MenuCategory Category { get; set; }

        public virtual ICollection<Meal> Meals { get; set; }

        public bool IsActive { get; set; }


    }
}