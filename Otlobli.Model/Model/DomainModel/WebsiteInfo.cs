﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class WebsiteInfo
    {
        public WebsiteInfo()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string ValueArabic { get; set; }

        [Required]
        public string ValueEnglish { get; set; }
    }
}
