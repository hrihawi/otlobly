﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class Visitor
    {
        public Visitor()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string Ip { get; set; }

        public string AdditionalInfo { get; set; }

        public virtual Restaurant Restaurant { get; set; }
    }
}
