﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Device
    {
        public Device()
        {
            Id = Guid.NewGuid();
            CreateDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreateDateTime { get; set; }

        public string OsType { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string DeviceName { get; set; }

        public virtual User User { get; set; }
    }
}
