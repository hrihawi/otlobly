﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Country
    {
        public Country()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            Cities = new List<City>();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
