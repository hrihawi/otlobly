﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Coupon
    {
        public Coupon()
        {
            Id = Guid.NewGuid();
            CreateDateTime = DateTime.UtcNow;
            LastHitDateTime = DateTime.UtcNow;
            Code = Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                .Replace("=", "").Replace("+", "").Substring(0, 6).ToLower();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreateDateTime { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public DateTime ExpiryDateTime { get; set; }

        public int HitsCount { get; set; }

        public int Limit { get; set; }

        [Required]
        public DateTime LastHitDateTime { get; set; }

        [Required]
        public double DiscountValue { get; set; }

        [Required]
        public double MinimumBillValue { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}