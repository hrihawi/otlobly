﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class City
    {
        public City()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public virtual Country Country { get; set; }
        
    }
}
