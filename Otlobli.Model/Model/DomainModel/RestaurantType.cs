﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class RestaurantType
    {
        public RestaurantType()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            IsActive = true;
        }


        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public bool IsActive { get; set; }
    }
}
