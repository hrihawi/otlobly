﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderDetail
    {
        public OrderDetail()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public double Price { get; set; }

        public string UserNotes { get; set; }

        [Required]
        public int Count { get; set; }

        [Required]
        public virtual Order Order { get; set; }

        public virtual Meal Meal { get; set; }
    }
}