﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Meal
    {
        public Meal()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            DiscountStartDateTime = DateTime.UtcNow.AddDays(-1);
            DiscountEndDateTime = DateTime.UtcNow.AddDays(-1);
            Images = new List<ImageForMeal>();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        [Required]
        public string DescriptionArabic { get; set; }

        [Required]
        public string DescriptionEnglish { get; set; }

        [Required]
        public double Price { get; set; }

        public double DiscountPrice { get; set; }

        public DateTime DiscountStartDateTime { get; set; }

        public DateTime DiscountEndDateTime { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public virtual Menu Menu { get; set; }

        [Required]
        public virtual ICollection<ImageForMeal> Images { get; set; }

    }
}