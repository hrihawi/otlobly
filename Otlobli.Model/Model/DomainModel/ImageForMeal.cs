﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class ImageForMeal
    {
        public ImageForMeal()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        public int Order { get; set; }

        [Required]
        public byte[] Data { get; set; }

        [Required]
        public virtual Meal Meal { get; set; }
    }
}