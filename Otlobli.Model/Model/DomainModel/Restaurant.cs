﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Restaurant
    {
        public Restaurant()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            Images = new List<ImageForRestaurant>();
            Managers = new List<User>();
            Coupons = new List<Coupon>();
            Menus = new List<Menu>();
            Orders = new List<Order>();
            Ratings = new List<Rating>();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string CreatedByUser { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public string DescriptionArabic { get; set; }

        public string DescriptionEnglish { get; set; }

        public string FullAddressArabic { get; set; }

        public string FullAddressEnglish { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string Website { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public byte[] Logo { get; set; }

        [DataType(DataType.Time)]
        public int StartWorkTime { get; set; }

        [DataType(DataType.Time)]
        public int EndWorkTime { get; set; }

        public double MinAmountForFreeDelivery { get; set; }

        public double DeliveryFees { get; set; }

        public string GpsLat { get; set; }

        public string GpsLng { get; set; }

        public bool IsAvailable { get; set; }

        public bool IsActive { get; set; }

        public bool IsAcceptingDelivery { get; set; }

        public string Currency { get; set; }

        [Required]
        public FeesPaymentTypes FeesPaymentType { get; set; }

        [Required]
        public double Fees { get; set; }

        public double Rate { get; set; }

        public virtual Country Country { get; set; }

        public virtual City City { get; set; }

        public virtual RestaurantType RestaurantType { get; set; }

        public virtual FoodType FoodType { get; set; }

        public virtual ICollection<User> Managers { get; set; }

        public virtual ICollection<ImageForRestaurant> Images { get; set; }

        public virtual ICollection<Coupon> Coupons { get; set; }

        public virtual ICollection<Menu> Menus { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }


    }
}