﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class Order
    {
        public Order()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            ReferenceNumber = Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                .Replace("=", "").Replace("+", "").Substring(0, 6).ToLower();
            OrderDetails = new List<OrderDetail>();
            ConfirmDateTime = DateTime.UtcNow.AddYears(-10);
            CustomDeliveryDateTime =DateTime.UtcNow.AddYears(-10);
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string ReferenceNumber { get; set; }

        public bool IsCustomDelivery { get; set; }

        public DateTime CustomDeliveryDateTime { get; set; }

        [Required]
        public PaymentTypes PaymentType { get; set; }

        [Required]
        public string DeliveryAddress { get; set; }

        public string UserNotes { get; set; }

        public string RestaurantNotes { get; set; }

        public OrderStates State { get; set; }

        public double DeliveryFees { get; set; }

        public DateTime ConfirmDateTime { get; set; }

        [Required]
        public virtual User User { get; set; }


        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string FullAddress { get; set; }

        public string OrderLocation { get; set; }

        [Required]
        public virtual Restaurant Restaurant { get; set; }

        public virtual Coupon Coupon { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

    }
}
