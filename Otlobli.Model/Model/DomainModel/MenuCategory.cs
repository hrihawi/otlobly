﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MenuCategory
    {
        public MenuCategory()
        {
            Id = Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
            Menus = new List<Menu>();
            IsActive = true;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public virtual ICollection<Menu> Menus { get; set; }

        public bool IsActive { get; set; }


    }
}