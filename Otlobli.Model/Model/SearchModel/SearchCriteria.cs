﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class SearchCriteria
    {
        public SearchCriteria()
        {
            DisplayCount = 20;
            DisplayStart = 0;
            SpecificPeriodUnit = PeriodUnits.Hour;
        }

        public int SpecificPeriodValue { get; set; }

        public PeriodUnits SpecificPeriodUnit { get; set; }

        public int? PriceRangeMin { get; set; }

        public int? PriceRangeMax { get; set; }

        public string AnyWhere { get; set; }

        public int DisplayCount { get; set; }

        public int DisplayStart { get; set; }

        public Guid RestaurantId { get; set; }

        public Guid MenuId { get; set; }

        public Guid Id { get; set; }

        public LanguageTypes Lng { get; set; }

        public string CreatedByUser { get; set; }

        public Guid UserId { get; set; }

        public string GpsLat { get; set; }

        public string GpsLng { get; set; }

        public string GpsDistance { get; set; }

        public string RestaurantType { get; set; }
        public string CategoryType { get; set; }

        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }


        //public string SortFieldName { get; set; }

        //public string SortType { get; set; }
    }
}
