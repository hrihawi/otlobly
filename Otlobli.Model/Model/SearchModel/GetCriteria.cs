﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class GetCriteria
    {
        public Guid Id { get; set; }

        public LanguageTypes Lng { get; set; }

    }
}
