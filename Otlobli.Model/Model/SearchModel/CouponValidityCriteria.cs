﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class CouponValidityCriteria
    {
        public string CouponCode { get; set; }

        public Guid RestaurantId { get; set; }

        public double SubTotalPrice { get; set; }
        
    }
}
