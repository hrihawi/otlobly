﻿using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class UpdatePhoneNumber
    {
        [Required]
        public string PhoneNumber { get; set; }
    }
}
