﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class DeleteCriteria
    {
        public Guid Id { get; set; }
    }
}
