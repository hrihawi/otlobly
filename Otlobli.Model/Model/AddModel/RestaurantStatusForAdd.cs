﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantStatusForAdd
    {
        public Guid ResturantId { get; set; }

        public bool IsActive { get; set; }
    }
}