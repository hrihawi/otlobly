﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class ImageForRestaurantForAdd
    {
        public int Order { get; set; }

        [Required]
        public byte[] Data { get; set; }

        public Guid RestaurantId { get; set; }
    }
}