﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MealForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        [Required]
        public string DescriptionArabic { get; set; }

        [Required]
        public string DescriptionEnglish { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public Guid MenuId { get; set; }

        public List<ImageForMealForAdd> Images { get; set; }
    }
}