﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class UserStatusForAdd
    {
        public Guid UserId { get; set; }

        public bool IsActive { get; set; }
    }
}