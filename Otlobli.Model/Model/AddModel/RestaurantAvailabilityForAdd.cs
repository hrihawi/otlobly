﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantAvailabilityForAdd
    {
        public Guid ResturantId { get; set; }

        public bool IsAvailable { get; set; }
    }
}