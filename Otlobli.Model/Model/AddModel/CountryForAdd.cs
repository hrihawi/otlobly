﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class CountryForAdd
    {
        public CountryForAdd()
        {
            Cities = new List<CityForAdd>();
        }

        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public List<CityForAdd> Cities { get; set; }
    }
}
