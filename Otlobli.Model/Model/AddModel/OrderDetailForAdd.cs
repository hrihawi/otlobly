﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderDetailForAdd
    {
        [Required]
        public int Count { get; set; }

        [Required]
        public Guid MealId { get; set; }

        public Guid OrderId { get; set; }

        public string UserNotes { get; set; }
    }
}