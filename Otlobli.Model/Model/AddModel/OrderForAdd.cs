﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class OrderForAdd
    {
        public OrderForAdd()
        {
            IsCustomDelivery = false;
            CustomDeliveryDateTime = DateTime.UtcNow.AddYears(-10);
            OrderDetails = new List<OrderDetailForAdd>();
        }
        public bool IsCustomDelivery { get; set; }

        public DateTime CustomDeliveryDateTime { get; set; }

        [Required]
        public PaymentTypes PaymentType { get; set; }

        [Required]
        public string DeliveryAddress { get; set; }

        public string UserNotes { get; set; }

        public OrderStates State { get; set; }

        public double DeliveryFees { get; set; }

        [Required]
        public Guid UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string FullAddress { get; set; }

        [Required]
        public Guid RestaurantId { get; set; }

        public string CouponCode { get; set; }

        public string OrderLocation { get; set; }


        public List<OrderDetailForAdd> OrderDetails { get; set; }

    }
}
