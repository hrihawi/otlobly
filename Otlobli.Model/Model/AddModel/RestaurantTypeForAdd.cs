﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class RestaurantTypeForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }
    }
}
