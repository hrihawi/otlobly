﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MenuForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        [Required]
        public Guid RestaurantId { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        public List<MealForAdd> Meals { get; set; }
    }
}