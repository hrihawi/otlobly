﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class DeviceForAdd
    {
        public string OsType { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string DeviceName { get; set; }
    }
}
