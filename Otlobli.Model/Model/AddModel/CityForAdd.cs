﻿using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class CityForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }
    }
}