﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RatingForAdd
    {
        [Required]
        public int Rate { get; set; }

        public string Comment { get; set; }

        //[Required]
        //public Guid UserId { get; set; }
        
        [Required]
        public Guid OrderId { get; set; }
    }
}
