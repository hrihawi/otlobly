﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class MenuCategoryForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        // for adding menus, in separate call
    }
}