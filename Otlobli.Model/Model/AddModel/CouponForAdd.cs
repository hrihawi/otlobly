﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class CouponForAdd
    {
        [Required]
        public DateTime ExpiryDateTime { get; set; }

        [Required]
        public int Limit { get; set; }

        [Required]
        public double DiscountValue { get; set; }

        [Required]
        public double MinimumBillValue { get; set; }

        [Required]
        public Guid RestaurantId { get; set; }
    }
}