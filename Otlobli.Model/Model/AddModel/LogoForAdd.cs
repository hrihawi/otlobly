﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class LogoForAdd
    {
        public Guid ResturantId { get; set; }

        public byte[] Logo { get; set; }
    }
}
