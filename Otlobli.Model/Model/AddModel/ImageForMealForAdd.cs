﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class ImageForMealForAdd
    {
        public int Order { get; set; }

        [Required]
        public byte[] Data { get; set; }

        public Guid MealId { get; set; }
    }
}