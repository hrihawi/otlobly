﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantUserForAdd
    {
        [Required]
        public string UserEmail { get; set; }

        [Required]
        public Guid ResturantId { get; set; }

        [Required]
        public string Role { get; set; }
    }
}