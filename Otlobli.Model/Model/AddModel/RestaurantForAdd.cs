﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otlobli.Model
{
    public class RestaurantForAdd
    {
        [Required]
        public string NameArabic { get; set; }

        [Required]
        public string NameEnglish { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string DescriptionArabic { get; set; }

        public string DescriptionEnglish { get; set; }

        public string FullAddressArabic { get; set; }

        public string FullAddressEnglish { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string Website { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public byte[] Logo { get; set; }

        [DataType(DataType.Time)]
        public string StartWorkTime { get; set; }

        [DataType(DataType.Time)]
        public string EndWorkTime { get; set; }

        public double MinAmountForFreeDelivery { get; set; }

        public double DeliveryFees { get; set; }

        public string GpsLng { get; set; }

        public string GpsLat { get; set; }

        public bool IsAvailable { get; set; }

        public bool IsAcceptingDelivery { get; set; }

        public string Currency { get; set; }

        [Required]
        public FeesPaymentTypes FeesPaymentType { get; set; }

        [Required]
        public double Fees { get; set; }

        public string RestaurantType { get; set; }

        public string FoodType { get; set; }
    }
}