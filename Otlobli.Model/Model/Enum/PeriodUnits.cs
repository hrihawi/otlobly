﻿namespace Otlobli.Model
{
    public enum PeriodUnits
    {
        Min,
        Hour,
        Day,
    }
}