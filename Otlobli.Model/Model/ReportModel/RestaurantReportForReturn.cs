﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otlobli.Model
{
    public class RestaurantReportForReturn
    {
        public Guid RestaurantId { get; set; }
        public string RestaurantName { get; set; }

        public int TotalOrdersCount { get; set; }
        public int CreatedOrdersCount { get; set; }
        public int ConfirmedOrdersCount { get; set; }
        public int ReceivedByCustomerOrdersCount { get; set; }
        public int CanceledOrdersCount { get; set; }
        
        public FeesPaymentTypes FeesPaymentType { get; set; }
        public double TotalFees { get; set; }
    }
}
