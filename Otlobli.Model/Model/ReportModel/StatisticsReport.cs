﻿namespace Otlobli.Model
{
    public class StatisticsReport
    {
        public int VisitorsCount { get; set; }
        public int RestaurantsCount { get; set; }
        public int OrdersCount { get; set; }
        public int UsersCount { get; set; }
    }
}
