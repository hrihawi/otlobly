﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Otlobli.Model
{
    public static class StringExtentions
    {
        //public static bool ContainsIgnoreCase(this string source, string value)
        //{
        //    return source?.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0;
        //}
        public static int ParseTime(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return 0;
            var time = source.Split(':');
            var hour = int.Parse(time[0]);
            var min = int.Parse(time[1]);
            return hour*60 + min;
        }
    }
}
