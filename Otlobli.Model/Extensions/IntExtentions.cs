﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Otlobli.Model
{
    public static class IntExtentions
    {
        public static string GetTime(this int source)
        {
            int hour = source / 60;
            int min = source % 60;
            if (min == 0)
                return string.Format("{0}:00", hour);
            if (min < 10)
                return string.Format("{0}:0{1}", hour, min);
            return string.Format("{0}:{1}", hour, min);
        }
    }
}
