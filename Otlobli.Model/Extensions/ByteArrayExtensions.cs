﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace Otlobli.Model
{
    public static class ByteArrayExtensions
    {
        public static byte[] ResizeImage(this byte[] byteImageIn)
        {
            var scale = AppConfiguration.GetAsInt("ImageScale", 2);
            if (byteImageIn == null || byteImageIn.Length == 0)
                return null;
            MemoryStream myMemStream = new MemoryStream(byteImageIn);
            Image fullsizeImage = Image.FromStream(myMemStream);

            Bitmap newImage = new Bitmap(fullsizeImage.Width / scale, fullsizeImage.Height / scale);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(fullsizeImage, new Rectangle(0, 0, fullsizeImage.Width / scale, fullsizeImage.Height / scale));
            }

            MemoryStream imageOut = new MemoryStream();
            newImage.Save(imageOut, System.Drawing.Imaging.ImageFormat.Jpeg);
            newImage.Dispose();
            return imageOut.ToArray();
        }
    }
}
