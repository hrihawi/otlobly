﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Otlobli.Model
{
    public static class DateTimeExtentions
    {
        public static int ParseTime(this DateTime source)
        {
            return source.Hour * 60 + source.Minute;
        }
    }
}
