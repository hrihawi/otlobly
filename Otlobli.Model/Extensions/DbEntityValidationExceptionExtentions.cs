﻿using System.Data.Entity.Validation;

namespace Otlobli.Model
{
    public static class DbEntityValidationExceptionExtentions
    {
        public static string[] HandleValidatedResult(this DbEntityValidationException ex)
        {
            string validationResult = string.Empty;
            foreach (var eve in ex.EntityValidationErrors)
            {
                validationResult += string.Format("Entity of type {0} in state {1} has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                foreach (var ve in eve.ValidationErrors)
                {
                    validationResult += string.Format("- Property: {0}, Error: {1}",
                        ve.PropertyName, ve.ErrorMessage);
                }
            }
            return new[] {validationResult, ex.StackTrace};
        }
    }
}
