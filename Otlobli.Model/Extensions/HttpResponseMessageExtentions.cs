﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Otlobli.Model
{
    public static class HttpResponseMessageExtentions
    {
        public static HttpResponseMessage ReturnContent(this HttpResponseMessage response, byte[] content)
        {
            var stream = new MemoryStream(content);
            {
                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
                response.Headers.CacheControl = new CacheControlHeaderValue { NoCache = true, NoStore = true, MustRevalidate = true };
                response.Headers.Pragma.Add(new NameValueHeaderValue("no-cache"));
                return response;
            }
        }
    }
}
