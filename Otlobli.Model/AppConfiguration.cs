﻿using System;
using System.Configuration;

namespace Otlobli.Model
{
    public static class AppConfiguration
    {
        public static string Get(string key, string defaultValue = null)
        {
            return string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key])
                ? defaultValue
                : ConfigurationManager.AppSettings[key];
        }

        public static int GetAsInt(string key, int defaultValue = 0)
        {
            var stringValue = ConfigurationManager.AppSettings[key];
            if (!string.IsNullOrWhiteSpace(stringValue))
                return Convert.ToInt32(stringValue);
            return defaultValue;
        }
    }
}