﻿namespace Otlobli.Model
{
    public class RoleForAssign
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}