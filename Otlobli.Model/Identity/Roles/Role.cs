﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Otlobli.Model
{
    public class Role : IdentityRole<Guid, UserRole>
    {
        public Role()
        {
            Id = Guid.NewGuid();
        }

        public string NameArabic { get; set; }
    }
}