﻿namespace Otlobli.Model
{
    public class RoleForReturn
    {
        public string Id { get; set; }
        public string NameEnglish { get; set; }
        public string NameArabic { get; set; }

    }
}