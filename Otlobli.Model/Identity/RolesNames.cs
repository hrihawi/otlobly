﻿namespace Otlobli.Model
{
    public class RolesNames
    {
        public const string WebsiteAdmin = "WebsiteAdmin";
        public const string WebsiteManager = "WebsiteManager";
        public const string RestaurantAdmin = "RestaurantAdmin";
        public const string RestaurantManager = "RestaurantManager";
        public const string User = "User";

        public const string AnyAdmin = "WebsiteAdmin, WebsiteManager, RestaurantAdmin, RestaurantManager";
        public const string AdminsAndOwner = "WebsiteAdmin, WebsiteManager, RestaurantAdmin";
        public const string AdminsAndUser = "WebsiteAdmin, WebsiteManager, User";
        public const string Admins = "WebsiteAdmin, WebsiteManager";
        public const string ResturantAdmin = "RestaurantAdmin, RestaurantManager";

    }
}
