﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Otlobli.Model
{
    public class UserForReturn
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string FullAddress { get; set; }
        public string Description { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public bool IsActive { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public byte[] Image { get; set; }

        public string Role { get; set; }
    }
}