﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Otlobli.Model
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser<Guid, Login, UserRole, Claim>
    {
        public User()
        {
            Id= Guid.NewGuid();
            CreatedDateTime = DateTime.UtcNow;
        }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        public string Password { get; set; }

        //[Required]
        [MaxLength(50)]
        public string Country { get; set; }

        //[Required]
        [MaxLength(50)]
        public string City { get; set; }

        //[Required]
        [MaxLength(50)]
        public string Region { get; set; }

        //[Required]
        [MaxLength(100)]
        public string FullAddress { get; set; }

        //[MaxLength(100)]
        public string Description { get; set; }

        [Required]
        public DateTime CreatedDateTime { get; set; }

        public bool IsActive { get; set; }

        public byte[] Image { get; set; }

        public virtual ICollection<Coupon> Coupons { get; set; }

        public virtual ICollection<Device> Devices { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual Restaurant Restaurant { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, Guid> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}