﻿using System;
using System.Linq;
using AutoMapper;

namespace Otlobli.Model
{
    public static class AutoMapping
    {
        private static MapperConfiguration mapperConfigurationArabic;
        private static MapperConfiguration mapperConfigurationEnglish;

        public static IMapper DefaultInstanse
        {
            get { return InstanseArabic; }
        }

        public static IMapper InstanseArabic { get; private set; }
        public static IMapper InstanseEnglish { get; private set; }

        public static void Configure()
        {
            mapperConfigurationArabic = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserForReturn>()
                    .ForMember(d => d.FullName, y => y.MapFrom(s => string.Format("{0} {1}", s.FirstName, s.LastName)));
                cfg.CreateMap<UserForReturn, User>();

                cfg.CreateMap<User, UserForReturnSummary>()
                    .ForMember(d => d.FullName, y => y.MapFrom(s => string.Format("{0} {1}", s.FirstName, s.LastName)));


                cfg.CreateMap<Role, RoleForReturn>()
                    .ForMember(d => d.NameEnglish, y => y.MapFrom(s => s.Name));

                cfg.CreateMap<UserRole, string>()
                    .ConvertUsing(source => source.RoleId.ToString());

                cfg.CreateMap<Restaurant, RestaurantForReturn>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.GetTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.GetTime()))
                    .ForMember(d => d.RestaurantType, y => y.MapFrom(s => s.RestaurantType.NameArabic))
                    .ForMember(d => d.FoodType, y => y.MapFrom(s => s.FoodType.NameArabic))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionArabic))
                    .ForMember(d => d.FullAddress, y => y.MapFrom(s => s.FullAddressArabic))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic))
                    .ForMember(d => d.Country, y => y.MapFrom(s => s.Country.NameArabic))
                    .ForMember(d => d.City, y => y.MapFrom(s => s.City.NameArabic));


                cfg.CreateMap<Restaurant, RestaurantForReturnSummary>()
                    .ForMember(d => d.IsOpen, y => y.MapFrom(s =>
                        DateTime.UtcNow.ParseTime() > s.StartWorkTime && DateTime.UtcNow.ParseTime() < s.EndWorkTime))
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.GetTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.GetTime()))
                    .ForMember(d => d.RestaurantType, y => y.MapFrom(s => s.RestaurantType.NameArabic))
                    .ForMember(d => d.FoodType, y => y.MapFrom(s => s.FoodType.NameArabic))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionArabic))
                    .ForMember(d => d.FullAddress, y => y.MapFrom(s => s.FullAddressArabic))
                    .ForMember(d => d.Country, y => y.MapFrom(s => s.Country.NameArabic))
                    .ForMember(d => d.City, y => y.MapFrom(s => s.City.NameArabic));


                cfg.CreateMap<RestaurantForAdd, Restaurant>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.ParseTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.ParseTime()))
                    .ForMember(x => x.RestaurantType, p => p.Ignore())
                    .ForMember(x => x.FoodType, p => p.Ignore())
                    .ForMember(x => x.City, p => p.Ignore())
                    .ForMember(x => x.Country, p => p.Ignore());


                cfg.CreateMap<RestaurantForReturn, Restaurant>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.ParseTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.ParseTime()))
                    .ForMember(x => x.RestaurantType, p => p.Ignore())
                    .ForMember(x => x.FoodType, p => p.Ignore())
                    .ForMember(x => x.City, p => p.Ignore())
                    .ForMember(x => x.Country, p => p.Ignore())
                    .ForMember(x => x.Images, p => p.Ignore());

                cfg.CreateMap<RestaurantType, RestaurantTypeForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic));

                cfg.CreateMap<RestaurantTypeForReturn, RestaurantType>();
                cfg.CreateMap<RestaurantTypeForAdd, RestaurantType>();

                cfg.CreateMap<FoodType, FoodTypeForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic));

                cfg.CreateMap<FoodTypeForReturn, FoodType>();
                cfg.CreateMap<FoodTypeForAdd, FoodType>();

                cfg.CreateMap<ImageForRestaurantForAdd, ImageForRestaurant>();
                cfg.CreateMap<ImageForRestaurant, ImageForRestaurantForReturn>();
                cfg.CreateMap<ImageForRestaurant, ImageForRestaurantForReturnSummary>();

                cfg.CreateMap<ImageForMealForAdd, ImageForMeal>();
                cfg.CreateMap<ImageForMeal, ImageForMealForReturn>();
                cfg.CreateMap<ImageForMeal, ImageForMealForReturnSummary>();
                cfg.CreateMap<ImageForMealForReturnSummary, ImageForMeal>();
                cfg.CreateMap<MenuCategoryForAdd, MenuCategory>();
                cfg.CreateMap<MenuCategoryForReturn, MenuCategory>();
                cfg.CreateMap<MenuCategory, MenuCategoryForReturn>();

                cfg.CreateMap<MenuForAdd, Menu>();
                cfg.CreateMap<MenuForReturn, Menu>();
                cfg.CreateMap<Menu, MenuForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic))
                    .ForMember(d => d.CategoryName, y => y.MapFrom(s => s.Category.NameArabic))
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));

                cfg.CreateMap<MealForAdd, Meal>();
                cfg.CreateMap<MealForReturn, Meal>();
                cfg.CreateMap<Meal, MealForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionArabic))
                    .ForMember(d => d.MenuId, y => y.MapFrom(s => s.Menu.Id));

                cfg.CreateMap<UserForAdd, User>();

                cfg.CreateMap<OrderForAdd, Order>()
                    .ForMember(d => d.OrderDetails, y => y.Ignore());

                cfg.CreateMap<OrderForReturn, Order>()
                    .ForMember(d => d.OrderDetails, y => y.Ignore());
                cfg.CreateMap<Order, OrderForReturn>()
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id))
                    .ForMember(d => d.UserId, y => y.MapFrom(s => s.User.Id))
                    .ForMember(d => d.FirstName, y => y.MapFrom(s => s.User.FirstName))
                    .ForMember(d => d.LastName, y => y.MapFrom(s => s.User.LastName))
                    .ForMember(d => d.PhoneNumber, y => y.MapFrom(s => s.User.PhoneNumber))
                    .ForMember(d => d.RestaurantName, y => y.MapFrom(s => s.Restaurant.NameArabic))
                    .ForMember(d => d.SubTotalPrice, y => y.MapFrom(s => s.OrderDetails.Sum(x => x.Price)))
                    .ForMember(d => d.TotalPrice, y => y.MapFrom(s => calcTotalPrice(s)))
                    .ForMember(d => d.Currency, y => y.MapFrom(s => s.Restaurant.Currency))
                    .ForMember(d => d.CouponId, y => y.MapFrom(s => s.Coupon.Id));

                cfg.CreateMap<OrderDetailForAdd, OrderDetail>();
                cfg.CreateMap<OrderDetail, OrderDetailForReturn>()
                    .ForMember(d => d.MealId, y => y.MapFrom(s => s.Meal.Id))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.Meal.NameArabic))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.Meal.DescriptionArabic))
                    .ForMember(d => d.Image, y => y.MapFrom(s => s.Meal.Images.FirstOrDefault()))
                    .ForMember(d => d.OrderId, y => y.MapFrom(s => s.Order.Id));

                cfg.CreateMap<OrderDetailForReturn, OrderDetail>();

                cfg.CreateMap<CountryForReturn, Country>();
                cfg.CreateMap<CountryForAdd, Country>();
                cfg.CreateMap<Country, CountryForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic));

                cfg.CreateMap<CityForReturn, City>();
                cfg.CreateMap<CityForAdd, City>();
                cfg.CreateMap<City, CityForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameArabic));

                cfg.CreateMap<CouponForAdd, Coupon>();
                cfg.CreateMap<Coupon, CouponForReturn>()
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));
                cfg.CreateMap<CouponForReturn, Coupon>();

                cfg.CreateMap<RatingForAdd, Rating>();
                cfg.CreateMap<Rating, RatingForReturn>()
                    .ForMember(d => d.UserId, y => y.MapFrom(s => s.User.Id))
                    .ForMember(d => d.OrderId, y => y.MapFrom(s => s.Order.Id))
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));
                cfg.CreateMap<RatingForReturn, Rating>();

                cfg.CreateMap<DeviceForAdd, Device>();
                cfg.CreateMap<DeviceForReturn, Device>();
                cfg.CreateMap<Device, DeviceForReturn>();

            });
            mapperConfigurationEnglish = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserForReturn>()
                    .ForMember(d => d.FullName, y => y.MapFrom(s => string.Format("{0} {1}", s.FirstName, s.LastName)));
                cfg.CreateMap<UserForReturn, User>();

                cfg.CreateMap<User, UserForReturnSummary>()
                    .ForMember(d => d.FullName, y => y.MapFrom(s => string.Format("{0} {1}", s.FirstName, s.LastName)));


                cfg.CreateMap<Role, RoleForReturn>()
                    .ForMember(d => d.NameEnglish, y => y.MapFrom(s => s.Name));

                cfg.CreateMap<UserRole, string>()
                    .ConvertUsing(source => source.RoleId.ToString());

                cfg.CreateMap<Restaurant, RestaurantForReturn>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.GetTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.GetTime()))
                    .ForMember(d => d.RestaurantType, y => y.MapFrom(s => s.RestaurantType.NameEnglish))
                    .ForMember(d => d.FoodType, y => y.MapFrom(s => s.FoodType.NameEnglish))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionEnglish))
                    .ForMember(d => d.FullAddress, y => y.MapFrom(s => s.FullAddressEnglish))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish))
                    .ForMember(d => d.Country, y => y.MapFrom(s => s.Country.NameEnglish))
                    .ForMember(d => d.City, y => y.MapFrom(s => s.City.NameEnglish));


                cfg.CreateMap<Restaurant, RestaurantForReturnSummary>()
                    .ForMember(d => d.IsOpen, y => y.MapFrom(s =>
                        DateTime.UtcNow.ParseTime() > s.StartWorkTime && DateTime.UtcNow.ParseTime() < s.EndWorkTime))
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.GetTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.GetTime()))
                    .ForMember(d => d.RestaurantType, y => y.MapFrom(s => s.RestaurantType.NameEnglish))
                    .ForMember(d => d.FoodType, y => y.MapFrom(s => s.FoodType.NameEnglish))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionEnglish))
                    .ForMember(d => d.FullAddress, y => y.MapFrom(s => s.FullAddressEnglish))
                    .ForMember(d => d.Country, y => y.MapFrom(s => s.Country.NameEnglish))
                    .ForMember(d => d.City, y => y.MapFrom(s => s.City.NameEnglish));


                cfg.CreateMap<RestaurantForAdd, Restaurant>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.ParseTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.ParseTime()))
                    .ForMember(x => x.RestaurantType, p => p.Ignore())
                    .ForMember(x => x.FoodType, p => p.Ignore())
                    .ForMember(x => x.City, p => p.Ignore())
                    .ForMember(x => x.Country, p => p.Ignore());


                cfg.CreateMap<RestaurantForReturn, Restaurant>()
                    .ForMember(d => d.StartWorkTime, y => y.MapFrom(s => s.StartWorkTime.ParseTime()))
                    .ForMember(d => d.EndWorkTime, y => y.MapFrom(s => s.EndWorkTime.ParseTime()))
                    .ForMember(x => x.RestaurantType, p => p.Ignore())
                    .ForMember(x => x.FoodType, p => p.Ignore())
                    .ForMember(x => x.City, p => p.Ignore())
                    .ForMember(x => x.Country, p => p.Ignore())
                    .ForMember(x => x.Images, p => p.Ignore());


                cfg.CreateMap<RestaurantType, RestaurantTypeForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish));

                cfg.CreateMap<RestaurantTypeForReturn, RestaurantType>();
                cfg.CreateMap<RestaurantTypeForAdd, RestaurantType>();

                cfg.CreateMap<FoodType, FoodTypeForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish));

                cfg.CreateMap<FoodTypeForReturn, FoodType>();
                cfg.CreateMap<FoodTypeForAdd, FoodType>();

                cfg.CreateMap<ImageForRestaurantForAdd, ImageForRestaurant>();
                cfg.CreateMap<ImageForRestaurant, ImageForRestaurantForReturn>();
                cfg.CreateMap<ImageForRestaurant, ImageForRestaurantForReturnSummary>();

                cfg.CreateMap<ImageForMealForAdd, ImageForMeal>();
                cfg.CreateMap<ImageForMeal, ImageForMealForReturn>();
                cfg.CreateMap<ImageForMeal, ImageForMealForReturnSummary>();
                cfg.CreateMap<ImageForMealForReturnSummary, ImageForMeal>();
                cfg.CreateMap<MenuCategoryForAdd, MenuCategory>();
                cfg.CreateMap<MenuCategoryForReturn, MenuCategory>();
                cfg.CreateMap<MenuCategory, MenuCategoryForReturn>();

                cfg.CreateMap<MenuForAdd, Menu>();
                cfg.CreateMap<MenuForReturn, Menu>();
                cfg.CreateMap<Menu, MenuForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish))
                    .ForMember(d => d.CategoryName, y => y.MapFrom(s => s.Category.NameEnglish))
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));

                cfg.CreateMap<MealForAdd, Meal>();
                cfg.CreateMap<MealForReturn, Meal>();
                cfg.CreateMap<Meal, MealForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.DescriptionEnglish))
                    .ForMember(d => d.MenuId, y => y.MapFrom(s => s.Menu.Id));

                cfg.CreateMap<UserForAdd, User>();

                cfg.CreateMap<OrderForAdd, Order>()
                    .ForMember(d => d.OrderDetails, y => y.Ignore());


                cfg.CreateMap<OrderForReturn, Order>()
                    .ForMember(d => d.OrderDetails, y => y.Ignore());
                cfg.CreateMap<Order, OrderForReturn>()
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id))
                    .ForMember(d => d.UserId, y => y.MapFrom(s => s.User.Id))
                    .ForMember(d => d.FirstName, y => y.MapFrom(s => s.User.FirstName))
                    .ForMember(d => d.LastName, y => y.MapFrom(s => s.User.LastName))
                    .ForMember(d => d.PhoneNumber, y => y.MapFrom(s => s.User.PhoneNumber))
                    .ForMember(d => d.RestaurantName, y => y.MapFrom(s => s.Restaurant.NameEnglish))
                    .ForMember(d => d.SubTotalPrice, y => y.MapFrom(s => s.OrderDetails.Sum(x => x.Price)))
                    .ForMember(d => d.TotalPrice, y => y.MapFrom(s => calcTotalPrice(s)))
                    .ForMember(d => d.Currency, y => y.MapFrom(s => s.Restaurant.Currency))
                    .ForMember(d => d.CouponId, y => y.MapFrom(s => s.Coupon.Id));

                cfg.CreateMap<OrderDetailForAdd, OrderDetail>();
                cfg.CreateMap<OrderDetail, OrderDetailForReturn>()
                    .ForMember(d => d.MealId, y => y.MapFrom(s => s.Meal.Id))
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.Meal.NameEnglish))
                    .ForMember(d => d.Description, y => y.MapFrom(s => s.Meal.DescriptionEnglish))
                    .ForMember(d => d.Image, y => y.MapFrom(s => s.Meal.Images.FirstOrDefault()))
                    .ForMember(d => d.OrderId, y => y.MapFrom(s => s.Order.Id));
                cfg.CreateMap<OrderDetailForReturn, OrderDetail>();

                cfg.CreateMap<CountryForReturn, Country>();
                cfg.CreateMap<CountryForAdd, Country>();
                cfg.CreateMap<Country, CountryForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish));

                cfg.CreateMap<CityForReturn, City>();
                cfg.CreateMap<CityForAdd, City>();
                cfg.CreateMap<City, CityForReturn>()
                    .ForMember(d => d.Name, y => y.MapFrom(s => s.NameEnglish));

                cfg.CreateMap<CouponForAdd, Coupon>();
                cfg.CreateMap<Coupon, CouponForReturn>()
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));
                cfg.CreateMap<CouponForReturn, Coupon>();

                cfg.CreateMap<RatingForAdd, Rating>();
                cfg.CreateMap<Rating, RatingForReturn>()
                    .ForMember(d => d.UserId, y => y.MapFrom(s => s.User.Id))
                    .ForMember(d => d.OrderId, y => y.MapFrom(s => s.Order.Id))
                    .ForMember(d => d.RestaurantId, y => y.MapFrom(s => s.Restaurant.Id));
                cfg.CreateMap<RatingForReturn, Rating>();

                cfg.CreateMap<DeviceForAdd, Device>();
                cfg.CreateMap<DeviceForReturn, Device>();
                cfg.CreateMap<Device, DeviceForReturn>();
            });
            InstanseArabic = mapperConfigurationArabic.CreateMapper();
            InstanseEnglish = mapperConfigurationEnglish.CreateMapper();
        }

        private static double calcTotalPrice(Order order)
        {
            double price = order.OrderDetails.Sum(x => x.Price) + order.DeliveryFees;
            if (order.Coupon != null)
                price -= order.Coupon.DiscountValue;
            return price;
        }
    }
}
