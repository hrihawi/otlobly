﻿using System;
using System.Linq;
using AutoMapper;
using Otlobli.DataAccess;
using Otlobli.Model;

namespace Otlobli.Services
{
    public class RestaurantService
    {
        public PagedListResult<Restaurant> Search(SearchCriteria searchCriteria)
        {
            var query = getQueryFromSearchCriteria(searchCriteria);
            var productRepository = new Repository<Restaurant>();
            return productRepository.Search(query);
        }

        private static SearchQuery<Restaurant> getQueryFromSearchCriteria(SearchCriteria searchCriteria)
        {
            var query = new SearchQuery<Restaurant>
            {
                Take = searchCriteria.DisplayCount,
                Skip = searchCriteria.DisplayStart
            };

            if (searchCriteria.DateTimeRangeMin.HasValue)
                query.AddFilter(restaurant => restaurant.CreatedDateTime > searchCriteria.DateTimeRangeMin.Value);

            if (searchCriteria.DateTimeRangeMax.HasValue)
                query.AddFilter(restaurant => restaurant.CreatedDateTime < searchCriteria.DateTimeRangeMax.Value);

            if (searchCriteria.PriceRangeMin.HasValue)
                query.AddFilter(restaurant => restaurant.DeliveryFees > searchCriteria.PriceRangeMin.Value);

            if (searchCriteria.PriceRangeMax.HasValue)
                query.AddFilter(restaurant => restaurant.DeliveryFees < searchCriteria.PriceRangeMax.Value);

            if (!string.IsNullOrWhiteSpace(searchCriteria.AnyWhere))
            {
                query.AddFilter(restaurant =>
                    restaurant.NameEnglish.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.Country.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.CreatedByUser.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.Description.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.Email.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.FoodType.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.FullAddress.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.NameArabic.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.PhoneNumber1.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.PhoneNumber2.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.RestaurantType.ContainsIgnoreCase(searchCriteria.AnyWhere)
                    || restaurant.Website.ContainsIgnoreCase(searchCriteria.AnyWhere));
            }
            return query;
        }

        public PagedListResult<RestaurantSummary> SearchSummary(SearchCriteria searchCriteria)
        {
            var pagedListResult = Search(searchCriteria);
            var sumuries = pagedListResult.Entities.Select(GetSummary);
            return new PagedListResult<RestaurantSummary>()
            {
                Count = pagedListResult.Count,
                HasNext = pagedListResult.HasNext,
                HasPrevious = pagedListResult.HasPrevious,
                Entities = sumuries.ToList()
            };
        }

        private RestaurantSummary GetSummary(Restaurant res)
        {
            var des = new RestaurantSummary();
            Mapper.Map(res, des);
            return des;
        }

        public Restaurant GetById(string id)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Resturants.FirstOrDefault(it => it.Id == id);
            }
        }

        public RestaurantSummary GetSummaryById(string id)
        {
            throw new NotImplementedException();
        }

        public Restaurant AddNew(Restaurant restaurant)
        {
            throw new NotImplementedException();
        }

        public Restaurant Update(Restaurant restaurant)
        {
            throw new NotImplementedException();
        }

        public Restaurant DeleteById(string id)
        {
            throw new NotImplementedException();
        }
    }
}
