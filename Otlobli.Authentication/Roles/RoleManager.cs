﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Otlobli.DataAccess;
using Otlobli.Model;

namespace Otlobli.Authentication
{
    public class RoleManager : RoleManager<Role, Guid>
    {
        public RoleManager(IRoleStore<Role, Guid> roleStore) : base(roleStore)
        {
        }

        public static RoleManager Create(IdentityFactoryOptions<RoleManager> options, IOwinContext context)
        {
            var appRoleManager = new RoleManager(new RoleStore<Role, Guid, UserRole>(context.Get<ApplicationDbContext>()));

            return appRoleManager;
        }

        public static RoleManager AppRoleManager(IOwinContext context)
        {
            return context.GetUserManager<RoleManager>();
        }
    }
}