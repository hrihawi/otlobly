﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using Otlobli.Model;

namespace Otlobli.Authentication
{
    public class SmsService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage identityMessage)
        {
            try
            {
                if (AppConfiguration.Get("smsService:Mode") == "Fake")
                {
                    File.WriteAllText(@"C:\ProgramData\SmsCode.txt", identityMessage.Body);
                }
                else
                {
                    string url = string.Format("https://www.asr3sms.com/sms/api/sendsms.php?username={0}&password={1}&message={2}&numbers={3}&sender={4}&unicode=e&return=json",
                        "msjarbou", "saleh4488", identityMessage.Body, identityMessage.Destination, "otloblee");

                    using (var httpClient = new HttpClient())
                    {
                        var response = await httpClient.GetAsync(new Uri(url));
                        response.EnsureSuccessStatusCode();

                        var responseString = await response.Content.ReadAsStringAsync();
                        var jObject = JObject.Parse(responseString);
                        string errorCode = jObject.GetValue("Code").ToString();
                        //TODO: if not 100 then log the error code
                        string messageIs = jObject.GetValue("MessageIs").ToString();
                    }
                }
            }
            catch (Exception)
            {

            }
            await Task.FromResult(0);
        }
    }
}