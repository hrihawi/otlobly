﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Otlobli.Authentication
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            await sendEmailasync(message);
        }

        private async Task sendEmailasync(IdentityMessage identityMessage)
        {
            try
            {
                var emailClient = new SmtpClient
                {
                    Host = "smtpout.asia.secureserver.net",
                    Port = 3535,
                    EnableSsl = false,
                    Timeout = 80000,
                    UseDefaultCredentials = false
                };

                string smtpUserName = "noreply@otloblee.net";
                string smtpPassword = "Qwe123456";
                emailClient.Credentials = new NetworkCredential(smtpUserName, smtpPassword);

                string senderAccount = "noreply@otloblee.com";
                var message = new MailMessage
                {
                    From = new MailAddress(senderAccount),
                    Body = identityMessage.Body,
                    IsBodyHtml = true,
                    Subject = identityMessage.Subject
                };

                message.To.Add(identityMessage.Destination);
                emailClient.Send(message);
            }
            catch (Exception ex)
            {
            }
            await Task.FromResult(0);
        }
    }
}