﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Otlobli.DataAccess;
using Otlobli.Model;

namespace Otlobli.Authentication
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            var userManager = context.OwinContext.GetUserManager<UserManager>();
            User user = await userManager.FindAsync(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
            //if (!user.EmailConfirmed)
            //{
            //    context.SetError("invalid_grant", "User did not confirm email.");
            //    return;
            //}
            var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");

            var userFull = new AccountService().GetUserById(user.Id);

            var currentRoles = await userManager.GetRolesAsync(user.Id);
            var props = new AuthenticationProperties();
            props.Dictionary.Add("Id", user.Id.ToString());
            props.Dictionary.Add("UserName", user.UserName);
            props.Dictionary.Add("Email", user.Email);
            props.Dictionary.Add("PhoneNumber", user.PhoneNumber);
            props.Dictionary.Add("FirstName", user.FirstName);
            props.Dictionary.Add("LastName", user.LastName);
            props.Dictionary.Add("EmailConfirmed", user.EmailConfirmed.ToString());
            props.Dictionary.Add("PhoneNumberConfirmed", user.PhoneNumberConfirmed.ToString());
            props.Dictionary.Add("Roles", String.Join(",", currentRoles));
            if (userFull.Restaurant != null)
            props.Dictionary.Add("RestaurantId", userFull.Restaurant.Id.ToString());
            
            var ticket = new AuthenticationTicket(oAuthIdentity, props);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            return Task.FromResult<object>(null);
        }
    }
}