﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json.Linq;
using Otlobli.Model;

namespace Otlobli.Resturant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void newOrderExecuted(OrderForReturn order)
        {
            MessageBox.Show(order.State.ToString());
        }

        public HttpClient GetAuthenticatedHttpClient(string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = GetAccessToken().Result;
            }
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUri)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            return client;
        }

        private static string apiBaseUri = "http://localhost";

        public async Task<string> GetAccessToken(string userName = "hasanUser11054", string password = "~Qwe123456")
        {
            var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", userName),
                    new KeyValuePair<string, string>("password", password),
                });

            var client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUri)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var responseMessage = await client.PostAsync("/OtlobliApi/api/oauth/token", formContent);
            client.Dispose();
            //get access token from response body
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            //var result = JsonConvert.DeserializeObject<YourObject>(responseString);
            JObject jObject = JObject.Parse(responseString);
            return jObject.GetValue("access_token").ToString();
        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            //Set connection
            var connection = new HubConnection("http://localhost/OtlobliApi");
            var t = await GetAccessToken();
            connection.Headers.Add("Authorization", "bearer " + t);
            //Make proxy to hub based on hub name on server
            var myHub = connection.CreateHubProxy("NotificationsHub");
            //Start connection
            connection.Start().Wait();
            myHub.On<OrderForReturn>("NotifyResturantForNewOrder", newOrderExecuted);
            //connection.Stop();
        }
    }
}
