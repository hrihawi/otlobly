﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class ImageForRestaurantTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_ImageForRestaurant_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForRestaurant/Add", new ImageForRestaurantForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_ImageForRestaurant_Return_Created()
        {
            var items = await getRestaurants();
            var Image = new ImageForRestaurantForAdd()
            {
                Order = 0,
                Data = File.ReadAllBytes("res-img.jpg"),
                RestaurantId = items[0].Id,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForRestaurant/Add", Image);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForRestaurant/Add", Image);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForRestaurant/Add", Image);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_ImagesForRestaurant_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "ImageForRestaurant/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_ImageForRestaurant_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "ImageForRestaurant/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_ImageForRestaurant_Should_Return_Data()
        {
            var ress = await getImages();
            if (ress.Count > 0)
            {
                var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "ImageForRestaurant/Get?id=" + ress[0].Id));
                Debug.Print(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            }
            else
                Assert.Fail("no items founded");
        }

        [Test]
        public async void Delete_Valid_ImageForRestaurant_Should_Return_OK()
        {
            var ress = await getImages();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForRestaurant/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<ImageForRestaurantForReturn>> getImages()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "ImageForRestaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<ImageForRestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<ImageForRestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
