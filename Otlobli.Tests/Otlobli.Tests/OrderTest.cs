﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class OrderTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Order_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", new Order());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Order_Return_Created()
        {
            var ress = await getRestaurants();
            var users = await getUsers();
            var meals = await getMeals();
            var Order = new OrderForAdd()
            {
                PaymentType = PaymentTypes.Cash,
                DeliveryAddress = "مرسين - بوزجو جانت الوطن للكمبيوتر",
                UserId = users[0].Id,
                RestaurantId = ress[0].Id,
                FullAddress = "mersin - yenisehir",
                OrderDetails = new List<OrderDetailForAdd>()
                {
                    new OrderDetailForAdd()
                    {
                        MealId = meals[0].Id,
                        Count = 1,
                    },
                    new OrderDetailForAdd()
                    {
                        MealId = meals[1].Id,
                        Count = 2,
                    }
                }
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Order_With_valid_Coupon_Return_Created()
        {
            var ress = await getRestaurants();
            var coupons = await getCoupons();
            var users = await getUsers();
            var meals = await getMeals();
            var Order = new OrderForAdd()
            {
                PaymentType = PaymentTypes.Cash,
                DeliveryAddress = "مرسين - بوزجو جانت الوطن للكمبيوتر",
                UserId = users[0].Id,
                RestaurantId = ress[0].Id,
                FullAddress = "mersin - yenisehir",
                DeliveryFees = 5,
                CouponCode = (coupons.FirstOrDefault(x => x.RestaurantId == ress[0].Id)?? new CouponForReturn()).Code,
                OrderLocation = "34234, 234324",
                OrderDetails = new List<OrderDetailForAdd>()
                {
                    new OrderDetailForAdd()
                    {
                        MealId = meals[0].Id,
                        Count = 5,
                        UserNotes = "بدون مخلل لو سمحت",
                    },
                    new OrderDetailForAdd()
                    {
                        MealId = meals[1].Id,
                        Count = 4,
                        UserNotes = "بدون مخلل لو سمحت",
                    },
                }
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Order_With_Un_Valid_Coupon_Return_Bad_Request()
        {
            var ress = await getRestaurants();
            var coupons = await getCoupons();
            var users = await getUsers();
            var meals = await getMeals();
            var Order = new OrderForAdd()
            {
                PaymentType = PaymentTypes.Cash,
                DeliveryAddress = "مرسين - بوزجو جانت الوطن للكمبيوتر",
                UserId = users[0].Id,
                RestaurantId = ress[0].Id,
                FullAddress = "mersin - yenisehir",
                CouponCode = "unvalid",
                OrderDetails = new List<OrderDetailForAdd>()
                {
                    new OrderDetailForAdd()
                    {
                        MealId = meals[0].Id,
                        Count = 5,
                        UserNotes = "بدون مخلل لو سمحت",
                    },
                    new OrderDetailForAdd()
                    {
                        MealId = meals[1].Id,
                        Count = 4,
                        UserNotes = "بدون مخلل لو سمحت",
                    },
                }
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Add", Order);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Seach_Orders_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Order/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_Orders_In_Last_Hour_Should_Return_Results()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Order/Search?SpecificPeriodValue=1");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Order_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Order/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Order_Should_Return_Data()
        {
            var ress = await getOrders();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Order/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Order_Should_Return_Created()
        {
            var orders = await getOrders();
            var ordToupdate = orders[0];
            ordToupdate.ConfirmDateTime = DateTime.UtcNow;
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Update", ordToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Update_Status_Valid_Order_Should_Return_Created()
        {
            var orders = await getOrders();
            var ord = new OrderForReturnSummary()
            {
                Id = orders[0].Id,
                State = OrderStates.Confirmed
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/UpdateStatus", ord);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Order_Should_Return_OK()
        {
            var ress = await getOrders();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Order/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<OrderForReturn>> getOrders()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Order/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<OrderForReturn> summaries = JsonConvert.DeserializeObject<List<OrderForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<UserForReturn>> getUsers()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Account/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<UserForReturn> summaries = JsonConvert.DeserializeObject<List<UserForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<MealForReturn>> getMeals()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Meal/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MealForReturn> summaries = JsonConvert.DeserializeObject<List<MealForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<CouponForReturn>> getCoupons()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Coupon/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<CouponForReturn> summaries = JsonConvert.DeserializeObject<List<CouponForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
