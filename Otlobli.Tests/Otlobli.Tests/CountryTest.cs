﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class CountryTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Country_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Country/Add", new CountryForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

            //Assert.NotNull(response);
            //    var result = response.Result.StatusCode;
            //    var result2 = await response.Result.Content.ReadAsAsync<dynamic>();
            //    Console.WriteLine((object)result2);

            //var body = response.Content.ReadAsStringAsync().Result;
            //Assert.That(body, Is.StringContaining("5"));
            //Assert.That(response.IsSuccessStatusCode, Is.False);
            //Assert.That(body, Is.StringContaining("The request is invalid."));
        }

        [Test]
        public async void Add_Valid_Country_Return_Created()
        {
            var country = new CountryForAdd()
            {
                NameArabic = "سوريا",
                NameEnglish = "Syria",
            };
            country.Cities.Add(new CityForAdd() { NameArabic = "حلب", NameEnglish = "Aleppo" });
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Country/Add", country);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Country/Add", country);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Country/Add", country);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Countrys_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Country/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Country_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Country/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Country_Should_Return_Data()
        {
            var ress = await getCountrys();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Country/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Country_Should_Return_Created()
        {
            var ress = await getCountrys();
            var resToupdate = ress[0];
            resToupdate.NameEnglish = "NameUpdated";
            var resFinal = AutoMapping.DefaultInstanse.Map<CountryForReturn>(resToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Country/Update", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        private async Task<List<CountryForReturn>> getCountrys()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Country/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<CountryForReturn> summaries = JsonConvert.DeserializeObject<List<CountryForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

    }
}
