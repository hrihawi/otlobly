﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class OrderDetailTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_OrderDetail_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "OrderDetail/Add", new OrderDetailForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_OrderDetail_Return_Created()
        {
            var orders = await getOrders();
            var meals = await getMeals();
            var orderDetail = new OrderDetailForAdd()
            {
                Count = 1,
                MealId = meals[0].Id,
                UserNotes = "a very good ",
                OrderId = orders[0].Id,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "OrderDetail/Add", orderDetail);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_OrderDetails_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "OrderDetail/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_OrderDetail_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "OrderDetail/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_OrderDetail_Should_Return_Data()
        {
            var ress = await getOrderDetails();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "OrderDetail/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_OrderDetail_Should_Return_Created()
        {
            var ress = await getOrderDetails();
            var resToupdate = ress[0];
            resToupdate.Count = 2;
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "OrderDetail/Update", resToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_OrderDetail_Should_Return_OK()
        {
            var ress = await getOrderDetails();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "OrderDetail/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<OrderDetailForReturn>> getOrderDetails()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "OrderDetail/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            return JsonConvert.DeserializeObject<List<OrderDetailForReturn>>(jsonResponse.result.ToString());
        }

        private async Task<List<MealForReturn>> getMeals()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Meal/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MealForReturn> summaries = JsonConvert.DeserializeObject<List<MealForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<OrderForReturn>> getOrders()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Order/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<OrderForReturn> summaries = JsonConvert.DeserializeObject<List<OrderForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
