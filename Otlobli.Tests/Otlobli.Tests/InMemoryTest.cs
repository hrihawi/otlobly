﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Otlobli.Model;

namespace Otlobli.Tests
{
    [TestFixture]
    public abstract class InMemoryTest
    {
        private static string apiBaseUri = "http://localhost";

        protected HttpClient HttpClient;
        
        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            HttpClient = GetAuthenticatedHttpClient();
            AutoMapping.Configure();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
        }

        public HttpClient GetAuthenticatedHttpClient(string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = GetAccessToken().Result;
            }
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUri)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            return client;
        }

        public HttpClient GetUnAuthenticatedHttpClient()
        {
            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUri)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public async Task<string> GetAccessToken(string userName = "OtlobliAdmin", string password = "~Qwe123456")
        {
            FormUrlEncodedContent formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", userName),
                    new KeyValuePair<string, string>("password", password),
                });

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUri)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage responseMessage = await client.PostAsync("/OtlobliApi/api/oauth/token", formContent);
            client.Dispose();
            //get access token from response body
            var responseString = await responseMessage.Content.ReadAsStringAsync();
            //var result = JsonConvert.DeserializeObject<YourObject>(responseString);
            JObject jObject = JObject.Parse(responseString);
            return jObject.GetValue("access_token").ToString();
        }
    }
}