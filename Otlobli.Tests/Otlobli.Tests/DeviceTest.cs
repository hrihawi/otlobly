﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class DeviceTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Device_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Add", new DeviceForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Device_Return_Created()
        {
            var device = new DeviceForAdd()
            {
                Key = "343",
                DeviceName = "Android43",
                OsType = "Android",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Add", device);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Add", device);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Add", device);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Devices_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Device/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Device_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Device/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Device_Should_Return_Data()
        {
            var ress = await getDevices();
            var response = await HttpClient.GetAsync(serverUrl + String.Format("Device/Get?id={0}", ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Device_Should_Return_Created()
        {
            var ress = await getDevices();
            var resToupdate = ress[0];
            resToupdate.Key = "KeyUpdated";
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Update", resToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Device_Should_Return_OK()
        {
            var ress = await getDevices();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Device/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Push_Data_Valid_Device_Should_Return_OK()
        {
            var tRequest = WebRequest.Create("https://gcm-http.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", AppConfiguration.Get("Android:GoogleAppKey", "AIzaSyBjJsNLP04wxULvo9cSvQRFNpTlABMxyuo")));

            using (var streamWriter = new StreamWriter(tRequest.GetRequestStream()))
            {
                dynamic requestData = new
                {
                    to = "APA91bHeDtlt4tqcMZsfwNZsBaWR_LESLh9NqxeNuKzy8_-qotBo41BAuwy5ZkQTAwrI5KmW4S7WETMIRb2MWKWsLRQEMghpr3XItbk7LP8DJt9Uzg7zlRtvqz8PhRhjH3LzAS5A6nwn",
                    data = new OrderForReturn()
                };

                streamWriter.Write(JsonConvert.SerializeObject(requestData));
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)tRequest.GetResponse();
            string response;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }

            Debug.Print(response);
            Assert.AreEqual(HttpStatusCode.OK, HttpStatusCode.OK);

        }

        private async Task<List<DeviceForReturn>> getDevices()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Device/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            var summaries = JsonConvert.DeserializeObject<List<DeviceForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
