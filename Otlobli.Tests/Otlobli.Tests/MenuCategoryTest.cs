﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class MenuCategoryTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_MenuCategory_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Add", new MenuCategoryForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_MenuCategory_Return_Created()
        {
            var MenuCategory = new MenuCategoryForAdd()
            {
                NameEnglish = "Pizza",
                NameArabic = "بيتزا",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Add", MenuCategory);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Add", MenuCategory);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Add", MenuCategory);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_MenuCategorys_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "MenuCategory/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_MenuCategory_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "MenuCategory/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_MenuCategory_Should_Return_Data()
        {
            var ress = await getMenuCategorys();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "MenuCategory/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_MenuCategory_Should_Return_Created()
        {
            var items = await getMenuCategorys();
            var itemToupdate = items[0];
            itemToupdate.NameEnglish = "Pizza Updated";
            //var itemFinal = AutoMapping.Instanse.Map<MenuCategoryForReturn>(itemToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Update", itemToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_MenuCategory_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Delete", new DeleteCriteria() { Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_MenuCategory_Should_Return_OK()
        {
            var items = await getMenuCategorys();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "MenuCategory/Delete", new DeleteCriteria() { Id = items[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<MenuCategoryForReturn>> getMenuCategorys()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "MenuCategory/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MenuCategoryForReturn> summaries = JsonConvert.DeserializeObject<List<MenuCategoryForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
