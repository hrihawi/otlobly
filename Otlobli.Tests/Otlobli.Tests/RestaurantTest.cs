﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class RestaurantTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Restaurant_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Add", new RestaurantForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Restaurant_Return_Created()
        {
            var restaurant = new RestaurantForAdd()
            {
                NameArabic = "المطبخ الشرقي",
                NameEnglish = "TurkishRest",
                Country = "Saudi Arabia",
                City = "Riyadh",
                DescriptionEnglish = "a turkish restaurant",
                DescriptionArabic = "مطعم تركي",
                FullAddressEnglish = "mersin yenisehir",
                FullAddressArabic = "مرسين يني شهير",
                PhoneNumber1 = "1565477685",
                PhoneNumber2 = "8756344565",
                Website = "www.has.com",
                Email = "hasan@h.com",
                Logo = File.ReadAllBytes("res-img.jpg"),
                StartWorkTime = "8:00",
                EndWorkTime = "20:01",
                MinAmountForFreeDelivery = 25,
                DeliveryFees = 5,
                RestaurantType = "Syrian",
                FoodType = "Fast Food",
                GpsLng = "5346",
                GpsLat = "488",
                IsAvailable = true,
                //IsActive = true,
                IsAcceptingDelivery = true,
                Currency = "TL",
                FeesPaymentType = FeesPaymentTypes.Monthly,
                Fees = 400,

            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Add", restaurant);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Add", restaurant);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Add", restaurant);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }


        [Test]
        public async void AssignAdmin_For_Restaurant_Should_Return_Created()
        {
            var ress = await getRestaurants();
            var users = await getUsers();
            var item = new RestaurantUserForAdd()
            {
                ResturantId = ress[0].Id,
                UserEmail = users[0].Email,
                Role = "RestaurantAdmin",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/AssignAdmin", item);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void RemoveAdmin_From_Restaurant_Should_Return_OK()
        {
            var ress = await getRestaurants();
            var users = await getUsers();
            var item = new RestaurantUserForRemove()
            {
                ResturantId = ress[0].Id,
                UserEmail = users[0].Email,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/RemoveAdmin", item);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_Restaurants_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Get_Restaurants_Report_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/GetRestaurantsReport");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_Restaurants_By_RestaurantType_Should_Return_Data()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/Search?RestaurantType=NameUpdated&lng=En");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_By_CategoryType_Restaurants_Should_Return_Rests()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/Search?CategoryType=بيتزا1");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_By_Coords_Restaurants_Should_Return_Near_Rest()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/Search?GpsLat=36.777437&GpsLng=34.568902&GpsDistance=2");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_Summary_Restaurants_Should_Return_All_Summaries()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/SearchSummary");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Restaurant_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Restaurant/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Restaurant_Should_Return_Data()
        {
            var ress = await getRestaurants();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Restaurant/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Restaurant_Should_Return_Created()
        {
            var ress = await getRestaurants();
            var resToupdate = ress[0];
            resToupdate.NameEnglish = "NameUpdated";
            resToupdate.RestaurantType = "NameUpdated";
            resToupdate.FoodType = "NameUpdated";
            resToupdate.Country = "Syria";
            resToupdate.Logo = null;
            var resFinal = AutoMapping.DefaultInstanse.Map<RestaurantForReturn>(resToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Update", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Restaurant_Status_Should_Return_Created()
        {
            var ress = await getRestaurants();
            var resFinal = new RestaurantStatusForAdd()
            {
                ResturantId = ress[0].Id,
                IsActive = true,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/UpdateStatus", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Restaurant_Availability_Should_Return_Created()
        {
            var ress = await getRestaurants();
            var resFinal = new RestaurantAvailabilityForAdd()
            {
                ResturantId = ress[0].Id,
                IsAvailable = true,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/UpdateAvailability", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_MenuCategory_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Delete", new DeleteCriteria() { Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Restaurant_Should_Return_OK()
        {
            var ress = await getRestaurants();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Restaurant/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public void Calculate_Two_Resturants_Coords_Should_Return_OK()
        {
            var sCoord = new GeoCoordinate(42.990967, -71.463767);
            var eCoord = new GeoCoordinate(36.777538, 34.568836);

            Debug.Print(sCoord.GetDistanceTo(eCoord).ToString());

            Assert.AreEqual(HttpStatusCode.OK, HttpStatusCode.OK);
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<UserForReturn>> getUsers()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Account/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<UserForReturn> summaries = JsonConvert.DeserializeObject<List<UserForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
