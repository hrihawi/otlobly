﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class RatingTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Rating_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Add", new RatingForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Rating_Return_Created()
        {
            var orders = await getOrders();
            var rating = new RatingForAdd()
            {
                Rate = 4,
                Comment = "وجبة طيبة كتيير, والتوصيل سريع",
                OrderId = orders[0].Id,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Add", rating);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Add", rating);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Add", rating);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Ratings_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Rating/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Seach_Ratings_For_Restaurant_Should_Return_Data()
        {
            var items = await getRestaurants();
            var response = await HttpClient.GetAsync(serverUrl + "Rating/Search?RestaurantId=" + items[2].Id);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Rating_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Rating/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Rating_Should_Return_Data()
        {
            var ress = await getRatings();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Rating/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Rating_Should_Return_Created()
        {
            var ress = await getRatings();
            var resToupdate = ress[0];
            resToupdate.Rate = 1;
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Update", resToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Rating_Should_Return_OK()
        {
            var ress = await getRatings();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Rating/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<RatingForReturn>> getRatings()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Rating/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RatingForReturn> summaries = JsonConvert.DeserializeObject<List<RatingForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<OrderForReturn>> getOrders()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Order/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<OrderForReturn> summaries = JsonConvert.DeserializeObject<List<OrderForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

    }
}
