﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class RestaurantTypeTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_RestaurantType_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Add", new RestaurantTypeForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_RestaurantType_Return_Created()
        {
            var RestaurantType = new RestaurantTypeForAdd()
            {
                NameArabic = "سوري",
                NameEnglish = "Syrian",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Add", RestaurantType);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Add", RestaurantType);
            var response3 = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Add", RestaurantType);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_RestaurantTypes_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "RestaurantType/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_RestaurantType_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "RestaurantType/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_RestaurantType_Should_Return_Data()
        {
            var ress = await getRestaurantTypes();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "RestaurantType/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_RestaurantType_Should_Return_Created()
        {
            var ress = await getRestaurantTypes();
            var resToupdate = ress[0];
            resToupdate.NameEnglish = "NameUpdated";
            var resFinal = AutoMapping.DefaultInstanse.Map<RestaurantTypeForReturn>(resToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Update", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_MenuCategory_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Delete", new DeleteCriteria() { Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_RestaurantType_Should_Return_OK()
        {
            var ress = await getRestaurantTypes();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "RestaurantType/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<RestaurantTypeForReturn>> getRestaurantTypes()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "RestaurantType/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantTypeForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantTypeForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
