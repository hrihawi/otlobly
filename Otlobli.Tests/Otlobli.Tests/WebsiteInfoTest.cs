﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class WebsiteInfoTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_WebsiteInfo_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Add", new WebsiteInfo());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

            //Assert.NotNull(response);
            //    var result = response.Result.StatusCode;
            //    var result2 = await response.Result.Content.ReadAsAsync<dynamic>();
            //    Console.WriteLine((object)result2);

            //var body = response.Content.ReadAsStringAsync().Result;
            //Assert.That(body, Is.StringContaining("5"));
            //Assert.That(response.IsSuccessStatusCode, Is.False);
            //Assert.That(body, Is.StringContaining("The request is invalid."));
        }

        [Test]
        public async void Add_Valid_WebsiteInfo_Return_Created()
        {
            var websiteInfo = new WebsiteInfo()
            {
                Key = "Owner",
                ValueEnglish = "Mohammad Jarbouh",
                ValueArabic = "محمد جربوع",
            };
            var websiteInfo1 = new WebsiteInfo()
            {
                Key = "Owner",
                ValueEnglish = "Mohammad Jarbouh",
                ValueArabic = "محمد جربوع",
            };
            var websiteInfo2 = new WebsiteInfo()
            {
                Key = "Owner",
                ValueEnglish = "Mohammad Jarbouh",
                ValueArabic = "محمد جربوع",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Add", websiteInfo);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Add", websiteInfo1);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Add", websiteInfo2);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_WebsiteInfos_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "WebsiteInfo/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_WebsiteInfo_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "WebsiteInfo/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_WebsiteInfo_Should_Return_Data()
        {
            var ress = await getWebsiteInfos();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "WebsiteInfo/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_WebsiteInfo_Should_Return_Created()
        {
            var ress = await getWebsiteInfos();
            var resToupdate = ress[0];
            resToupdate.Key = "KeyUpdated";
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Update", resToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_WebsiteInfo_Should_Return_OK()
        {
            var ress = await getWebsiteInfos();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "WebsiteInfo/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<WebsiteInfo>> getWebsiteInfos()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "WebsiteInfo/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<WebsiteInfo> summaries = JsonConvert.DeserializeObject<List<WebsiteInfo>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
