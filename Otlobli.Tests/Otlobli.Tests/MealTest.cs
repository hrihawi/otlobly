﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class MealTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Meal_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Add", new MealForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Meal_Return_Created()
        {
            var menues = await getMenues();
            var meal = new MealForAdd()
            {
                NameEnglish = "two person pizza menu",
                NameArabic = "قائمة بيتزا شخصين",
                DescriptionArabic = "جبنة ,سجق ,فطر ,صلصة الطماطم, صلصة حارة",
                DescriptionEnglish = "cheese, tomates",
                Price = 21.5,
                MenuId = menues[0].Id,
                Images = new List<ImageForMealForAdd>()
                {
                    new ImageForMealForAdd()
                    {
                        Order = 0,
                        Data = new byte[1],
                    }
                },
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Add", meal);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Add", meal);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Add", meal);
            var response3 = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Add", meal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Meals_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Meal/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Meal_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Meal/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Meal_Should_Return_Data()
        {
            var ress = await getMeals();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Meal/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Meal_Should_Return_Created()
        {
            var items = await getMeals();
            var itemToupdate = items[0];
            itemToupdate.NameEnglish = "Pizza Updated";
            //var itemFinal = AutoMapping.Instanse.Map<MealForReturn>(itemToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Update", itemToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_Meal_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Delete", new DeleteCriteria() { Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Meal_Should_Return_OK()
        {
            var items = await getMeals();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Meal/Delete", new DeleteCriteria() { Id = items[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<MealForReturn>> getMeals()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Meal/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MealForReturn> summaries = JsonConvert.DeserializeObject<List<MealForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<MenuForReturn>> getMenues()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Menu/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MenuForReturn> summaries = JsonConvert.DeserializeObject<List<MenuForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
