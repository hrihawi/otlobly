﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class AccountTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_User_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Add", new UserForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_User_Return_Created()
        {
            Random r = new Random();
            var Account = new UserForAdd()
            {
                Email = "h.rihawi" + r.Next(1, 1000) + "@idscan.eu",
                Username = "hasanUser1" + r.Next(1000, 2000),
                FirstName = "hasan" + r.Next(2000, 3000),
                LastName = "rihawi",
                PhoneNumber = "905374841382",
                Password = "~Qwe123456",
                ConfirmPassword = "~Qwe123456",
                Image = File.ReadAllBytes("res-img.jpg"),
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Add", Account);
            //var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Add", Account);
            //var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Add", Account);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Confirm_Valid_User_Phone_Number_Return_Created()
        {
            var model = new ConfirmPhoneNumber()
            {
                Code = "160707"
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/ConfirmPhoneNumber", model);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }


        [Test]
        public async void Seach_Users_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Account/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_User_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Account/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_User_Should_Return_Data()
        {
            var ress = await getUsers();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Account/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_User_Should_Return_Created()
        {
            var ress = await getUsers();
            var resToupdate = ress[0];
            resToupdate.FirstName = "NameUpdated";
            var resFinal = AutoMapping.DefaultInstanse.Map<UserForReturn>(resToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Update", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_User_Status_Should_Return_Created()
        {
            var ress = await getUsers();
            var resFinal = new UserStatusForAdd()
            {
                UserId = ress[0].Id,
                IsActive = true,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/UpdateStatus", resFinal);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_Account_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/Delete", new DeleteCriteria() { Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Assign_Roles_To_User_Should_Return_OK()
        {
            var users = await getUsers();
            var roleForAssign = new RoleForAssign()
            {
                UserId = users[0].Id.ToString(),
                Role = "WebsiteAdmin",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/AssignRolesToUser", roleForAssign);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Forgot_Password_Should_Return_OK()
        {
            var users = await getUsers();
            var model = new ForgotPasswordModel()
            {
                Email = users[0].Email,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/ForgotPassword", model);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Reset_Password_Should_Return_OK()
        {
            var users = await getUsers();
            var model = new ResetPasswordModel()
            {
                UserId = users[0].Id,
                Code = "WGta7i4PvjofrowjKtAHn37CxFDRoGdcgjb2psLuA844XsVwjXNL37hV3f1Esi9TydxpSz7Ja3wkQ0rIz3NS/q+448znJ6vATIr/BudmGEFJ/xC7KHnge6Gru/+wMpyKAuY5ieZ8Ddzez2gxir1WcF5ToKZoLHK+eoUFVVWyjBGPIRHY5HANd6AGrZK4UPZ3ULoRK5vlyhU4A228e3wa5A==",
                NewPassword = "~Qwe123456",
                ConfirmPassword = "~Qwe123456",
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Account/ResetPassword", model);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }


        private async Task<List<UserForReturn>> getUsers()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Account/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<UserForReturn> summaries = JsonConvert.DeserializeObject<List<UserForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
