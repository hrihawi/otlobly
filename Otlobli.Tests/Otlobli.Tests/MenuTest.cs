﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class MenuTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Menu_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Add", new MenuForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

            //Assert.NotNull(response);
            //    var result = response.Result.StatusCode;
            //    var result2 = await response.Result.Content.ReadAsAsync<dynamic>();
            //    Console.WriteLine((object)result2);

            //var body = response.Content.ReadAsStringAsync().Result;
            //Assert.That(body, Is.StringContaining("5"));
            //Assert.That(response.IsSuccessStatusCode, Is.False);
            //Assert.That(body, Is.StringContaining("The request is invalid."));
        }

        [Test]
        public async void Add_Valid_Menu_Return_Created()
        {
            var categories = await getMenuCategorys();
            var restaurants = await getRestaurants();
            var menu = new MenuForAdd()
            {
                NameEnglish = "Pizza",
                NameArabic = "بيتزا",
                CategoryId = categories.First().Id,
                RestaurantId = restaurants.First().Id
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Add", menu);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Add", menu);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Add", menu);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Menus_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Menu/Search?RestaurantId=fde440f8-860f-4691-96be-516f4fc16c34");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Menu_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Menu/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Menu_Should_Return_Data()
        {
            var ress = await getMenus();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Menu/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Menu_Should_Return_Created()
        {
            var items = await getMenus();
            var itemToupdate = items[0];
            itemToupdate.NameEnglish = "Pizza Updated";
            //var itemFinal = AutoMapping.Instanse.Map<MenuForReturn>(itemToupdate);
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Update", itemToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Not_Valid_Menu_Should_Return_NotFound()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Delete", new DeleteCriteria() {Id = new Guid("F8F56CE2-AB65-4732-B98E-CAED51CD5D00") });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Menu_Should_Return_OK()
        {
            var items = await getMenus();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Menu/Delete", new DeleteCriteria() { Id = items[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<MenuForReturn>> getMenus()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Menu/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MenuForReturn> summaries = JsonConvert.DeserializeObject<List<MenuForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<MenuCategoryForReturn>> getMenuCategorys()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "MenuCategory/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MenuCategoryForReturn> summaries = JsonConvert.DeserializeObject<List<MenuCategoryForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
