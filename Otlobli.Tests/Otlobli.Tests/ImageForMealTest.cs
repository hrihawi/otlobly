﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class ImageForMealTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_ImageForMeal_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForMeal/Add", new ImageForRestaurantForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [Test]
        public async void Add_Valid_ImageForMeal_ForMeal_Return_Created()
        {
            var items = await getMeals();
            var Image = new ImageForMealForAdd()
            {
                Order = 0,
                Data = File.ReadAllBytes("res-img.jpg"),
                MealId = items[0].Id,
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForMeal/Add", Image);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForMeal/Add", Image);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForMeal/Add", Image);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_ImagesForMeal_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "ImageForMeal/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_ImagesForMeal_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "ImageForMeal/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_ImageForMeal_Should_Return_Data()
        {
            var ress = await getImages();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "ImageForMeal/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_ImageForMeal_Should_Return_OK()
        {
            var items = await getImages();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "ImageForMeal/Delete", new DeleteCriteria() { Id = items[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<ImageForMeal>> getImages()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "ImageForMeal/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<ImageForMeal> summaries = JsonConvert.DeserializeObject<List<ImageForMeal>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<MealForReturn>> getMeals()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Meal/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<MealForReturn> summaries = JsonConvert.DeserializeObject<List<MealForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }
    }
}
