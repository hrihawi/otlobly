﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using NUnit.Framework;
using Otlobli.Model;
using Assert = NUnit.Framework.Assert;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Otlobli.Tests
{
    [TestFixture]
    public class CouponTest : InMemoryTest
    {
        public static string serverUrl = "http://localhost/OtlobliApi/api/";

        [Test]
        public async void Add_UnValid_Coupon_Return_BadRequest()
        {
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Add", new CouponForAdd());
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Add_Valid_Coupon_Return_Created()
        {
            var ress = await getRestaurants();
            var coupon = new CouponForAdd()
            {
                ExpiryDateTime = DateTime.UtcNow.AddDays(10),
                DiscountValue = 10,
                Limit = 50,
                RestaurantId = ress[0].Id,
                MinimumBillValue = 100
            };
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Add", coupon);
            var response1 = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Add", coupon);
            var response2 = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Add", coupon);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Seach_Coupons_Should_Return_All()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Coupon/Search");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void GetByID_Not_Valid_Coupon_Should_Return_NotFound()
        {
            var response = await HttpClient.GetAsync(serverUrl + "Coupon/Get?id=9F9A78B3-00B0-44C5-B6E1-2C668977EE6E");
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public async void GetByID_Valid_Coupon_Should_Return_Data()
        {
            var ress = await getCoupons();
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Coupon/Get?id=" + ress[0].Id));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async void Check_Un_Valid_Coupon_Should_Return_BadRequest()
        {
            var response = await HttpClient.GetAsync(string.Format("{0}{1}", serverUrl, "Coupon/CheckValidity?CouponCode=o23jjh"));
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public async void Update_Valid_Coupon_Should_Return_Created()
        {
            var coupons = await getCoupons();
            var couponToupdate = coupons[0];
            couponToupdate.HitsCount = 3;
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Update", couponToupdate);
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [Test]
        public async void Delete_Valid_Coupon_Should_Return_OK()
        {
            var ress = await getCoupons();
            var response = await HttpClient.PostAsJsonAsync(serverUrl + "Coupon/Delete", new DeleteCriteria() { Id = ress[0].Id });
            Debug.Print(response.Content.ReadAsStringAsync().Result);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        private async Task<List<CouponForReturn>> getCoupons()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Coupon/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<CouponForReturn> summaries = JsonConvert.DeserializeObject<List<CouponForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

        private async Task<List<RestaurantForReturn>> getRestaurants()
        {
            var resResponse = await HttpClient.GetAsync(serverUrl + "Restaurant/Search");
            var data = resResponse.Content.ReadAsStringAsync().Result;
            dynamic jsonResponse = JsonConvert.DeserializeObject<dynamic>(data);
            List<RestaurantForReturn> summaries = JsonConvert.DeserializeObject<List<RestaurantForReturn>>(jsonResponse.result.ToString());
            return summaries;
        }

    }
}
