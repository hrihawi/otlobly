(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.widgets',
        'app.layout',
        'app.views',
        'app.messages'
       
    ]);

    angular.module('app').run(appRun);

    appRun.$inject = ['common'];
    function appRun(common) {
        var logger = common.logger;

        //logger.success('Welcome To Ot');
    }
})();
