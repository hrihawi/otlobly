﻿(function () {
    'use strict';

    angular
        .module('app.layout')
         .directive('stopPropagate', function () {
             return {
                 restrict: 'C',
                 link: function (scope, element) {
                     element.on('click', function (event) {
                         event.stopPropagation();
                     });
                 }
             }
         })
      .directive('aPrevent', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                element.on('click', function (event) {
                    event.preventDefault();
                });
            }
        }
    })

     .directive('print', function () {
         return {
             restrict: 'A',
             link: function (scope, element) {
                 element.click(function () {
                     window.print();
                 })
             }
         }
     })
    

})();
