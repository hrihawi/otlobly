﻿(function () {
    'use strict';

    angular
        .module('app.layout')
        .service('layoutService', layoutService);

    layoutService.$inject = ['exception'];
    /* @ngInject */
    function layoutService(exception) {
        var vm = this;

        vm.sidebarToggle = {
            left: false,
            right: false
        };

        //vm.sidebarStat = function (event) {
        //    if (!angular.element(event.target).parent().hasClass('active')) {
        //        this.sidebarToggle.left = false;
        //    }
        //}
        var service = {
            setSidebarToggle: setSidebarToggle

        };
        function setSidebarToggle(left,right) {
         return vm.sidebarToggle = {
                left: left,
                right: right
            }
        };

       
    }



})();

