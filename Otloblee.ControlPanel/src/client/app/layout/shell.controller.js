﻿(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', ShellController);

    ShellController.$inject = ['$timeout', '$state', '$scope', 'growlService', 'layoutService'];
    /* @ngInject */
    function ShellController($timeout, $state, $scope, growlService, layoutService) {
        var vm = this;
       
            //Welcome Message
            // Detact Mobile Browser
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                angular.element('html').addClass('ismobile');
            }
            // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
            vm.sidebarToggle = layoutService.sidebarToggle; 
            // By default template has a boxed layout
            vm.layoutType = localStorage.getItem('ma-layout-status');
            // For Mainmenu Active Class
            vm.$state = $state;

            //Close sidebar on click
            vm.sidebarStat = function (event) {
                if (!angular.element(event.target).parent().hasClass('active')) {
                    this.sidebarToggle.left = false;
                }
            }

            //Listview Search (Check listview pages)
            vm.listviewSearchStat = false;

            vm.lvSearch = function () {
                this.listviewSearchStat = true;
            }

            //Listview menu toggle in small screens
            vm.lvMenuStat = false;

            //Blog
            vm.wallCommenting = [];

            vm.wallImage = false;
            vm.wallVideo = false;
            vm.wallLink = false;

            //Skin Switch
            vm.currentSkin = 'blue';

            vm.skinList = [
                'lightblue',
                'bluegray',
                'cyan',
                'teal',
                'green',
                'orange',
                'blue',
                'purple'
            ]

            vm.skinSwitch = function (color) {
                this.currentSkin = color;
            }

        
    }
})();
