﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantsController', restaurantsController);

    restaurantsController.$inject = ['$filter', 'common', '$stateParams', 'ngTableParams', 'restaurantsService', 'askDialog', 'ngDialog'];

    function restaurantsController($filter, common, $stateParams, ngTableParams, restaurantsService, askDialog, ngDialog) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.item = {};
        var data;
        vm.changeStatus = changeStatus;
        vm.addNewRestaurant = addNewRestaurant;
        vm.deleteResturant = deleteResturant;

        function addNewRestaurant() {
            ngDialog.open({
                template: 'app/views/restaurants/addNewRestaurant/addNewRestaurant.html',
                controller: 'addNewRestaurantController',
                controllerAs: 'vm',
                className: 'ngdialog-theme-default'
            });
        }
        activate();

        function activate() {
            restaurantsService.getAll().then(function (result) {
                data = result;
                /*    vm.tableSorting = new ngTableParams({
                        page: 1,            // show first page
                        count: 10,           // count per page
                        sorting: {
                            name: 'asc'     // initial sorting
                        }
                    }, {
                            total: data.length, // length of data
                            getData: function ($defer, params) {
                                // use build-in angular filter
                                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                 */
                vm.tableSorting = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,
                    sorting: {
                        name: 'asc'     // initial sorting
                    }
                }, {
                        total: data.length, // length of data
                        getData: function ($defer, params) {
                            // use build-in angular filter
                            var orderedData = params.filter() ? $filter('filter')(data, params.filter()) : data;

                            this.nameArabic = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.email = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.createdByUser = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.country = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.feesPaymentType = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                            params.total(orderedData.length); // set total for recalc pagination
                            $defer.resolve(this.nameArabic, this.email, this.createdByUser, this.country, this.feesPaymentType);
                        }
                    })

            });


        }


        function changeStatus(userid, status) {
            askDialog.confirm(
                'Messages.ChangeActivationQ', "warning", "Messages.ChangeActivation", function (success) {
                    restaurantsService.changeActivation(userid, status).then(function () {
                        activate();
                        success();
                    });
                }
            );
        }

        function deleteResturant(id) {
            askDialog.confirm(
                'Messages.DeleteResturantQ', "warning", "Messages.DeleteResturant", function (success) {
                    restaurantsService.deleteRestaurant(id, status).then(function () {
                        activate();
                        success();
                    });
                }
            );
        }


    }
})();
