﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper,access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'restaurants',
                config: {
                    url: '/restaurants',
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/restaurants/restaurants.html',
                            controller: 'restaurantsController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.User
                    }
                }
            },
             {
                 state: 'restaurants.viewRestaurantsIAdded',
                 config: {
                     url: '/viewRestaurantsIAdded',
                     views: {
                         'header': {
                             templateUrl: 'app/layout/header/header.html'
                         },
                         'footer': {
                             templateUrl: 'app/layout/footer/footer.html'
                         },
                         'content@': {
                             templateUrl: 'app/views/restaurants/viewRestaurantsIAdded/viewRestaurantsIAdded.html',
                             controller: 'viewRestaurantsIAdded',
                             controllerAs: 'vm'
                         }
                     },
                     data: {
                         access: access.WebsiteManager
                     }
                 }
             },
            
            
            {
                state: 'restaurants.addNewRestaurant',
                config: {
                    url: '/addNewRestaurant',
                    views: {
                        'content@': {
                            templateUrl: 'app/views/restaurants/addNewRestaurant/addNewRestaurant.html',
                            controller: 'addNewRestaurantController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.User
                    }
                }
            },
            {
                state: 'restaurants.profile',
                config: {
                    url: '/profile/:resturantId',
                    views: {
                        'content@': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/restaurantProfile.html',
                            controller: 'restaurantProfileController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.User
                    }
                }
            },
            {
                state: 'restaurants.profile.information',
                config: {
                    url: '/information',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/about/about.html',
                            controller: 'restaurantAbout',
                            controllerAs: 'vm'
                        }
                    },

                    data: {
                        access: access.User
                    }
                }
            } ,
            {
                state: 'restaurants.profile.map',
                config: {
                    url: '/map',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/map/map.html',
                            controller: 'restaurantMap',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.profile.menus',
                config: {
                    url: '/menus',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/menus/menus.html',
                            controller: 'restaurantMenus',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.profile.photos',
                config: {
                    url: '/photos',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/photos/photos.html',
                            controller: 'restaurantPhotos',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.profile.users',
                config: {
                    url: '/users',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/users/users.html',
                            controller: 'restaurantUsers',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.profile.coupons',
                config: {
                    url: '/coupons',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/coupons/coupons.html',
                            controller: 'restaurantCoupons',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
            {
                state: 'restaurants.profile.subscribingType',
                config: {
                    url: '/subscribingType',
                    views: {
                        'details': {
                            templateUrl: 'app/views/restaurants/restaurantProfile/subscribingType/subscribingType.html',
                            controller: 'subscribingType',
                            controllerAs: 'vm'
                        }
                    },

                    data: {
                        access: access.User
                    }
                }
            }
        ];
    }
})();
