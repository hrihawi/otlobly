﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('restaurantsService', restaurantsService);

    restaurantsService.$inject = ['$http', '$location', 'common', 'exception'];
    /* @ngInject */
    function restaurantsService($http, $location, common, exception) {

        var logger = common.logger;
        var localRestaurantInfo = null;
        var localMenusCategorise = null;
        var selectedResturantId;
        var service = {
            getAll: getAllRestaurants,
            getAllSummary: getAllRestaurantsSummary,
            getAllIAdded: getAllRestaurantsIAdded,
            add: addNewRestaurant,
            changeActivation: changeRestaurantActivation,
            deleteRestaurant: deleteRestaurant,
            resturantInfo: {
                getById: getRestaurantInfo,
                setRestaurantInfo: setRestaurantInfo,
                getSelectedResturant: getSelectedResturant,
                uploadLogo: uploadLogo
            },
            Menus: {
                get: getMenus,
                getById: getMenuInfo,
                getCategories: getMenusCategorise,
                Add: AddMenus,
                edit: editMenus,
                deleteMenu: deleteMenu,
                meals: {
                    get: getMeals,
                    getById: getMealInfo,
                    add: addMeals,
                    update: editMeals,
                    deleteMeal: deleteMeal
                }
            },
            photos: {
                get: getPhotos,
                add: addPhotos,
                edit: editPhotos
            },
            users: {
                get: getUsers,
                add: addUser,
                remove: removeUser
            },
            general: {
                countries: getCountries,
                foodTypes: getFoodTypes,
                resturantTypes: getRestaurantTypes,
                paymentTypes: getPaymentTypes

            }
        };

        return service;

        function getLang() {
            return 'Ar';
        }

        function addNewRestaurant(restaurantInfo) {
            return $http.post('api/Restaurant/Add', {
                nameArabic: restaurantInfo.nameArabic,
                nameEnglish: restaurantInfo.nameEnglish,
                country: restaurantInfo.country,
                phoneNumber1: restaurantInfo.phoneNumber1,
                email: restaurantInfo.email,
                feesPaymentType: restaurantInfo.feesPaymentType,
                fees: restaurantInfo.fees,
                startWorkTime: '08:00',
                endWorkTime: '23:59'
            })
                .then(addNewRestaurantComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add new restaurant')(message);
                });

            function addNewRestaurantComplete(data, status, headers, config) {
                console.log(data);
                return data.data;

            }
        }
        function getAllRestaurantsIAdded() {
            return $http.get('api/Restaurant/Search')
                .then(getAllRestaurantsIAddedComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsIAddedComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function getAllRestaurants() {
            return $http.get('api/Restaurant/Search?DisplayCount=100000')
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                console.log(data);
                return data.data.result;
            }
        }

        function getAllRestaurantsSummary() {
            return $http.get('api/Restaurant/SearchSummary?DisplayCount=100000')
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                console.log(data);
                return data.data.result;
            }

        }

        function getSelectedResturant() {
            console.log(localRestaurantInfo);

            if (localRestaurantInfo) {
                return common.$q.when(localRestaurantInfo);
            }
            return common.$q.when(null);
        }

        function getRestaurantInfo(restaurantId, force) {
            if (localRestaurantInfo && localRestaurantInfo.id == restaurantId && force != true) {
                return common.$q.when(localRestaurantInfo);
            }
            selectedResturantId = restaurantId;
            return $http.get('api/Restaurant/Get?id=' + restaurantId + '&Lng=' + getLang())
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                console.log('****');
                console.log(data);
                localRestaurantInfo = data.data.result;
                return localRestaurantInfo;
            }
            //return {
            //    id: 'fb34546e-de7a-4d78-8912-6b2963c80833',
            //    name: 'AL Dana',
            //    arabicName: 'الدانا',
            //    country: 'Turkey',
            //    restaurantType: 'Syrian',
            //    foodType: 'Fast Food',
            //    fullAddress: 'Turkey- Mersin - Mazitli 435mah No:44',
            //    startWorkTime: '08:00',
            //    endWorkTime: '05:00',
            //    minFreeDelivery: '20',
            //    deliveryFee: '10',
            //    currency: 'TRY',
            //    logo: '',
            //    referenceCode: '4534trettey3566',
            //    createdDateTime: '16/01/2016',
            //    phoneNumber1: '(382)-122-5003',
            //    phoneNumber2: '(382)-122-5443',
            //    website: 'www.aldana.com',
            //    email: 's.ghannoum@hotmail.com',
            //    description: 'some texts',
            //    fees: '440',
            //    feesPaymentType: 'Monthly',
            //    gPSLng: '10.55446',
            //    gPSLat: '11.444444',
            //    isAvailable: true,
            //    isSuspended: false,
            //}
        }

        function changeRestaurantActivation(restaurantId, state) {
            return $http.post('api/Restaurant/UpdateStatus', {
                resturantId: restaurantId,
                isActive: state,
            })
                .then(changeRestaurantActivationComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for change Restaurant Activation')(message);
                });

            function changeRestaurantActivationComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function deleteRestaurant(restaurantId) {
            return $http.post('api/Restaurant/Delete', {
                id: restaurantId,
            })
                .then(changeRestaurantActivationComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for change Restaurant Activation')(message);
                });

            function changeRestaurantActivationComplete(data, status, headers, config) {
                return data.data.result;
            }
        }


        function setRestaurantInfo(restaurant) {
            restaurant.managers = [];
            restaurant.logo = undefined;

            return $http.post('api/Restaurant/update', restaurant)
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function uploadLogo(restaurantId, logo) {
            return $http.post('api/Restaurant/UploadLogo', {
                ResturantId: restaurantId,
                logo: logo.split("base64,")[1]

            })
                .then(getAllRestaurantsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Getting Restaurants')(message);
                });

            function getAllRestaurantsComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function setMapLocation(restaurantId, location) {

        }

        function getMenus(restaurantId) {

            return $http.get('api/menu/Search?RestaurantId=' + restaurantId + '&Lng=' + getLang())
                .then(getMenusComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get menus')(message);
                });

            function getMenusComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function getMenuInfo(menuId) {

        }

        function getMenusCategorise() {
            if (localMenusCategorise) {
                return common.$q.when(localMenusCategorise);
            }
            return $http.get('api/MenuCategory/Search?Lng=' + getLang())
                .then(getMenusCategoriseComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get menus')(message);
                });

            function getMenusCategoriseComplete(data, status, headers, config) {
                localMenusCategorise = data.data.result;
                return data.data.result;
            }
        }

        function AddMenus(restaurantId, menu) {
            return $http.post('api/menu/Add', {
                nameArabic: menu.nameArabic,
                nameEnglish: menu.nameEnglish,
                restaurantId: restaurantId,
                categoryId: menu.categoryId
            })
                .then(AddMenusComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Add Menus')(message);
                });

            function AddMenusComplete(data, status, headers, config) {
                return data.data.result;

            }
        }
        function editMenus(restaurantId, menu) {

        }
        function deleteMenu(restaurantId, menuId) {

        }
        function getMeals(restaurantId, menuId) {

        }
        function getMealInfo(mealId) {

        }
        function addMeals(meal) {
            console.log(meal);
            return $http.post('api/Meal/Add', {
                nameArabic: meal.nameArabic,
                nameEnglish: meal.nameEnglish,
                descriptionArabic: meal.descriptionArabic,
                descriptionEnglish: meal.descriptionEnglish,
                price: meal.price,
                isActive: true,
                menuId: meal.menuId,
                images: [
                    {
                        data: meal.image.split("base64,")[1]
                    }
                ]
            })
                .then(addMealsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add Meals')(message);
                });

            function addMealsComplete(data, status, headers, config) {
                return data.data.result;

            }
        }

        function editMeals(meal) {
            return $http.post('api/Meal/update', {
                id: meal.id,
                nameArabic: meal.nameArabic,
                nameEnglish: meal.nameEnglish,
                descriptionArabic: meal.descriptionArabic,
                descriptionEnglish: meal.descriptionEnglish,
                price: meal.price,
                isActive: true,
                menuId: meal.menuId
            })
                .then(addMealsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add Meals')(message);
                });

            function addMealsComplete(data, status, headers, config) {
                return data.data.result;

            }
        }

        function deleteMeal(mealId) {
                return $http.post('api/meal/delete', {
                Id:mealId
            })
                .then(deleteMealComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Add Menus')(message);
                });

            function deleteMealComplete(data, status, headers, config) {
                return data.data.result;

            }
        }
        function getPhotos(restaurantId) {
            return $http.get('api/ImageForRestaurant/Search?RestaurantId='+restaurantId)
                .then(getPhotosComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get Photos')(message);
                });

            function getPhotosComplete(data, status, headers, config) {
                return data.data.result;
            }
        }
        function addPhotos(data) {
            return $http.post('api/ImageForRestaurant/Add', {
                restaurantId: data.restaurantId,
                data: data.image.split("base64,")[1]
            })
                .then(addPhotosComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add Photos')(message);
                });

            function addPhotosComplete(data, status, headers, config) {
                return data.data;
            }
        }
        function editPhotos(photoId) {

        }

        function getUsers(restaurantId) {
            return $http.get('api/users/Search?Lng=' + getLang())
                .then(getFoodTypesComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get FoodTypes')(message);
                });

            function getFoodTypesComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function removeUser(userId) {

        }

        function addUser(restaurantId, email, roleName) {
            return $http.post('api/Restaurant/AssignAdmin', {
                UserEmail: email,
                ResturantId: restaurantId,
                Role: roleName
            })
                .then(addUserComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for add User ')(message);
                });

            function addUserComplete(data, status, headers, config) {
                return localRestaurantInfo;
            }
        }

        function getCountries() {
            return common.$q.when([
                {
                    DisplayName: 'المملكة العربية السعودية',
                    Value: 'المملكة العربية السعودية'
                },
                {
                    DisplayName: 'الامارات العربية',
                    Value: 'الامارات العربية'
                },
                {
                    DisplayName: 'الكويت',
                    Value: 'الكويت'
                },
                {
                    DisplayName: 'تركيا',
                    Value: 'تركيا'
                },
                {
                    DisplayName: 'مصر',
                    Value: 'مصر'
                }
            ]);
        }

        function getFoodTypes() {
            return $http.get('api/FoodType/Search?Lng=' + getLang())
                .then(getFoodTypesComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get FoodTypes')(message);
                });

            function getFoodTypesComplete(data, status, headers, config) {
                return data.data.result;
            }
        }
        function getRestaurantTypes() {

            return $http.get('api/RestaurantType/Search?Lng=' + getLang())
                .then(RestaurantTypeComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get RestaurantType')(message);
                });

            function RestaurantTypeComplete(data, status, headers, config) {

                return data.data.result;
            }


        }

        function getPaymentTypes() { }
    }
})();


