﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('addNewRestaurantController', addNewRestaurantController);

    addNewRestaurantController.$inject = ['$scope', 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ngDialog', 'userManagmentService','$state'];

    function addNewRestaurantController($scope, common, $stateParams, growlService, restaurantsService, askDialog, ngDialog, userManagmentService, $state) {
        /* jshint validthis:true */
        var vm = this;
        var file, data;
        var logger = common.logger;
        vm.submit = submit;
        activate();
        vm.countries = [];
        function activate() {

            countries();
        }
        function countries() {

            restaurantsService.general.countries().then(function (result) {
                vm.countries = result;
            });

        }
        function submit(data) {
            restaurantsService.add(data).then(function (result) {
                if (result) {
                    growlService.growl('تمت الاضافة بنجاح', 'success');
                    $state.go('restaurants');
                }

            });
        }

      

       


    }
})();
