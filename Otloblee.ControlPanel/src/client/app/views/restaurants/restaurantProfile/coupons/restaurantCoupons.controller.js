﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantCoupons', restaurantCoupons);

    restaurantCoupons.$inject = [ 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog'];

    function restaurantCoupons(common, $stateParams, growlService, restaurantsService, askDialog) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;

        activate();

        function activate() {
        }

    }
})();
