﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('subscribingType', subscribingType);

    subscribingType.$inject = ['common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog'];

    function subscribingType(common, $stateParams, growlService, restaurantsService, askDialog) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.restaurantInfo = {};
        vm.editContent = 0;
        vm.showEdit = function (section) {
            if (section === 'editContent') {
                vm.edit = angular.copy(vm.restaurantInfo);
                vm.editContent = 1;
            }
        }
        vm.submit = function (item, message, edit) {
            console.log(edit);
            if (item === 'Subscribe') {
                vm.editContent = 0;
            }
            vm.restaurantInfo = angular.copy(vm.edit);
            vm.edit = {};
            restaurantsService.resturantInfo.setRestaurantInfo(vm.restaurantInfo).then(
                 function (result) {
                     growlService.growl('تم التعديل بنجاح', 'inverse');
                 });
        }

        activate();

        function activate() {
            restaurantsService.resturantInfo.getById($stateParams.resturantId).then(function (result) {
                vm.restaurantInfo = result;

            });
        }

    }
})();
