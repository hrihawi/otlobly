﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantMap', restaurantMap);

    restaurantMap.$inject = ['$filter', 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', '$geolocation'];

    function restaurantMap($filter, common, $stateParams, growlService, restaurantsService, askDialog, $geolocation) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.restaurantInfo = {};
        vm.saveLocation = saveLovation;
        vm.getLocation = getLocation;
        vm.changedHappen = false;
        activate();

        function activate() {
            restaurantsService.resturantInfo.getById($stateParams.resturantId).then(function (result) {
                vm.restaurantInfo = result;
                vm.map = { center: { latitude: vm.restaurantInfo.gpsLat, longitude: vm.restaurantInfo.gpsLng }, zoom: 8 };

                vm.marker = {
                    id: 0,
                    coords: {
                        latitude: vm.restaurantInfo.gpsLat,
                        longitude: vm.restaurantInfo.gpsLng
                    },
                    options: { draggable: true },
                    events: {
                        dragend: function (marker, eventName, args) {
                            vm.changedHappen = true;
                            vm.marker.options = {
                                draggable: true,
                                labelContent: "موقع المطعم",
                                labelAnchor: "0 0",
                                labelClass: "marker-labels"
                            };
                        }
                    }
                };
            });
        }

        function saveLovation() {
            vm.restaurantInfo.gpsLat = vm.marker.coords.latitude;
            vm.restaurantInfo.gpsLng = vm.marker.coords.longitude;
            console.log(vm.restaurantInfo.gpsLng);
            restaurantsService.resturantInfo.setRestaurantInfo(vm.restaurantInfo).then(
            function (result) {
                growlService.growl('تم التعديل بنجاح', 'inverse');
                vm.changedHappen = false;

            });
        }

        function getLocation() {
         $geolocation.getCurrentPosition({
                timeout: 60000
         }).then(function (position) {
             vm.changedHappen = true;

                vm.myPosition = position.coords.latitude;
                vm.map = {
                    center:
                        { latitude: position.coords.latitude, longitude: position.coords.longitude }
                    , zoom: 15
                };
                vm.marker.coords.latitude = position.coords.latitude;
                vm.marker.coords.longitude = position.coords.longitude;
            });


        }

    }
})();
