﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantAbout', restaurantAbout);

    restaurantAbout.$inject = ['$filter', 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', '$geolocation'];

    function restaurantAbout($filter, common, $stateParams, growlService, restaurantsService, askDialog, $geolocation) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.restaurantInfo = {};
        //User
        vm.profileSummary = "Sed eu est vulputate, fringilla ligula ac, maximus arcu. Donec sed felis vel magna mattis ornare ut non turpis. Sed id arcu elit. Sed nec sagittis tortor. Mauris ante urna, ornare sit amet mollis eu, aliquet ac ligula. Nullam dolor metus, suscipit ac imperdiet nec, consectetur sed ex. Sed cursus porttitor leo.";

        vm.emailAddress = "malinda.h@gmail.com";
        vm.twitter = "@malinda";
        vm.twitterUrl = "twitter.com/malinda";
        vm.skype = "malinda.hollaway";
        vm.addressSuite = "44-46 Morningside Road";
        vm.addressCity = "Edinburgh";
        vm.addressCountry = "Scotland";
        vm.edit = {};
        //Edit
        vm.editSummary = 0;
        vm.editInfo = 0;
        vm.editContact = 0;
        vm.editRules = 0;
        vm.showEdit = function (section) {
            if (section === 'editInfo') {
                vm.edit = angular.copy(vm.restaurantInfo);
                
                vm.editInfo = 1;
            }
            if (section === 'editRules') {
                vm.edit = angular.copy(vm.restaurantInfo);
                vm.editRules = 1;
            }
            if (section === 'editContact') {
                vm.edit = angular.copy(vm.restaurantInfo);
                vm.editContact = 1;
            }
        }
        vm.submit = function (item, message, edit) {
            console.log(edit);
            vm.restaurantInfo = angular.copy(vm.edit);
            vm.edit = {};
              if (item === 'profileInfo') {
                vm.editInfo = 0;
            }

            if (item === 'profileContact') {
                vm.editContact = 0;
            }

            if (item === 'rules') {
                vm.editRules = 0;

            }
            restaurantsService.resturantInfo.setRestaurantInfo(vm.restaurantInfo).then(
                function (result) {
                    growlService.growl('تم التعديل بنجاح', 'inverse');
                });
        }

        vm.foodTypes = [];
        vm.resturantTypes = [];
        vm.countries = [];

        function foodType() {
            restaurantsService.general.foodTypes().then(function (result) {
                vm.foodTypes = result;
            });
        }

        function resturantTypes() {
            restaurantsService.general.resturantTypes().then(function (result) {
                vm.resturantTypes = result;
            });
        }
        function countries() {

            restaurantsService.general.countries().then(function (result) {
                vm.countries = result;
            });

        }

        activate();

        function activate() {
            restaurantsService.resturantInfo.getById($stateParams.resturantId).then(function (result) {
                console.log(result);
                vm.restaurantInfo = result;

            });
            countries();
            resturantTypes();
            foodType();
        }

    }
})();
