﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantUsers', restaurantUsers);

    restaurantUsers.$inject = ['common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ngDialog','$state'];

    function restaurantUsers(common, $stateParams, growlService, restaurantsService, askDialog, ngDialog,$state) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.addRoleUser = addRoleUser;
        vm.addNewUser = addNewUser;
        vm.users = [];
        activate();

        function activate() {
            getUsers();
        }
        function addNewUser(){
            $state.go('userManagment.addNewUser', {restaurantId:$stateParams.resturantId, backUrl:'restaurants.profile'});

        }
        function addRoleUser() {
            modalInstances();

        }
        function getUsers() {
            restaurantsService.resturantInfo.getById($stateParams.resturantId,true).then(function (result) {
                vm.users = result.managers;
            });
        }

        function modalInstances() {
            ngDialog.open({
                template: 'app/views/restaurants/restaurantProfile/users/dialogs/addUserToResturant.html',
                controller: 'addUserToResturant',
                controllerAs: 'vm',
                data: {
                    resturantId: $stateParams.resturantId,
                    success: activate
                },
                className: 'ngdialog-theme-default'
            });
        }
    }
})();
