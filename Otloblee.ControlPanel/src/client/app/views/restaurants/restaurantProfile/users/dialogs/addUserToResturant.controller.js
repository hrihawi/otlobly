﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('addUserToResturant', addUserToResturant);

    addUserToResturant.$inject = ['$scope','common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog','userManagmentService','$state'];

    function addUserToResturant($scope, common, $stateParams, growlService, restaurantsService, askDialog, userManagmentService, $state) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.submit = submit;
        activate();
        vm.restaurantRoles = userManagmentService.getResturantRoles();
        function activate() {

        }
        function submit(dialogInfo) {
            console.log(dialogInfo);
            restaurantsService.users.add($scope.ngDialogData.resturantId, dialogInfo.email, dialogInfo.role).then(function (result) {
                if (result) {
                    growlService.growl('تم الاضافة بنجاح', 'success');
                    $scope.ngDialogData.success();
                    $scope.closeThisDialog();

                }else
                    growlService.growl('خطأ بالبرامترات المرسلة يرجى التأكد من البريد الالكتروني', 'danger');

               
            });
        }

     
    }
})();
