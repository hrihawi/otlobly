﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantProfileController', restaurantProfileController);

    restaurantProfileController.$inject = ['$state', '$filter', 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ImageProcess'];

    function restaurantProfileController($state, $filter, common, $stateParams, growlService, restaurantsService, askDialog, ImageProcess) {
        /* jshint validthis:true */
        var vm = this;
        var file;
        var logger = common.logger;
        vm.dataUrl = 'src/client/img/logo.png';
        activate();
        vm.onFile = onFile;
        function onFile(event) {
            if (!event.target.files) {
                vm.allowCropping = false;
                vm.dataUrl = null;
                return;
            }
            var uploadedFile = event.target.files[0];
            initializeImageCropper(uploadedFile);
        }

        function initializeImageCropper(uploadedFile) {
            ImageProcess.encode((file = uploadedFile)).then(function (dataUrl) {
                vm.dataUrl = dataUrl;
                restaurantsService.resturantInfo.uploadLogo($stateParams.resturantId, vm.dataUrl).then(function (result) {
                    growlService.growl('تم تحميل الشعار بنجاح', 'success');
                });
            });

        }


        function activate() {
            getRestaurantInfo($stateParams.resturantId);
        }

        function getRestaurantInfo(resturantId) {
            restaurantsService.resturantInfo.getById($stateParams.resturantId).then(function (result) {
                if (!result) {
                    //$state.go();
                }
                vm.restaurantInfo = result;
                if (!!result.logo) {
                    vm.dataUrl = 'data:image/png;base64,' + result.logo;
                }
               
                console.log(vm.restaurantInfo);
                $state.go('restaurants.profile.information');

            });


        }

    }
})();
