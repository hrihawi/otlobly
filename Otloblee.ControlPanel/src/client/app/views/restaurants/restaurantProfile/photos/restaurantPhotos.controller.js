﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantPhotos', restaurantPhotos);

    restaurantPhotos.$inject = ['config','$scope', 'common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ImageProcess', '$rootScope'];

    function restaurantPhotos(config,$scope, common, $stateParams, growlService, restaurantsService, askDialog, ImageProcess, $rootScope) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.photos = null;
        activate();
        vm.prefix= config.basePath + 'api/ImageForRestaurant/get?id=';
        vm.addImage = addImage;
        vm.onFile = onFile;
        vm.dataUrl = null;
        var file;
        vm.showImages = false;
        vm.initPhotoViewer = initPhotoViewer;
        function activate() {
            getImages();

        }

        $scope.$on('vm.ngRepeatFinished', function (ngRepeatFinishedEvent) {
        });
        function initPhotoViewer() {
            $scope.$broadcast('initPhotoViewer');
        }
        function getImages() {
            restaurantsService.photos.get($stateParams.resturantId).then(function (results) {
                vm.photos = results;

            });
        }

        function addImage() {
            var imageForAdd = {
                restaurantId: $stateParams.resturantId,
                image: vm.dataUrl
            };
            restaurantsService.photos.add(imageForAdd).then(function (results) {
            getImages();
            });
        }

        function onFile(event) {
            if (!event.target.files) {
                vm.allowCropping = false;
                vm.dataUrl = null;
                return;
            }
            var uploadedFile = event.target.files[0];
            initializeImageCropper(uploadedFile);
        }

        function initializeImageCropper(uploadedFile) {
            ImageProcess.encode((file = uploadedFile)).then(function (dataUrl) {
                vm.dataUrl = dataUrl;
            });

        }
    }
})();
