﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('addMealController', addMealController);

    addMealController.$inject = ['$scope','common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ngDialog', 'userManagmentService', 'ImageProcess'];

    function addMealController($scope,common, $stateParams, growlService, restaurantsService, askDialog, ngDialog, userManagmentService, ImageProcess) {
        /* jshint validthis:true */
        var vm = this;
        var file, data;
        var logger = common.logger;
        vm.mealModel = {};
        vm.submit = submit;
        vm.submitName = 'ح';
        vm.menuId = $scope.ngDialogData.menuId;
        activate();
        function activate() {
            if ( $scope.ngDialogData.action) {
                var item = $scope.ngDialogData.item;
                vm.mealModel = item;
            }

        }
        vm.onFile = onFile;
        function onFile(event) {
            if (!event.target.files) {
                vm.allowCropping = false;
                vm.dataUrl = null;
                return;
            }
            var uploadedFile = event.target.files[0];
            initializeImageCropper(uploadedFile);
        }

        function initializeImageCropper(uploadedFile) {
            ImageProcess.encode((file = uploadedFile)).then(function (dataUrl) {
                vm.dataUrl = dataUrl;
             });
        
        }

        function submit(dialogInfo) {
            var meal = {
                nameArabic: dialogInfo.nameArabic,
                nameEnglish: dialogInfo.nameEnglish,
                descriptionArabic: dialogInfo.descriptionArabic,
                descriptionEnglish: dialogInfo.descriptionEnglish,
                price: dialogInfo.price,
                isActive: true,
                menuId: vm.menuId,
                image: vm.dataUrl
               
            };

            if (!$scope.ngDialogData.action) {
                restaurantsService.Menus.meals.add(meal).then(function (result) {
                    growlService.growl('تمت الاضافة بنجاح', 'success');
                    $scope.ngDialogData.success();
                    $scope.closeThisDialog()
                });
            } else {
                meal.id = vm.mealModel.id;
                restaurantsService.Menus.meals.update(meal).then(function (result) {
                    growlService.growl('تمت التعديل بنجاح', 'success');
                    $scope.ngDialogData.success();
                    $scope.closeThisDialog()
                });
            }

        }


    }
})();
