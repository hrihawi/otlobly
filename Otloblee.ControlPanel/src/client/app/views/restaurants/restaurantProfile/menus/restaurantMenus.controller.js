﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('restaurantMenus', restaurantMenus);

    restaurantMenus.$inject = ['common', '$stateParams', 'growlService', 'restaurantsService', 'askDialog', 'ngDialog'];

    function restaurantMenus(common, $stateParams, growlService, restaurantsService, askDialog, ngDialog) {
        /* jshint validthis:true */
        var vm = this;
        vm.resturantId = $stateParams.resturantId;
        var logger = common.logger;
        vm.edit = 'تعديل';
        activate();
        vm.menus = [];
        vm.addMenu = addMenu;
        vm.categories = [];
        vm.editMeal = editMeal;
        vm.addMeal = addMeal;
        vm.deleteMeal = deleteMeal;

        function editMeal(id, arabicName, englishName,mealItem) {
            ngDialog.open({
                template: 'app/views/restaurants/restaurantProfile/menus/dialogs/addMeal.html',
                controller: 'addMealController',
                controllerAs: 'vm',
                data: {
                    menuName: arabicName,
                    menuId: id,
                    success: getMenus,
                    action: 'update',
                    item: mealItem
                },
                className: 'ngdialog-theme-default'
            });
        }

        function addMeal(id,arabicName,englishName) {
            ngDialog.open({
                template: 'app/views/restaurants/restaurantProfile/menus/dialogs/addMeal.html',
                controller: 'addMealController',
                controllerAs: 'vm',
                data: {
                    menuName: arabicName,
                    menuId: id,
                    success: getMenus
                },
                className: 'ngdialog-theme-default'
            });
        }
        function getCategories() {
          return  restaurantsService.Menus.getCategories().then(function (result) {
                vm.categories = result;
            });
        }
        
        function getMenus() {
            return restaurantsService.Menus.get($stateParams.resturantId).then(function (result) {
                vm.menus = result;
            });
        }

        function addMenu(menu){
            restaurantsService.Menus.Add($stateParams.resturantId, {
                nameArabic: menu.nameArabic,
                nameEnglish: menu.nameEnglish,
                categoryId: menu.categoryId
            }).then(function (result) {
                getMenus();
                growlService.growl('Messages.SucesssAddMenu', 'success');
                vm.showAddMenu = false;
                vm.hideTimeLine = false;
            });
        }
        function deleteMeal(meal) {
            restaurantsService.Menus.meals.deleteMeal(meal).then(function (result) {
                if (result) {
                    growlService.growl('تم الحذف بنجاح', 'success');
                }
            });
        }
        function activate() {
            getCategories();
            getMenus();
        }

    }
})();
