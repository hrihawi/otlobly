﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'userManagment',
                config: {
                    url: '/userManagment',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/userManagment/userManagment.html',
                            controller: 'userManagmentController',
                            controllerAs: 'vm'
                        }
                    }
                }
            },
             {
                 state: 'userManagment.addNewUser',
                 config: {
                     url: '/userManagment/addNewUser',
                     params: { restaurantId: null,backUrl:null },
                     
                     data: {
                         access: access.User
                     },
                     views: {
                         'header': {
                             templateUrl: 'app/layout/header/header.html'
                         },
                         'footer': {
                             templateUrl: 'app/layout/footer/footer.html'
                         },
                         'content@': {
                             templateUrl: 'app/views/userManagment/addUser/addUser.html',
                             controller: 'addUserController',
                             controllerAs: 'vm'
                         }
                     }
                 }
             }

            
        ];
    }
})();
