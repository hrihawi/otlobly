﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('userManagmentController', userManagmentController);

    userManagmentController.$inject = ['$filter', 'common', '$stateParams', 'ngTableParams', 'userManagmentService', 'askDialog'];

    function userManagmentController($filter, common, $stateParams, ngTableParams, userManagmentService,askDialog) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.item = {};
        vm.data;
        vm.changeRole = changeRole;
        vm.changeStatus = changeStatus;
        activate();


        function changeStatus(userid, status) {
            askDialog.confirm(
                "Messages.ChangeRoleQ", "warning", "Messages.ChangeRole", function (success) {
                    success();
                }
                );
        }
        function changeRole(id, role) {
            askDialog.confirm(
                         'Messages.ChangeRoleQ', "warning", "Messages.ChangeRole", function (success) {
                             userManagmentService.changeRole(id, role).then(function () {
                                 success();
                             });
                         }
                          );
        }
        
        function activate() {
            vm.roles = userManagmentService.getRoles();
            getUsers();
           
        }

        function getUsers() {

            userManagmentService.getAllUsers()
                  .then(getAllUsersSuccess);
            function getAllUsersSuccess(result) {
                vm.data = result;

                //Binding  with Sorting
         /*       vm.tableSorting = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,           // count per page
                    sorting: {
                        name: 'asc'     // initial sorting
                    }
                }, {
                    total: vm.data.length, // length of data
                    getData: function ($defer, params) {
                        // use build-in angular filter
                        var orderedData = params.sorting() ? $filter('orderBy')(vm.data, params.orderBy()) : vm.data;

                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
*/

                       vm.tableSorting = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,
                    sorting: {
                        name: 'asc'     // initial sorting
                    }
                }, {
                        total: vm.data.length, // length of data
                        getData: function ($defer, params) {
                            // use build-in angular filter
                            var orderedData = params.filter() ? $filter('filter')(vm.data, params.filter()) : vm.data;

                            this.userName = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.fullName = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.email = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            this.mobileNumber = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

                            params.total(orderedData.length); // set total for recalc pagination
                            $defer.resolve(this.userName, this.fullName, this.email, this.mobileNumber);
                        }
                    })


            };

        }
    }
})();
