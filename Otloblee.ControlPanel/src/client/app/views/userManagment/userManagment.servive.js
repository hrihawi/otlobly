﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('userManagmentService', userManagmentService);

    userManagmentService.$inject = ['$http', '$location', 'common', 'exception'];
    /* @ngInject */
    function userManagmentService($http, $location, common, exception) {

        var logger = common.logger;

        var service = {
            getAllUsers: getAllUsers,
            getRoles: getRoles,
            getResturantRoles: getResturantRoles,
            changeRole: changeRole
        };

        return service;

        function getAllUsers() {
            return $http.get('api/account/search')
           .then(getAllUsersComplete)
           .catch(function (message) {
               exception.catcher('XHR Failed for Confirm Email')(message);
           });

            function getAllUsersComplete(data, status, headers, config) {
                console.log(data);
                return data.data.result;
            }
            //return [
            //  {
            //      "id": 'fb34546e-de7a-4d78-8912-6b2963c8084f',
            //      "userName": "safwat23",
            //      "email": "safwat.exe@hotmail.com",
            //      "mobileNumber": "(382)-122-5003",
            //      "country": "Syria",
            //      "createDate": "15/05/2016 02:50:00",
            //      "status": true,
            //      "type": "Restaurant Admin",
            //  },
            //  {
            //        "id": 'fb34546e-de7a-4d78-8912-6b2963c808aa',
            //      "userName": "hasoon",
            //      "email": "hasan.xmen@hotmail.com",
            //      "mobileNumber": "(382)-122-3455",
            //      "country": "Syria",
            //      "createDate": "16/05/2016 02:50:00",
            //      "status": true,
            //      "type": "Restaurant Admin",
            //  },
            //  {
            //      "id": 'fb34546e-de7a-4d78-8912-6b2963c808bb',
            //      "userName": "SafwatGhannoum",
            //      "email": "safwat.exe@hotmail.com",
            //      "mobileNumber": "(382)-122-5003",
            //      "country": "Syria",
            //      "createDate": "15/05/2016 02:50:00",
            //      "status": false,
            //      "type": "Restaurant Admin",
            //  },
            // {
            //     "id": 'fb34546e-de7a-4d78-8912-6b2963c80844',
            //     "userName": "hasoon",
            //     "email": "safwat.exe@hotmail.com",
            //     "mobileNumber": "(382)-122-5003",
            //     "country": "Syria",
            //     "createDate": "15/05/2016 02:50:00",
            //     "status": false,
            //     "type": "Restaurant Admin",
            // },
            //];

        }

        function changeRole(userId,roleName) {
            return $http.post('api/account/AssignRolesToUser', {
                userId: userId,
                role: roleName
            })
                .then(changeRoleComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for Change Role')(message);
                });

            function changeRoleComplete(data, status, headers, config) {
                console.log(data);
                return data.data.result;
            }

        }

        function getRoles() {
            return [
                {
                    name: 'User',
                    value: 'User'
                },
                {
                    name: 'Otloblee Admin',
                    value: 'WebsiteAdmin'
                },
                {
                    name: 'Otloblee Manager',
                    value: 'WebsiteManager'
                }, {
                    name: 'Restaurant Admin',
                    value: 'RestaurantAdmin'
                },
                {
                    name: 'Restaurant Manager',
                    value: 'RestaurantManager'
                }
                 ]
         
        }

        function getResturantRoles() {
            return [
                {
                    name: 'Restaurant Admin',
                    value: 'RestaurantAdmin'
                },
                {
                    name: 'Restaurant Manager',
                    value: 'RestaurantManager'
                }
            ]

        }

    }

})();
