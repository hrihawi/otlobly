﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('addUserController', addUserController);

    addUserController.$inject = ['$filter', 'common', '$stateParams', 'ngTableParams', 'loginService', 'restaurantsService', '$state', 'growlService'];

    function addUserController($filter, common, $stateParams, ngTableParams, loginService, restaurantsService, $state, growlService) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.restaurantId = null;
        vm.backUrl = null;
        vm.submit = submit;

        activate();


        function submit(user) {
            loginService.register(user)
                .then(function (result) {
                    if (result) {
                        if (vm.restaurantId) {
                            restaurantsService.users.add(vm.restaurantId, user.email, 'RestaurantAdmin').then(function (result) {
                            });
                        }

                        growlService.growl('تمت الاضافة بنجاح', 'success');
                        if (vm.backUrl) {
                            $state.go(vm.backUrl,{resturantId:vm.restaurantId});
                        } else
                            $state.go('userManagment');

                    }
                });
        }

        function activate() {
            if ($stateParams.restaurantId) {
                vm.restaurantId = $stateParams.restaurantId;
                vm.backUrl = $stateParams.backUrl;
            }

        }

    }
})();
