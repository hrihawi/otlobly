﻿
(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('loginController', loginController);

    loginController.$inject = ['common', '$location', 'auth', 'loginService', '$state', '$stateParams'];

    function loginController(common, $location, auth, loginService, $state, $stateParams) {
        /* jshint validthis:true */
        var vm = this;
        var logger = common.logger;
        vm.title = 'login';
        vm.content = [];
        vm.cridential = {};
        vm.getlogin = getlogin;
        vm.customValidation = false;
        vm.submitted = false;
        vm.submit = submit;
        vm.registerUser = registerUser;
        this.login = 1;
        this.register = 0;
        this.forgot = 0;
        vm.successfulAddPage = true;
        activate();
        //////////////////////////
        function activate() {
            if ($stateParams.code && $stateParams.userId) {
                vm.successfulAddPage = false;
                sendConfirmEmail($stateParams.code, $stateParams.userId);

            }
        }

        function registerUser(user) {
            loginService.register(user)
                .then(loginSuccess, loginField);
            function loginSuccess(userInfo) {
                var loginData = { userName: user.userName, password: user.password };
                return auth.login(loginData).then(function (result) {
                    $state.go('loginApp.confirm');
                });
            }
            function loginField() {
                logger.error("please try register again")
            }
        }
        function getlogin(login) {
            var loginData = { userName: login.userName, password: login.password };
            return auth.login(loginData)
                .then(loginSuccess, loginField);

            function loginSuccess(userInfo) {
                if (userInfo) {
                    console.log(auth.User);
                    if (userInfo.RestaurantId) {
                        $state.go('restaurants.profile', { resturantId: userInfo.RestaurantId });
                    } else
                        $location.path('/');
                } else
                    logger.error("Wrong username or password")
            }
            function loginField() {
                vm.customValidation = true;
            }
        }

        function submit(login) {
            console.log(login);
            // Set the 'submitted' flag to true
            vm.submitted = true;
            return getlogin(login);
        }

        function sendConfirmEmail(code, userid) {
            loginService.confirmEmail(code, userid)
                .then(confirmEmailSuccess);
            function confirmEmailSuccess(userInfo) {
                if (userInfo && userInfo.status == 'OK') {
                    $state.go('loginApp.emailConfirmed');
                }
            }
        }
    }
})();
