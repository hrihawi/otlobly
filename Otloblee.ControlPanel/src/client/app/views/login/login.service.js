﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('loginService', loginService);

    loginService.$inject = ['$http', '$location', 'common', 'exception'];
    /* @ngInject */
    function loginService($http, $location, common, exception) {
        var logger = common.logger;

        var service = {
            register: registerNewUser,
            confirmEmail: confirmEmail

        };

        return service;

        function confirmEmail(code,userId) {
            return $http.get('ConfirmEmail?code=' + code + '&userId=' + userId)
               .then(confirmEmailComplete)
               .catch(function (message) {
                   exception.catcher('XHR Failed for Confirm Email')(message);
               });

            function confirmEmailComplete(data, status, headers, config) {
                return data.data;
            }

        }

        function registerNewUser(user) {
            return $http.post('api/account/Add', {
                email: user.email,
                username: user.userName,
                firstName: user.firstName,
                lastName: user.lastName,
                phoneNumber: user.phoneNumber,
                password: user.password,
                confirmPassword: user.confirmPassword
            })
               .then(registerComplete)
               .catch(function (message) {
                   exception.catcher('XHR Failed for registering New User')(message);
               });

            function registerComplete(data, status, headers, config) {
                return data.data;
            }
        }



    }
})();


