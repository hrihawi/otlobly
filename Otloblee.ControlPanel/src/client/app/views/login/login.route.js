﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'loginApp',
                config: {
                    url: '/login',
                    views: {
                        'all': {
                            templateUrl: 'app/views/login/login.html',
                            controller: 'loginController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.public
                    }
                }
            },
             {
                 state: 'loginApp.confirm',
                 config: {
                     url: '/confirm?userId&code',
                     views: {
                         'all@': {
                             templateUrl: 'app/views/login/confirm.html',
                             controller: 'loginController',
                             controllerAs: 'vm'
                         }
                     },
                     data: {
                         access: access.public
                     }
                 }
             },
             {
                 state: 'loginApp.emailConfirmed',
                 config: {
                     url: '/emailConfirmed',
                     views: {
                         'all@': {
                             templateUrl: 'app/views/login/emailConfirmed.html'
                         }
                     },
                     data: {
                         access: access.public
                     }
                 }
             }
         
        ];
    }
})();
