﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper','access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'dashboard',
                config: {
                    url: '/',
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/dashboard/dashboard.html',
                            controller: 'dashboardController',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        access: access.AnyAdmin
                    }
                }
            }
        ];
    }
})();
