﻿(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$timeout', '$state', '$scope', 'growlService', 'auth'];
    /* @ngInject */
    function dashboardController($timeout, $state, $scope, growlService, auth) {
        var vm = this;

        activate();


        function activate() {
            if (auth.user.restaurantId) {
                $state.go('restaurants.profile', { resturantId: auth.user.restaurantId });
            }
            growlService.growl('اهلاً وسهلاً بك, لوحة تحكم اطلبلي!', 'inverse')

        }


    }
})();
