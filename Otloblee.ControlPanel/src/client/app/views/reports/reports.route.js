(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'reports',
                config: {
                    url: '/reports',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/reports/reports.html',
                            controller: 'ordersController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }, {
                state: 'reports.allRestaurantsReport',
                config: {
                    url: '/allRestaurantsReport',
                    data: {
                        access: access.User
                    },
                    views: {
                       
                        'content@': {
                            templateUrl: 'app/views/reports/allRestaurantsReport/allRestaurantsReport.html',
                            controller: 'allRestaurantsReportController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
            , {
                state: 'reports.restaurantReport',
                config: {
                    url: '/restaurantReport',
                    data: {
                        access: access.User
                    },
                    views: {
                       
                        'content@': {
                            templateUrl: 'app/views/reports/restaurantReport/restaurantReport.html',
                            controller: 'restaurantReportController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();
