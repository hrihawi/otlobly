(function() {
    'use strict';

    angular.module('app.views', ['app.core', 'app.widgets',  'ngGeolocation', 'ngDialog', 'flow']);
})();
