(function() {
'use strict';

    angular
        .module('app.views')
        .controller('ordersController', ordersController);

    ordersController.$inject = ['ordersService','ngTableParams','$filter'];
    function ordersController(ordersService,ngTableParams,$filter) {
        var vm = this;
        vm.orders = null;

        activate();

        ////////////////

        function activate() {

            getOrders();
        }

        function getOrders(){

               ordersService.getAllOrders()
                  .then(getOrdersSuccess);
            function getOrdersSuccess(result) {
                vm.data = result;

                //Binding  with Sorting
                vm.tableSorting = new ngTableParams({
                    page: 1,            // show first page
                    count: 10,           // count per page
                    sorting: {
                        name: 'asc'     // initial sorting
                    }
                }, {
                    total: vm.data.length, // length of data
                    getData: function ($defer, params) {
                        // use build-in angular filter
                        var orderedData = params.sorting() ? $filter('orderBy')(vm.data, params.orderBy()) : vm.data;

                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
            };
        }
    }
})();