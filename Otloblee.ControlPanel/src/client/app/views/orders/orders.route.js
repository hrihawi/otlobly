(function () {
    'use strict';

    angular
        .module('app.views')
        .run(appRun);

    appRun.$inject = ['routerHelper', 'access'];
    /* @ngInject */
    function appRun(routerHelper, access) {
        var otherwise = '/';
        routerHelper.configureStates(getStates(access), otherwise);
    }

    function getStates(access) {
        return [
            {
                state: 'orders',
                config: {
                    url: '/orders',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/orders/orders.html',
                            controller: 'ordersController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }, {
                state: 'orders.newOrders',
                config: {
                    url: '/newOrders',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/orders/newOrders/newOrders.html',
                            controller: 'newOrdersController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
            , {
                state: 'orders.orderDetails',
                config: {
                    url: '/orderDetails?orderId',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/orders/orderDetails/orderDetails.html',
                            controller: 'orderDetailsController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }, {
                state: 'orders.invoice',
                config: {
                    url: '/invoice?orderId',
                    data: {
                        access: access.User
                    },
                    views: {
                        'header': {
                            templateUrl: 'app/layout/header/header.html'
                        },
                        'footer': {
                            templateUrl: 'app/layout/footer/footer.html'
                        },
                        'content@': {
                            templateUrl: 'app/views/orders/invoice/invoice.html',
                            controller: 'invoiceController',
                            controllerAs: 'vm'
                        }
                    }
                }
            }


        ];
    }
})();
