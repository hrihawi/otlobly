(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('orderDetailsController', orderDetailsController);

    orderDetailsController.$inject = ['ordersService', '$stateParams','growlService'];
    function orderDetailsController(ordersService, $stateParams,growlService) {
        var vm = this;
        vm.items = null;
        vm.order = null;
        vm.changeState = changeState;
        activate();

        ////////////////

        function activate() {
            getDetails();
        }

        function changeState(state) {
            ordersService.changeState($stateParams.orderId,state).then(function (result) {
                              growlService.growl('تم تعديل حالة الطلب', 'success');
                               getDetails();
            });

        }

        function getDetails() {
            ordersService.getOrderDetails($stateParams.orderId).then(function (result) {
                vm.order = result;
                vm.items = result.orderDetails;
                console.log(result);
            });
        }
    }
})();