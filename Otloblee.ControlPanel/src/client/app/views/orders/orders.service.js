(function () {
    'use strict';

    angular
        .module('app.views')
        .factory('ordersService', ordersService);

    ordersService.$inject = ['$http', '$location', 'common', 'exception'];
    /* @ngInject */
    function ordersService($http, $location, common, exception) {

        var logger = common.logger;

        var service = {
            getAllOrders: getAllOrders,
            getNewOrders: getNewOrders,
            getOrderDetails: getOrderDetails,
            changeState: changeState
        };

        return service;

        function getAllOrders() {
            return $http.get('api/order/search')
                .then(getAllOrdersComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for getAllOrders')(message);
                });

            function getAllOrdersComplete(data, status, headers, config) {
                return data.data.result;
            }
        }
        function getOrderDetails(id) {
            return $http.get('api/order/get?id='+id)
                .then(getOrderDetailsComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for get Order Details ')(message);
                });

            function getOrderDetailsComplete(data, status, headers, config) {
                return data.data.result;
            }
        }
        function getNewOrders(userId, roleName) {
            return $http.get('api/order/search?SpecificPeriodValue=3')
                .then(getAllOrdersComplete)
                .catch(function (message) {
                    exception.catcher('XHR Failed for getAllOrders')(message);
                });

            function getAllOrdersComplete(data, status, headers, config) {
                return data.data.result;
            }
        }

        function changeState(orderId,stateName) {
          return $http.post('api/order/UpdateStatus', {
                id: orderId,
                State: stateName
            })
         .then(changeStateComplete)
         .catch(function (message) {
             exception.catcher('XHR Failed for Change State')(message);
         });

            function changeStateComplete(data, status, headers, config) {
                console.log(data);
                return data.data.result;
            }  
                }

        function getResturantRoles() {
            return [
                {
                    name: 'Restaurant Admin',
                    value: 'RestaurantAdmin'
                },
                {
                    name: 'Restaurant Manager',
                    value: 'RestaurantManager'
                }
            ]

        }

    }

})();
