(function () {
    'use strict';

    angular
        .module('app.views')
        .controller('invoiceController', invoiceController);

    invoiceController.$inject = ['ordersService','$stateParams','restaurantsService'];
    function invoiceController(ordersService,$stateParams,restaurantsService) {
        var vm = this;
        vm.orderInfo = {};
        vm.restaurantInfo = {};
        vm.items = [];
        activate();

        ////////////////

        function activate() {
            getOrderInfo();
        }

        function getOrderInfo() {
            ordersService.getOrderDetails($stateParams.orderId).then(function (result) {
                vm.orderInfo = result;
                vm.items = result.orderDetails;
                 restaurantsService. resturantInfo.getById(vm.orderInfo.restaurantId).then(function (result) {
                vm.restaurantInfo = result;
            });
                console.log(result);
            });
        }
         
    }
})();