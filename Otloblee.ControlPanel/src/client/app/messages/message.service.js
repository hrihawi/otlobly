﻿(function () {
    'use strict';

    angular
        .module('app.messages')
        .service('messageService', messageService);

    messageService.$inject = ['exception', '$resource'];
    /* @ngInject */
    function messageService(exception, $resource) {

        this.getMessage = function (img, user, text) {
            var gmList = $resource("data/messages-notifications.json");

            return gmList.get({
                img: img,
                user: user,
                text: text
            });
        }


    }



})();

