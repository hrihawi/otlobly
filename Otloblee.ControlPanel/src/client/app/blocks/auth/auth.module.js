﻿(function (exports) {

    var config = {

      
        roles: [
            'public',
            'User',
            'WebsiteAdmin',
            'WebsiteManager',
            'RestaurantAdmin',
            'RestaurantManager',
            'AnyAdmin'
        ],

     
        accessLevels: {
            'public': '*',
            'anon': ['public'],
            'User': ['User', 'WebsiteAdmin', 'RestaurantAdmin', 'WebsiteManager', 'RestaurantManager'],
            'WebsiteAdmin': ['WebsiteAdmin'],
            'WebsiteManager': ['WebsiteAdmin', 'WebsiteManager'],
            'RestaurantAdmin': ['RestaurantAdmin', 'WebsiteManager', 'WebsiteAdmin'],
            'RestaurantManager': ['RestaurantAdmin', 'RestaurantManager', 'WebsiteAdmin', 'WebsiteManager'],
            'AnyAdmin': ['RestaurantAdmin', 'RestaurantManager', 'WebsiteAdmin', 'WebsiteManager']
        }

    }

    exports.userRoles = buildRoles(config.roles);
    exports.accessLevels = buildAccessLevels(config.accessLevels, exports.userRoles);

    
    function buildRoles(roles) {

        var bitMask = '01';
        var userRoles = {};

        for (var role in roles) {
            var intCode = parseInt(bitMask, 2);
            userRoles[roles[role]] = {
                bitMask: intCode,
                title: roles[role]
            };
            bitMask = (intCode << 1).toString(2)
        }

        return userRoles;
    }

    /*
    This method builds access level bit masks based on the accessLevelDeclaration parameter which must
    contain an array for each access level containing the allowed user roles.
     */
    function buildAccessLevels(accessLevelDeclarations, userRoles) {

        var accessLevels = {};
        for (var level in accessLevelDeclarations) {

            if (typeof accessLevelDeclarations[level] == 'string') {
                if (accessLevelDeclarations[level] == '*') {

                    var resultBitMask = '';

                    for (var role in userRoles) {
                        resultBitMask += '1'
                    }
                    //accessLevels[level] = parseInt(resultBitMask, 2);
                    accessLevels[level] = {
                        bitMask: parseInt(resultBitMask, 2)
                    };
                }
                else {
                    console.log('Access Control Error: Could not parse "' + accessLevelDeclarations[level] + '" as access definition for level "' + level + '"');

                }
            }
            else {

                var resultBitMaskInt = 0;
                for (var role_ in accessLevelDeclarations[level]) {
                    if (userRoles.hasOwnProperty(accessLevelDeclarations[level][role_])) {
                        resultBitMaskInt = resultBitMaskInt | userRoles[accessLevelDeclarations[level][role_]].bitMask
                    }
                    else {
                        console.log('Access Control Error: Could not find role "' + accessLevelDeclarations[level][role_] + '" in registered roles while building access for "' + level + '"')
                    }
                }
                accessLevels[level] = {
                    bitMask: resultBitMaskInt
                };
            }
        }

        return accessLevels;
    }

})(typeof exports === 'undefined' ? this['routingConfig'] = {} : exports);

(function () {
    'use strict';

    angular.module('blocks.auth', []);



    angular
        .module('blocks.auth')
        .constant('access', routingConfig.accessLevels);
})();
