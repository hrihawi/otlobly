﻿(function () {
    'use strict';

    angular
        .module('blocks.auth')
        .directive('allowed', allowed);

    allowed.$inject = ['common', 'auth'];

    function allowed(common, auth) {
        var directive = {
            restrict: "A",
            link: linker
        };


        function linker(scope, element, attrs) {
            var roles = attrs.allowed.split(',');
            var isAllow = false;
            for (var index = 0; index < roles.length; index++) {
                if (auth.allowed(roles[index])) {
                    isAllow = true;
                    break;
                }

            }
            if (!isAllow) {
                element.remove();
            }
        }

        return directive;


    }

})();