angular
    .module('app.widgets')

    // =========================================================================
    // MEDIA ELEMENT
    // =========================================================================

    .directive('mediaElement', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.mediaelementplayer();
            }
        }

    })


    // =========================================================================
    // LIGHTBOX
    // =========================================================================
    .directive('lightbox', lightboxDirective);

lightboxDirective.$inject = [];
function lightboxDirective() {
    return {
        restrict: 'C',
        link: function (scope, element) {
            scope.$on('initPhotoViewer', function (event, args) {
                element.lightGallery({
                    enableTouch: true
                });
            });
        }
    }

}