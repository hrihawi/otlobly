(function () {
    'use strict';

    angular
        .module('app.core', ['pascalprecht.translate',
            'ngAnimate', 'ngSanitize',
            'blocks.exception', 'blocks.logger', 'blocks.router', 'blocks.auth','ngCookies',
            'ui.router', 'ngplus','ngResource','ui.bootstrap','angular-loading-bar', 'oc.lazyLoad', 'nouislider','ngTable', 'uiGmapgoogle-maps'
        ]);
})();

   