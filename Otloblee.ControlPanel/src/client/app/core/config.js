﻿(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[app Error] ',
        appTitle: 'Otloblee',
        basePath: 'http://localhost/OtlobliApi/'

    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider'];
    /* @ngInject */
    function configure($logProvider, routerHelperProvider, exceptionHandlerProvider) {

        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }

        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({ docTitle: + ': ' });
    }

    var translations = {
        DashBoard: "القائمة الرئيسية",
        OTLOBLEE: 'اطلبلي',
        Slider: {
            ViewProfile: 'عرض الملف الشخصي',
            Settings: 'الاعدادات',
            Logout: 'تسجيل خروج',
            DashBoard: 'لوحة التحكم',
            WebsiteInformation: 'ادارة موقع اطلبلي',
            UserManagement: 'ادارة المستخدمين',
            RestaurantsManagement: 'ادارة المطاعم',
            Reports: 'التقارير',
            ViewAllUsers: 'عرض المستخدمين',
            ViewAllRestaurants: 'عرض المطاعم',
            ViewResturantIAdded: 'عرض المطاعم التي اضفتها',
            AddUser: 'اضافة مستخدم جديد',
            Orders: 'الطلبيات',
            NewOrders: 'الطلبيات الجديدة',
            AllOrders: 'جميع الطلبيات'

        },
        Messages: {
            NotAuthrized: 'عذرا..لا يوجد لديك صلاحية لدخول هذه الصفحة',
            ChangeRoleQ: 'هل بالتأكيد تريد تغيير السماحيات للمستخدم الذي اخترته؟',
            ChangeRole: 'تغيير سماحيات',
            sucessfulMessage: 'تم تنفيذ الامر بنجاح',
            Done: 'تم!',
            UnderConstruction: 'هذه الصفحة قيد التطوير',
            Sorry: 'عذراً!',
            SucesssAddMenu: 'تم اضافة القائمة بنجاح',
            ChangeActivationQ: 'هل بالتأكيد تريد تغيير التفعيل للمطعم الذي اخترته؟',
            ChangeActivation: 'تغيير حالة التفعيل',
            DeleteResturantQ: 'هل بالتأكيد تريد حذف المطعم الذي اخترته؟',
            DeleteResturant: 'حذف المطعم'

        },
        Login: {
            UserName: 'اسم المستخدم',
            FirstName: 'الاسم الاول',
            LastName: 'الكنية',
            Email: 'البريد الالكتروني',
            Password: 'كلمة المرور',
            ConfirmPassword: 'تأكيد كلمة المرور',
            Login: 'تسجيل دخول',
            Register: 'مستخدم جديد',
            ForgetPassword: 'نسيت كلمة المرور',
            PhoneNumber: 'رقم الجوال'

        },
        Pages: {
            Admin: {
                Restaurants: {
                    RestaurantMembers: 'المطاعم المشتركة',
                    RestaurantMemberDescripe: 'هذه الصفحة تحوي جميع المطاعم المشتركة',
                    Name: 'الاسم',
                    Email: 'الايميل',
                    CreatedByUser: 'انشئ بواسطة',
                    Country: 'الدولة',
                    FeesPaymentType: 'نوعية الاشتراك',
                    IsAvailable: 'الحالة',
                    IsSuspended: 'هل هو متوقف',
                    AddNewMeal: 'اضافة وجبة جديدة',
                    AddNewRestaurant: 'اضافة مطعم جديد'
                },
                UserManagement: {
                    UserManagement: 'إدارة المستخدمين',
                    UserManagementMessage: 'ادارة جميع مستخدمين الموقع من المطاعم والمسؤولين والمستخدمين العاديين',
                    ViewWebsiteUsers: 'عرض المستخدمين',
                    ViewWebsiteUsersMessage: 'عرض وتحرير جميع مستخدمين الموقع',
                    UserName: 'اسم المستخدم',
                    FullName: 'الاسم الكامل',
                    Email: 'البريد الاكتروني',
                    MobileNumber: 'رقم الجوال',
                    CreatedDate: 'تاريخ الانشاء',
                    Status: 'الحالة',
                    Role: 'السماحيات',
                    PaymentType: 'طريثة الدفع',
                    FullAddress: 'العنوان الكامل',
                },
                Coupons: {
                    Code: 'الكود',
                    Ammount: 'قيمة الخصم',
                    Count: 'عدد الكوبونات',
                    Taken: 'عدد الكوبونات المأخوذة',
                    ExpireDate: 'تاريخ انتهاء الصلاحية'
                }
            },
            Orders: {
                Orders: 'طلبيات المطعم',
                NewOrders: 'الطلبيات الجديدة',
                AllOrders:'جميع الطلبات'
            }

        },
        Ok: 'موافق',
        Cancel: 'الغاء',
        UploadLogo: 'تحميل الشغار',
        RestaurantName: 'اسم المطعم بالانجليزية',
        RestaurantNameArabic: 'اسم المعطم بالعربية',
        Country: 'الدولة',
        RestaurantType: 'نوع المطعم',
        FoodType: 'نوع الطعام',
        Address: 'العنوان',
        AddressEnglish: 'العنوان بالانجليزية',
        AddressArabic: 'العنوان بالعربية',
        DescriptionEnglish: 'الوصف بالانجليزية',
        DescriptionArabic: 'الوصف بالعربية',
        StartWorkTime: 'وقت بدأ العمل',
        EndWorkTime: 'وقت نهاية العمل',
        MinimumAmountforFreeDelivery: 'اقل قيمة فاتورة للتوصيل المجاني',
        DeliveryFee: 'سعر التوصيل',
        Currency: 'العملة',
        MobilePhone: 'رقم الموبايل',
        EmailAddress: 'البريد الالكتروني',
        RestaurantInfo: 'معلومات المطعم',
        ContactInformation: 'معلومات الاتصال',
        RestaurantRules: 'قوانين المطعم',
        Save: 'حفظ',
        Edit: 'تعديل',
        Delete: 'حذف',
        RestaurantMap: 'خريطة المطعم',
        ChangeRestaurantMapDescription: 'يمكنك سحب المؤشر ووضعه فوق مكان مطعمك مباشرة',
        ChangeRestaurantMapEvent: 'لاحظنا انك قمت بتغيير مكان المؤشر لمطعمك, يرجى ضغط حفظ لحفظ الموقع الجديد',
        ArabicName: 'الاسم بالعربية',
        EnglishName: 'الاسم بالانجليزية',
        Description: 'الوصف',
        Price: 'السعر',
        DiscountPrice: 'سعر الخصم',
        DiscountStartDate: 'تاريخ بداية الخصم',
        DiscountEndDate: 'تاريخ نهاية الخصم',
        AddMenu: 'اضافة قائمة جديدة',
        Add: 'اضافة',
        User: 'مستخدم عادي',
        WebsiteAdmin: 'مدير العام اطلبلي',
        WebsiteManager: 'مسؤول اطلبلي',
        RestaurantAdmin: 'مدير العام للمطعم',
        RestaurantManager: 'مسؤول مطعم',
        FeesPaymentType: 'نوعية الاشتراك',
        Fees: 'مقدار الدفع',
        SubscribingType: 'اعدادات الاشتراك',
        Website: 'الموقع الالكتروني',
        PhoneNumber1: 'رقم الهاتف ',
        PhoneNumber2: 'رقم الهاتف 2',
        AddNewUser: 'اضافة مستخدم جديد',
        AddExistUser: 'اضافة مستخدم مسجل مسبقاً',
        Email: 'البريد الالكتروني',
        State: 'الحالة',
        ReferenceNumber: 'الرقم المرجعي',
        PaymentType: 'طريقة الدفع',
        DeliveryFees: 'تكلفة التوصيل',
        CreateTime: 'وقت الارسال',
        FullName: 'الاسم الكامل',
        UserNotes: 'ملاحظات الزبون'
    };

    core.config(['$translateProvider', function ($translateProvider) {
        // add translation table
        $translateProvider
            .translations('ar', translations)
            .preferredLanguage('ar');
    }]);

    core.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
           key: 'AIzaSyBWr1DQiqfQoBMjBaUhAEm4OTTqzsujG4w',
        v: '3.20' //defaults to latest 3.X anyhow
       
    });
})

})();
