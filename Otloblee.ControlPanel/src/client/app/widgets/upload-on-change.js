﻿(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('drOnChange', drOnChange);

    drOnChange.$inject = [];

    function drOnChange() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeFunc = scope.$eval(attrs.drOnChange);
                element.bind('change', onChangeFunc);
            }
        };
    }
})();