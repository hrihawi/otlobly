(function() {
    'use strict';

    angular.module('app.widgets', []);

    angular.module('app.widgets').config(function ($sceDelegateProvider) {
        $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
    });

})();
