﻿
// =========================================================================
// Malihu Scroll - Custom Scroll bars
// =========================================================================

(function () {
    'use strict';

    angular
        .module('app.widgets')
        .service('scrollService', scrollService);

    scrollService.$inject = ['exception'];
    /* @ngInject */
    function scrollService(exception) {
        var ss = {};
        ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
            $(selector).mCustomScrollbar({
                theme: theme,
                scrollInertia: 100,
                axis:'yx',
                mouseWheel: {
                    enable: true,
                    axis: mousewheelaxis,
                    preventDefault: true
                }
            });
        }
        
        return ss;
    }

    

})();

