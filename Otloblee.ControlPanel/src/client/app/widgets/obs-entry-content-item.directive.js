﻿(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('obsEntryContentItem', obsEntryContentItem);

    obsEntryContentItem.$inject = ['common'];

    function obsEntryContentItem(common) {
        // Usage:
        //     <obs-entry-content-item></obs-entry-content-item>
        // Creates:
        // 

         var $compile = common.$compile;
          var logger = common.logger;
        var imageTemplate = '<div class="entry-photo"><h2>&nbsp;</h2><div class="entry-img"><span><a href="{{rootDirectory}}{{content.data}}"><img ng-src="{{rootDirectory}}{{content.data}}" alt="entry photo"></a></span></div><div class="entry-text"><div class="entry-title">{{content.title}}</div><div class="entry-copy">{{content.description}}</div></div></div>';
        var videoTemplate = '<div class="entry-video"><h2>&nbsp;</h2><div class="entry-vid"><iframe ng-src="{{content.data}}" width="280" height="200" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div><div class="entry-text"><div class="entry-title">{{content.title}}</div><div class="entry-copy">{{content.description}}</div></div></div>';
        var noteTemplate = '<div class="entry-note"><h2>&nbsp;</h2><div class="entry-text"><div class="entry-title">{{content.title}}</div><div class="entry-copy">{{content.data}}</div></div></div>';

        var directive = {
            restrict: "E",
            link: linker,
            scope: {
                content: '='
            }
        };


        function linker(scope, element, attrs) {
            scope.rootDirectory = 'images/';

            element.html(getTemplate(scope.content.contentType)).show();
            $compile(element.contents())(scope);
        }


        function getTemplate(contentType) {
            var template = '';
            switch (contentType) {
                case 'image':
                    template = imageTemplate;
                    break;
                case 'video':
                    template = videoTemplate;
                    break;
                case 'notes':
                    template = noteTemplate;
                    break;
            }

            return template;
        }

        return directive;


    }

})();