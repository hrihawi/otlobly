﻿(function () {
    'use strict';

    angular
        .module('app.widgets')
        .service('askDialog', askDialog);

    askDialog.$inject = ['exception', '$translate', '$filter'];
    /* @ngInject */
    function askDialog(exception, $translate, $filter) {
        var gs = {};
        gs.confirm = function (messageTag, type, titleTag, functionAfterConfirm) {
            var message, title, confirmButtonText, cancelButtonText, sucessfulMessage;
            console.log(messageTag);

            swal({
                title: $filter('translate')(titleTag),
                text: $filter('translate')(messageTag),
                type: type,
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: $filter('translate')('Ok'),
                cancelButtonText: $filter('translate')('Cancel'),
                closeOnConfirm: false
            }, function () {
                functionAfterConfirm(function () {
                    swal({
                        title: $filter('translate')('Messages.Done'),
                        text: $filter('translate')('Messages.sucessfulMessage'),
                        type: "success",
                        confirmButtonText: $filter('translate')('Ok')
                    });
                })
            });
        }
      

        return gs;


    }

    

})();

