﻿module.exports = function() {
    var client = './src/client/';
    var server = './src/server/';
    var clientApp = client + 'app/';
    var config = {
        alljs: ['./src/**/*.js'],
        index: client + 'index.html',
        js: [
            clientApp + '**/*.module.js',
            clientApp + '**/*.js'
        ],
        client: client,
        server: server,
        getWiredepDefaultOptions: getWiredepDefaultOptions,
        bower: {
            json: require('./bower.json'),
            directory: './bower_components',
            ignorePath: '../..'

        },
        defaultPort: 7206,
        nodeServer: './src/server/app.js'
};

  function getWiredepDefaultOptions() {
        var options = {
            bowerJson: config.bower.json,
            directiory: config.bower.directory,
            ignorePath : config.bower.ignorePath

        };
        return options;
    }

    return config;
};