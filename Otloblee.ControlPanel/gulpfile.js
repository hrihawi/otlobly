﻿var gulp = require('gulp');
var config = require('./gulp.config.js')();
var browserSync = require('browser-sync');
var port = process.env.PORT || config.defaultPort;
var $ = require('gulp-load-plugins')({ lazy: true });
var nodemon = require('nodemon');

gulp.task('vet', function () {
    return gulp
        .src(config.alljs)
        .pipe($.print());


});

gulp.task('wiredep', function () {
    var option = config.getWiredepDefaultOptions();
    var wiredep = require('wiredep').stream;
    return gulp
        .src(config.index)
        .pipe(wiredep(option))
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.client));


});

gulp.task('serve-dev', ['wiredep'], function () {
    var isDev = true;
    var nodeOptions = {
        script: config.nodeServer,
        delayTime: 1,
        env: {
            'PORT': port,
            'NOE_ENV': isDev ? 'dev' : 'build'
        },
        watch: [config.server]
    };
    return nodemon(nodeOptions)
        .on('start', function () {
            startBrowserSync();
        });

});

function startBrowserSync() {
    if (browserSync.active) {
        return;
    }
    var options = {
        proxy: 'localhost:' + port,
        port: 3006,
        files: [config.client + '**/*.*'],
        ghostMode: {
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        injectChanges: true,
        logFileChanges: true,
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000

    }
    browserSync(options);
}

//gulp.task('inject',['wiredep','styles'], function () {

//    return gulp
//        .src(config.index)
//        .pipe($.inject(gulp.src(config.css)))
//        .pipe(gulp.dest(config.client));


//});