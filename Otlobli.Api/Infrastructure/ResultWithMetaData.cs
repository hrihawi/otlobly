﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Otlobli.Infrastructure
{
    public class ResultWithMetaData<T> : IHttpActionResult
    {
        public HttpStatusCode Status { get; set; }
        public T Result { get; set; }
        public HttpRequestMessage Request { get; set; }
        public IList<MetaData> MetaData { get; set; }
        public IList<Error> Errors { get; private set; }

        public ResultWithMetaData(HttpRequestMessage request, HttpStatusCode status, T result, IList<MetaData> metaData, IList<Error> errors)
        {
            Result = result;
            Request = request;
            MetaData = metaData;
            Status = status;
            Errors = errors;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var apiResult = new ApiResult<T>(Status, Result, MetaData, Errors);
            var httpResponseMessage = Request.CreateResponse(Status, apiResult);
            return Task.FromResult(httpResponseMessage);
        }
    }

    public class ApiResult<T>
    {
        public ApiResult(HttpStatusCode status, T result, IList<MetaData> metaData, IList<Error> errors)
        {
            Result = result;
            MetaData = metaData;
            Status = status;
            Errors = errors;
        }
        public HttpStatusCode Status { get; private set; }
        public T Result { get; private set; }
        public IList<MetaData> MetaData { get; private set; }
        public IList<Error> Errors { get; private set; }

    }

    public class MetaData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class Error
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}