﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Otlobli.Authentication;
using Otlobli.DataAccess;

namespace Otlobli.Infrastructure
{
    public class BaseApiController : ApiController
    {
        private readonly UserManager appUserManager = null;
        protected UserManager AppUserManager
        {
            get { return appUserManager ?? UserManager.AppUserManager(Request.GetOwinContext()); }
        }


        private readonly RoleManager appRoleManager = null;
        protected RoleManager AppRoleManager
        {
            get { return appRoleManager ?? RoleManager.AppRoleManager(Request.GetOwinContext()); }
        }


        private IIdentity currentUser;
        protected IIdentity CurrentUser
        {
            get { return currentUser ?? (currentUser = RequestContext.Principal.Identity); }
        }


        #region Return Extensions

        protected IHttpActionResult ReturnValidatedResult<T>(T result, IList<MetaData> metaData, IList<Error> errors, HttpStatusCode? httpStatusCode)
        {
            var modelState = result as ModelStateDictionary;
            if (modelState != null)
            {
                return new ResultWithMetaData<T>(Request, HttpStatusCode.BadRequest, default(T), metaData,
                    modelState.Select(item => new Error()
                    {
                        Message = item.Key,
                        StackTrace = (item.Value.Errors.FirstOrDefault() ?? new ModelError("unknown")).ErrorMessage.ToString()
                    }).ToList());
            }

            // general state
            var method = Request.Method.Method;
            HttpStatusCode status;
            if (httpStatusCode.HasValue)
                status = httpStatusCode.Value;
            else if (result == null && errors != null && errors.Any())
                status = HttpStatusCode.InternalServerError;
            else if (result == null && method == HttpMethod.Get.Method)
                status = HttpStatusCode.NotFound;
            else if (method == HttpMethod.Post.Method)
                status = HttpStatusCode.Created;
            else if (method == HttpMethod.Delete.Method)
                status = HttpStatusCode.NoContent;
            else
                status = HttpStatusCode.OK;

            return new ResultWithMetaData<T>(Request, status, result, metaData, errors);
        }

        protected IHttpActionResult ReturnValidatedResult<T>(PagedListResult<T> result)
        {
            return ReturnValidatedResult(result.Entities, new List<MetaData>()
            {
                new MetaData()
                {
                    Key = "Count",
                    Value = result.Count.ToString(),
                },
                new MetaData()
                {
                    Key = "HasNext",
                    Value = result.HasNext.ToString(),
                },
                new MetaData()
                {
                    Key = "HasPrevious",
                    Value = result.HasPrevious.ToString(),
                },
            }, null, null);
        }

        protected IHttpActionResult ReturnValidatedResult<T>(T result)
        {
            return ReturnValidatedResult(result, null, null, null);
        }

        protected IHttpActionResult ReturnValidatedResult(HttpStatusCode httpStatusCode, string metaDataKey, string metaDataValue)
        {
            return ReturnValidatedResult(default(IHttpActionResult), new List<MetaData>()
            {
                new MetaData()
                {
                    Key = metaDataKey,
                    Value = metaDataValue
                }
            }, null, httpStatusCode);
        }

        protected IHttpActionResult ReturnValidatedResult(HttpStatusCode httpStatusCode, List<MetaData> metaData)
        {
            return ReturnValidatedResult(default(IHttpActionResult), metaData, null, httpStatusCode);
        }

        protected IHttpActionResult ReturnValidatedResult(HttpStatusCode httpStatusCode)
        {
            return ReturnValidatedResult(default(IHttpActionResult), null, null, httpStatusCode);
        }

        protected IHttpActionResult ReturnValidatedResult<T>(T result, HttpStatusCode httpStatusCode)
        {
            return ReturnValidatedResult(result, null, null, httpStatusCode);
        }

        protected IHttpActionResult ReturnValidatedResult(IList<Error> errors)
        {
            return ReturnValidatedResult(default(IHttpActionResult), null, errors, null);
        }

        protected IHttpActionResult ReturnValidatedResult(Error error)
        {
            return ReturnValidatedResult(default(IHttpActionResult), null, new List<Error>() { error }, null);
        }

        protected IHttpActionResult ReturnValidatedResult(Error error, HttpStatusCode httpStatusCode)
        {
            return ReturnValidatedResult(default(IHttpActionResult), null, new List<Error>() { error }, httpStatusCode);
        }

        #endregion

    }
}