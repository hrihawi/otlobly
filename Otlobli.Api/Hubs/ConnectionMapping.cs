﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otlobli.Hubs
{
    public class ConnectionMapping<T>
    {
        private readonly Dictionary<T, HashSet<string>> allConnections = new Dictionary<T, HashSet<string>>();

        public int Count
        {
            get
            {
                return allConnections.Count;
            }
        }

        public void Add(T key, string connectionId)
        {
            lock (allConnections)
            {
                HashSet<string> connections;
                if (!allConnections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    allConnections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetByKey(T key)
        {
            lock (allConnections)
            {
                HashSet<string> connections;
                if (allConnections.TryGetValue(key, out connections))
                {
                    return connections;
                }
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(T key, string connectionId)
        {
            lock (allConnections)
            {
                HashSet<string> connections;
                if (!allConnections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        allConnections.Remove(key);
                    }
                }
            }
        }
    }
}