﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Otlobli.Model;

namespace Otlobli.Hubs
{
    public class NotificationsHub : Hub
    {
        #region Connections managing

        public override Task OnConnected()
        {
            ConnectionManager.Connections.Add(Context.User.Identity.Name, Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            ConnectionManager.Connections.Remove(Context.User.Identity.Name, Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var userName = Context.User.Identity.Name;
            if (!ConnectionManager.Connections.GetByKey(userName).Contains(Context.ConnectionId))
                ConnectionManager.Connections.Add(userName, Context.ConnectionId);

            return base.OnReconnected();
        }

        #endregion
    }
}