﻿using System.Web;
using System.Web.Http;
using Otlobli.Model;

namespace Otlobli
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapping.Configure();
        }
    }
}
