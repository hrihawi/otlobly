﻿using System;
using System.Data.Entity.Validation;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    [Authorize(Roles = RolesNames.WebsiteAdmin)]
    public class RolesController : BaseApiController
    {
        public async Task<IHttpActionResult> GetRole(string id)
        {
            try
            {
                Guid userId;
                if (!Guid.TryParse(id, out userId))
                {
                    ModelState.AddModelError("id", "User id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var role = await AppRoleManager.FindByIdAsync(userId);

                if (role != null)
                    return ReturnValidatedResult(AutoMapping.DefaultInstanse.Map<RoleForReturn>(role));

                return ReturnValidatedResult(HttpStatusCode.NotFound);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        public IHttpActionResult GetAllRoles()
        {
            try
            {
                var roles = AppRoleManager.Roles;
                return ReturnValidatedResult(roles);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        public async Task<IHttpActionResult> ManageUsersInRole(UsersInRoleModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var role = await AppRoleManager.FindByIdAsync(new Guid(model.Id));

                if (role == null)
                {
                    ModelState.AddModelError("", "Role does not exist");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                foreach (string user in model.EnrolledUsers)
                {
                    var appUser = await AppUserManager.FindByIdAsync(new Guid(user));

                    if (appUser == null)
                    {
                        ModelState.AddModelError("", String.Format("User: {0} does not exists", user));
                        continue;
                    }

                    if (!AppUserManager.IsInRole(new Guid(user), role.Name))
                    {
                        IdentityResult result = await AppUserManager.AddToRoleAsync(new Guid(user), role.Name);

                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", String.Format("User: {0} could not be added to role", user));
                        }
                    }
                }

                foreach (string user in model.RemovedUsers)
                {
                    var appUser = await AppUserManager.FindByIdAsync(new Guid(user));

                    if (appUser == null)
                    {
                        ModelState.AddModelError("", String.Format("User: {0} does not exists", user));
                        continue;
                    }

                    IdentityResult result = await AppUserManager.RemoveFromRoleAsync(new Guid(user), role.Name);

                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", String.Format("User: {0} could not be removed from role", user));
                    }
                }

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}
