﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Otlobli.DataAccess;
using Otlobli.Hubs;
using Otlobli.Infrastructure;
using Otlobli.Model;
using SignalR = Microsoft.AspNet.SignalR;


namespace Otlobli.Controllers
{
    public class OrderController : BaseApiController
    {
        // GET api/order/Search
        [Authorize]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new OrderService().Search(searchCriteria, new Guid(CurrentUser.GetUserId()));
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/order/Get
        [HttpGet]
        [Authorize(Roles = RolesNames.AnyAdmin)]
        public IHttpActionResult Get([FromUri]GetCriteria getCriteria)
        {
            try
            {
                if (getCriteria.Id == Guid.Empty)
                {
                    ModelState.AddModelError("id", "order id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var resturant = new OrderService().GetById(getCriteria);
                return ReturnValidatedResult(resturant);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (NotFoundException bre)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = bre.Message,
                }, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/order/Add
        [HttpPost]
        [Authorize]
        public IHttpActionResult Add(OrderForAdd order)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var orderForReturn = new OrderService().AddNew(order);

                // notify resturant managers for the new order
                var context = SignalR.GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
                var rest = new RestaurantService().GetById(new GetCriteria() { Id = order.RestaurantId });
                foreach (var connectionId in rest.Managers
                    .SelectMany(manager => ConnectionManager.Connections.GetByKey(manager.UserName)))
                    context.Clients.Client(connectionId).NotifyResturantForNewOrder(orderForReturn);

                return ReturnValidatedResult(orderForReturn);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (BadRequestException bre)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = bre.Message,
                }, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/order/Update
        [HttpPost]
        [System.Web.Http.Authorize(Roles = RolesNames.AdminsAndOwner)]
        public IHttpActionResult Update(OrderForReturn order)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new OrderService().Update(order);
                return ReturnValidatedResult(HttpStatusCode.Created);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/order/UpdateStatus
        [HttpPost]
        [System.Web.Http.Authorize(Roles = RolesNames.AdminsAndOwner)]
        public async Task<IHttpActionResult> UpdateStatus(OrderForReturnSummary orderSummary)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var orderForReturn = new OrderService().UpdateStatus(orderSummary);

                // notify order owner for the order status change (Confirmed, Canceled)
                var context = SignalR.GlobalHost.ConnectionManager.GetHubContext<NotificationsHub>();
                //var orderForReturn = new OrderService().GetById(new GetCriteria() { Id = orderSummary.Id });
                var user = await AppUserManager.FindByIdAsync(orderForReturn.UserId);
                foreach (var connectionId in ConnectionManager.Connections.GetByKey(user.UserName))
                    context.Clients.Client(connectionId).NotifyUserForUpdatedOrder(orderForReturn);

                var androidDevices = new DeviceService().Search(new SearchCriteria() { UserId = orderForReturn.UserId });
                string response = null;
                foreach (var device in androidDevices.Entities)
                {
                    try
                    {
                        var tRequest = WebRequest.Create("https://gcm-http.googleapis.com/gcm/send");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";
                        tRequest.Headers.Add(string.Format("Authorization: key={0}", AppConfiguration.Get("Android:GoogleAppKey", "AIzaSyBjJsNLP04wxULvo9cSvQRFNpTlABMxyuo")));

                        using (var streamWriter = new StreamWriter(tRequest.GetRequestStream()))
                        {
                            dynamic requestData = new
                            {
                                to = device.Key,
                                data = orderForReturn
                            };

                            streamWriter.Write(JsonConvert.SerializeObject(requestData));
                            streamWriter.Flush();
                            streamWriter.Close();
                        }

                        var httpResponse = (HttpWebResponse)tRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            response = response ?? streamReader.ReadToEnd();
                        }
                    }
                    catch (Exception)
                    {
                    }
                }

                return ReturnValidatedResult(HttpStatusCode.Created, new List<MetaData>()
                {
                    new MetaData()
                    {
                        Key = "Android push notification Devices Count",
                        Value = androidDevices.Entities.Count().ToString(),
                    },
                    new MetaData()
                    {
                        Key = "Android push notification Result",
                        Value = response,
                    },
                });
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (NotFoundException bre)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = bre.Message,
                }, HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/order/Delete
        [HttpPost]
        [System.Web.Http.Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                if (!new OrderService().DeleteById(deleteCriteria.Id))
                    return ReturnValidatedResult(HttpStatusCode.NotFound);
                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}