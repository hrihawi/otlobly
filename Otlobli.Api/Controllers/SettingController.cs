﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Http;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class SettingController : BaseApiController
    {
        // GET api/Setting/Search
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new SettingService().Search(searchCriteria);
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/Setting/Get
        [HttpGet]
        [Authorize(Roles = RolesNames.AnyAdmin)]
        public IHttpActionResult Get([FromUri]GetCriteria getCriteria)
        {
            try
            {
                if (getCriteria.Id == Guid.Empty)
                {
                    ModelState.AddModelError("id", "Setting id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var Setting = new SettingService().GetById(getCriteria);
                return ReturnValidatedResult(Setting);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/Setting/Add
        [HttpPost]
        [Authorize(Roles = RolesNames.Admins)]
        public IHttpActionResult Add(Setting Setting)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new SettingService().AddNew(Setting);
                return ReturnValidatedResult(Setting);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/Setting/Update
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndOwner)]
        public IHttpActionResult Update(Setting Setting)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new SettingService().Update(Setting);
                return ReturnValidatedResult(Setting);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/Setting/Delete
        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new SettingService().DeleteById(deleteCriteria.Id);
                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}