﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class DeviceController : BaseApiController
    {
        // GET api/device/Search
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new DeviceService().Search(searchCriteria);
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/device/Get
        [HttpGet]
        [Authorize(Roles = RolesNames.AnyAdmin)]
        public IHttpActionResult Get([FromUri]GetCriteria getCriteria)
        {
            try
            {
                if (getCriteria.Id == Guid.Empty)
                {
                    ModelState.AddModelError("id", "device id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var Device = new DeviceService().GetById(getCriteria);
                return ReturnValidatedResult(Device);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/device/Add
        [HttpPost]
        [Authorize]
        public IHttpActionResult Add(DeviceForAdd device)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new DeviceService().AddNew(device,new Guid(CurrentUser.GetUserId()));
                return ReturnValidatedResult(device);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/device/Update
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndOwner)]
        public IHttpActionResult Update(DeviceForReturn device)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new DeviceService().Update(device);
                return ReturnValidatedResult(device);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/device/Delete
        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new DeviceService().DeleteById(deleteCriteria.Id);
                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}