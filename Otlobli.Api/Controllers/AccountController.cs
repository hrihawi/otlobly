﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class AccountController : BaseApiController
    {
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var usersQuery = AppUserManager.Users.OrderBy(x => (true));
                var resultCount = usersQuery.Count();
                var result = (searchCriteria.DisplayCount > 0)
                                    ? (usersQuery.Skip(searchCriteria.DisplayStart).Take(searchCriteria.DisplayCount).ToList())
                                    : (usersQuery.ToList());

                bool hasNext = (searchCriteria.DisplayStart > 0 || searchCriteria.DisplayCount > 0) && (searchCriteria.DisplayStart + searchCriteria.DisplayCount < resultCount);

                var mappedResults = new List<UserForReturn>();
                foreach (var user in result)
                {
                    var mappedUser = AutoMapping.DefaultInstanse.Map<UserForReturn>(user);
                    var roles = AppUserManager.GetRolesAsync(user.Id).Result;
                    mappedUser.Role = ((roles == null || roles.Count == 0) ? RolesNames.User : roles[0]);
                    mappedResults.Add(mappedUser);
                }

                return ReturnValidatedResult(new PagedListResult<UserForReturn>()
                {
                    Entities = mappedResults,
                    HasNext = hasNext,
                    HasPrevious = (searchCriteria.DisplayStart > 0),
                    Count = resultCount
                });
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [Authorize(Roles = RolesNames.AdminsAndUser)]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string id)
        {
            try
            {
                Guid userId;
                if (!Guid.TryParse(id, out userId))
                {
                    ModelState.AddModelError("id", "User id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var user = await AppUserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    var mappedUser = AutoMapping.DefaultInstanse.Map<UserForReturn>(user);
                    var roles = AppUserManager.GetRolesAsync(user.Id).Result;
                    mappedUser.Role = roles[0];
                    return ReturnValidatedResult(mappedUser);
                }

                return ReturnValidatedResult(HttpStatusCode.NotFound);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public async Task<IHttpActionResult> GetByName(string username)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(username))
                {
                    ModelState.AddModelError("username", "user name is required");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var user = await AppUserManager.FindByNameAsync(username);
                if (user != null)
                {
                    var mappedUser = AutoMapping.DefaultInstanse.Map<UserForReturn>(user);
                    var roles = AppUserManager.GetRolesAsync(user.Id).Result;
                    mappedUser.Role = roles[0];
                    return ReturnValidatedResult(mappedUser);
                }

                return ReturnValidatedResult(HttpStatusCode.NotFound);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [AllowAnonymous]
        public async Task<IHttpActionResult> Add(UserForAdd userForAdd)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var user = AutoMapping.DefaultInstanse.Map<User>(userForAdd);
                user.Image = user.Image.ResizeImage();
                var result = await AppUserManager.CreateAsync(user, userForAdd.Password);
                if (!result.Succeeded)
                {
                    return ReturnValidatedResult(new Error()
                    {
                        Message = string.Join(",", result.Errors),
                    });
                }

                await AppUserManager.AddToRoleAsync(user.Id, RolesNames.User);

                string code = await AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                //var confirmEmailUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code = code }));

                string confirmEmailUrl = string.Format("{0}?userId={1}&code={2}",
                    AppConfiguration.Get("Account:ConfirmEmailRedirectUrl"),
                    user.Id, code);

                await AppUserManager.SendEmailAsync(user.Id,
                    "Confirm your account",
                    string.Format("Please confirm your account by clicking <a href=\"{0}\">here</a>", confirmEmailUrl));

                // Generate the token 
                var smsCode = await AppUserManager.GenerateChangePhoneNumberTokenAsync(user.Id, user.PhoneNumber);
                if (AppUserManager.SmsService != null)
                {
                    var message = new IdentityMessage
                    {
                        Destination = user.PhoneNumber,
                        Body = "otloblee.net+code:+" + smsCode
                    };
                    // Send token
                    await AppUserManager.SmsService.SendAsync(message);
                }

                var mappedUser = AutoMapping.DefaultInstanse.Map<UserForReturn>(user);
                var roles = AppUserManager.GetRolesAsync(user.Id).Result;
                mappedUser.Role = roles[0];
                return ReturnValidatedResult(mappedUser);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST: /Account/UpdatePhoneNumber
        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> UpdatePhoneNumber(UpdatePhoneNumber model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                // Generate the token 
                var smsCode = await AppUserManager.GenerateChangePhoneNumberTokenAsync(new Guid(CurrentUser.GetUserId()), model.PhoneNumber);
                if (AppUserManager.SmsService != null)
                {
                    var message = new IdentityMessage
                    {
                        Destination = model.PhoneNumber,
                        Body = "otloblee.net+code:+" + smsCode
                    };
                    // Send token
                    await AppUserManager.SmsService.SendAsync(message);
                }

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST: /Account/ConfirmPhoneNumber
        [HttpPost]
        [Authorize]
        public async Task<IHttpActionResult> ConfirmPhoneNumber(ConfirmPhoneNumber model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var user = await AppUserManager.FindByIdAsync(new Guid(CurrentUser.GetUserId()));

                var result = await AppUserManager.ChangePhoneNumberAsync(user.Id, user.PhoneNumber, model.Code);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("phone number", result.Errors.Aggregate((i, j) => i + j));
                    return ReturnValidatedResult(ModelState);
                }

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmailRoute")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId = "", string code = "")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
                {
                    ModelState.AddModelError("", "User Id and Code are required");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var result = await AppUserManager.ConfirmEmailAsync(new Guid(userId), code);

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                IdentityResult result = await AppUserManager.ChangePasswordAsync(new Guid(User.Identity.GetUserId()), model.OldPassword, model.NewPassword);

                return ReturnValidatedResult(result);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);

                var user = await AppUserManager.FindByEmailAsync(model.Email);
                if (user == null /* || !(await AppUserManager.IsEmailConfirmedAsync(user.Id))*/)
                    return ReturnValidatedResult(HttpStatusCode.BadRequest);

                var code = await AppUserManager.GeneratePasswordResetTokenAsync(user.Id);

                string resetPasswordUrl = string.Format("{0}?userId={1}&code={2}",
                    AppConfiguration.Get("Account:ResetPasswordRedirectUrl"),
                    user.Id, code);

                await AppUserManager.SendEmailAsync(user.Id, "Reset Password",
                    string.Format("Please reset your password by clicking <a href=\"{0}\">here</a>", resetPasswordUrl));

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);

                var result = await AppUserManager.ResetPasswordAsync(model.UserId, model.Code, model.NewPassword);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("Reset Password", result.Errors.Aggregate((i, j) => i + j));
                    return ReturnValidatedResult(ModelState);
                }

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
        

        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public async Task<IHttpActionResult> Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var appUser = await AppUserManager.FindByIdAsync(deleteCriteria.Id);

                if (appUser != null)
                {
                    IdentityResult result = await AppUserManager.DeleteAsync(appUser);
                    return ReturnValidatedResult(result, HttpStatusCode.OK);
                }

                return ReturnValidatedResult(HttpStatusCode.NotFound);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/Account/Update
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndUser)]
        public IHttpActionResult Update(UserForReturn userForReturn)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new AccountService().Update(userForReturn);

                return ReturnValidatedResult(userForReturn);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/Restaurant/UpdateStatus
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndUser)]
        public IHttpActionResult UpdateStatus(UserStatusForAdd status)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new AccountService().UpdateStatus(status);
                return ReturnValidatedResult(HttpStatusCode.Created);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        [HttpPost]
        public async Task<IHttpActionResult> AssignRolesToUser(RoleForAssign roleForAdd)
        {
            try
            {
                Guid userId;
                if (!Guid.TryParse(roleForAdd.UserId, out userId) || string.IsNullOrWhiteSpace(roleForAdd.Role))
                {
                    ModelState.AddModelError("id", "User id is required as a valid guid");
                    ModelState.AddModelError("rolesToAssign", "rolesToAssign is required as a list of roles");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var appUser = await AppUserManager.FindByIdAsync(userId);
                if (appUser == null)
                    return ReturnValidatedResult(HttpStatusCode.NotFound);

                var currentRoles = await AppUserManager.GetRolesAsync(appUser.Id);

                if (!AppRoleManager.Roles.Any(x => x.Name == roleForAdd.Role))
                {
                    ModelState.AddModelError("", String.Format("Role '{0}' does not exixts in the system", roleForAdd.Role));
                    return ReturnValidatedResult(HttpStatusCode.BadRequest);
                }

                IdentityResult removeResult = await AppUserManager.RemoveFromRolesAsync(appUser.Id, currentRoles.ToArray());

                if (!removeResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to remove user roles");
                    return ReturnValidatedResult(HttpStatusCode.BadRequest);
                }

                IdentityResult addResult = await AppUserManager.AddToRolesAsync(appUser.Id, new string[] { roleForAdd.Role });

                if (!addResult.Succeeded)
                {
                    ModelState.AddModelError("", "Failed to add user roles");
                    return ReturnValidatedResult(HttpStatusCode.BadRequest);
                }

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}