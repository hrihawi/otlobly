﻿using System;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Http;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class RestaurantTypeController : BaseApiController
    {
        // GET api/RestaurantType/Search
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new RestaurantTypeService().Search(searchCriteria);
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/RestaurantType/Get
        [HttpGet]
        [Authorize(Roles = RolesNames.AnyAdmin)]
        public IHttpActionResult Get([FromUri]GetCriteria getCriteria)
        {
            try
            {
                if (getCriteria.Id == Guid.Empty)
                {
                    ModelState.AddModelError("id", "Resturant id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var resturant = new RestaurantTypeService().GetById(getCriteria);
                return ReturnValidatedResult(resturant);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/RestaurantType/Add
        [HttpPost]
        [Authorize(Roles = RolesNames.Admins)]
        public IHttpActionResult Add(RestaurantTypeForAdd RestaurantType)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var rest = new RestaurantTypeService().AddNew(RestaurantType);
                return ReturnValidatedResult(rest);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/RestaurantType/Update
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndOwner)]
        public IHttpActionResult Update(RestaurantTypeForReturn RestaurantType)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new RestaurantTypeService().Update(RestaurantType);
                return ReturnValidatedResult(HttpStatusCode.Created);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/RestaurantType/Delete
        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                if (!new RestaurantTypeService().DeleteById(deleteCriteria.Id))
                    return ReturnValidatedResult(HttpStatusCode.NotFound);
                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}