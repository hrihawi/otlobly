﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class ImageForMealController : BaseApiController
    {
        // GET api/ImageForMeal/Search
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new ImageForMealService().Search(searchCriteria);
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/ImageForMeal/Get
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage Get([FromUri]string Id)
        {
            try
            {
                Id = Id.Replace(".jpg", string.Empty);
                Guid guid;
                if (!Guid.TryParse(Id, out guid))
                {
                    ModelState.AddModelError("id", "Image id is required as a valid guid");
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }

                var image = new ImageForMealService().GetById(new GetCriteria() {Id = guid });
                if (image == null || image.Data == null)
                    return new HttpResponseMessage(HttpStatusCode.NotFound);

                return new HttpResponseMessage(HttpStatusCode.OK).ReturnContent(image.Data);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        // POST api/ImageForMeal/Add
        [HttpPost]
        [Authorize(Roles = RolesNames.Admins)]
        public IHttpActionResult Add(ImageForMealForAdd imageForAdd)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var imgForReturn = new ImageForMealService().Add(imageForAdd);
                return ReturnValidatedResult(imgForReturn);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/ImageForMeal/Delete
        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new ImageForMealService().DeleteById(deleteCriteria.Id);
                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}