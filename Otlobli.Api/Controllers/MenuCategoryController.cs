﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Http;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class MenuCategoryController : BaseApiController
    {
        // GET api/MenuCategory/Search
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Search([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var pagedListResult = new MenuCategoryService().Search(searchCriteria);
                return ReturnValidatedResult(pagedListResult);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // GET api/MenuCategory/Get
        [HttpGet]
        [Authorize(Roles = RolesNames.AnyAdmin)]
        public IHttpActionResult Get([FromUri]GetCriteria getCriteria)
        {
            try
            {
                if (getCriteria.Id == Guid.Empty)
                {
                    ModelState.AddModelError("id", "MenuCategory id is required as a valid guid");
                    return ReturnValidatedResult(ModelState, HttpStatusCode.BadRequest);
                }

                var MenuCategory = new MenuCategoryService().GetById(getCriteria);
                return ReturnValidatedResult(MenuCategory);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/MenuCategory/Add
        [HttpPost]
        [Authorize(Roles = RolesNames.Admins)]
        public IHttpActionResult Add(MenuCategoryForAdd MenuCategory)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                var menuCtg = new MenuCategoryService().AddNew(MenuCategory);
                return ReturnValidatedResult(menuCtg);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // POST api/MenuCategory/Update
        [HttpPost]
        [Authorize(Roles = RolesNames.AdminsAndOwner)]
        public IHttpActionResult Update(MenuCategoryForReturn menuCategoryForReturn)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                new MenuCategoryService().Update(menuCategoryForReturn);
                return ReturnValidatedResult(HttpStatusCode.Created);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

        // DELETE api/MenuCategory/Delete
        [HttpPost]
        [Authorize(Roles = RolesNames.WebsiteAdmin)]
        public IHttpActionResult Delete(DeleteCriteria deleteCriteria)
        {
            try
            {
                if (!ModelState.IsValid)
                    return ReturnValidatedResult(ModelState);

                if (!new MenuCategoryService().DeleteById(deleteCriteria.Id))
                    return ReturnValidatedResult(HttpStatusCode.NotFound);

                return ReturnValidatedResult(HttpStatusCode.OK);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }
    }
}