﻿using System;
using System.Data.Entity.Validation;
using System.Net;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Otlobli.DataAccess;
using Otlobli.Infrastructure;
using Otlobli.Model;

namespace Otlobli.Controllers
{
    public class ReportController : BaseApiController
    {
        // GET api/Report/GetStatisticsReport
        [HttpGet]
        [Authorize(Roles = RolesNames.Admins)]
        public IHttpActionResult GetStatisticsReport([FromUri]SearchCriteria searchCriteria)
        {
            try
            {
                var roles = AppUserManager.GetRolesAsync(new Guid(CurrentUser.GetUserId())).Result;
                var role = (roles == null || roles.Count == 0) ? RolesNames.User : roles[0];

                var report = new ReportService().GetStatisticsReport(searchCriteria, CurrentUser.Name, role);
                return ReturnValidatedResult(report);
            }
            catch (DbEntityValidationException dbE)
            {
                var results = dbE.HandleValidatedResult();
                return ReturnValidatedResult(new Error()
                {
                    Message = results[0],
                    StackTrace = results[1],
                });
            }
            catch (Exception ex)
            {
                return ReturnValidatedResult(new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                });
            }
        }

    }
}